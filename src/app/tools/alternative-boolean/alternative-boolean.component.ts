import { Component, OnInit, Input, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { fromEventValue } from '../common/gmat-functions';
import { IsCheckedAlternative } from '../common/alternative';

@Component({
    selector: 'gmat-alternative-boolean',
    templateUrl: './alternative-boolean.component.html',
    styleUrls: ['./alternative-boolean.component.less']
})
export class AlternativeBooleanComponent implements OnInit {

    alternatives = [];
    @Input() question: any;
    @Input() form: FormGroup;

    ngOnInit() {
        this.form.controls.answersId.setValidators([Validators.required]);
        this.form.controls.answersId.updateValueAndValidity();
    }

    onChangeInput(value: any) {
        this.form.controls.answersId.setValue([value]);
    }

    customRadioIdentify(alternative: any) {
        return 'custom-radio' + alternative.id;
    }

    alternativeIsChecked(value: any) {
        if (this.question.clientAnswer) {
            if (this.question.clientAnswer.find((x) => Number(x) === value)) {
                return true;
            }
        }
        return false;
    }
}
