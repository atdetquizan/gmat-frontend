import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { isValidateAlternative, isValidateAlternatives } from '../common/alternatives-functions';

@Component({
    selector: 'gmat-alternative-yesnomultiplechoice',
    templateUrl: './alternative-yesnomultiplechoice.component.html',
    styleUrls: ['./alternative-yesnomultiplechoice.component.less']
})
export class AlternativeYesnomultiplechoiceComponent implements OnInit {
    pathImage = '';
    formAlternatives: FormGroup;
    @Input() question: any;
    @Input() form: FormGroup;

    @ViewChild('selectFile') selectFile: ElementRef;
    constructor(private _fb: FormBuilder) {
        this.createform();
    }

    ngOnInit() {
        this.form.controls.answersId.setValidators([Validators.required]);
        this.form.controls.answersId.updateValueAndValidity();
        this.pathImage =
            this.question.headerTags && this.question.headerTags.length > 0
                ? this.question.headerTags[0].filePathUrl
                : '';
        for (const item of this.question.alternatives) {
            this.addAlternatives(item);
        }
    }

    ngAfterViewInit() {
        if (this.question.clientAnswer) {
            this.formAlternatives.disable();
        }
    }

    onChangePathImage(value: string) {
        this.pathImage = value;
    }

    onChangeAlternative(index: any, value: string) {
        const formArrayAlt = this.formAlternatives.controls.alternatives as FormArray;
        const formGroupAlt = formArrayAlt.controls[index] as FormGroup;
        formGroupAlt.controls.answerId.setValue([value]);
        if (isValidateAlternatives(this.formAlternatives)) {
            this.form.controls.answersId.setValue(this.formAlternatives.value.alternatives);
        }
        console.log(this.form.valid);
    }

    isValidate(index: any) {
        return isValidateAlternative(this.formAlternatives, index);
    }

    customRadioId(subalternative: any) {
        return 'custom-radio-' + subalternative.name;
    }

    addAlternatives(alternative: any) {
        const controls = this.formAlternatives.controls.alternatives as FormArray;
        controls.push(
            this._fb.group({
                alternativeId: [alternative.id, null],
                answerId: [null, [Validators.required]]
            })
        );
    }

    
    alternativeIsChecked(value: any) {
        if (this.question.clientAnswer) {
            if (this.question.clientAnswer.find((x) => Number(x) === value)) {
                return true;
            }
        }
        return false;
    }

    private createform() {
        this.formAlternatives = this._fb.group({
            alternatives: this._fb.array([])
        });
    }
}
