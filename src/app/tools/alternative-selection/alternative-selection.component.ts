import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'gmat-alternative-selection',
    templateUrl: './alternative-selection.component.html',
    styleUrls: ['./alternative-selection.component.less']
})
export class AlternativeSelectionComponent implements OnInit {
    formAlterntives: FormGroup;
    alternatives = [];
    @Input() question: any;
    @Input() form: FormGroup;

    constructor(private _fb: FormBuilder) {
        this.createform();
    }

    ngOnInit() {
        this.form.controls.answersId.setValidators([Validators.required]);
        this.form.controls.answersId.updateValueAndValidity();
        this.alternatives = this.question.alternatives;
        this.addCheckboxes();

        if (this.question.clientAnswer) {
            const formArray = this.formAlterntives.controls.alternatives as FormArray;
            formArray.controls.forEach((value, index) => {
                value.disable();
                // console.log(this.alternatives[index].id);
                if (this.question.clientAnswer.find(x => x === this.alternatives[index].id)) {
                    value.setValue(this.alternatives[index].id);
                }
            })
        }
    }

    onChangeCheckbox() {
        const selectedIds = this.formAlterntives.value.alternatives
            .map((v, i) => v ? this.alternatives[i].id : null)
            .filter(v => v !== null);
        this.form.controls.answersId.setValue(selectedIds.length > 0 ? selectedIds : null);
    }

    customCheckboxIdentify(index: number) {
        return 'custom-radio' + this.alternatives[index].id;
    }

    controlsList() {
        return (this.formAlterntives.controls.alternatives as FormGroup).controls
    }

    private addCheckboxes() {
        this.alternatives.map((o, i) => {
            const control = new FormControl();
            (this.formAlterntives.controls.alternatives as FormArray).push(control);
        });
    }

    private createform() {
        this.formAlterntives = this._fb.group({
            alternatives: new FormArray([])
        });
    }

}
