import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';
import { Subscription } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ControlMessagesService {

    constructor() { }

    static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        const config = {
            'required': '(*) The field is required.',
            'email': 'The email is not valid.',
            'invalidPassword': 'Invalid password. Password must be at least 6 characters long, and contain a number.',
            'minlength': `Minimum characters allowed ${validatorValue.requiredLength}`,
            'maxlength': `Maximum characters allowed ${validatorValue.requiredLength}`,
            'isnumber': 'Allowed only numbers.',
            'isdecimal': 'Allowed only whole numbers and decimals.',
            'pattern': 'The value not allowed.',
            'isequals': 'The value is not allowed.',
            'match': 'Enter the same value again.',
            'digits': 'The value entered is not correct only numbers are allowed'            
        };
        return config[validatorName];
    }

    static isNumber(control) {
        const NUMBER_REGEXP = /^[0-9]+$/;
        return NUMBER_REGEXP.test(control.value) ? null : {
            isnumber: {
                valid: false
            }
        };
    }
    static isDecimal(control) {
        const DECIMAL_REGEXP = /^[.\d]+$/;
        return DECIMAL_REGEXP.test(control.value) ? null : {
            isdeciaml: {
                valid: false
            }
        };
    }
    static isEmptyInputValue(value: any) {
        return value == null || typeof value === 'string' && value.length === 0;
    }

    static matchOtherValidator(otherControlName: string): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } => {
            const otherControl: AbstractControl = control.root.get(otherControlName);
    
            if (otherControl) {
                const subscription: Subscription = otherControl
                    .valueChanges
                    .subscribe(() => {
                        control.updateValueAndValidity();
                        subscription.unsubscribe();
                    });
            }
    
            return (otherControl && control.value !== otherControl.value) ? { match: true } : null;
        };
    }

}
