import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { InformationShowType } from './information-show.type';

@Injectable({
    providedIn: 'root'
})
export class InformationShowService {
    dataSource = new BehaviorSubject<InformationShowType>(null);
    status = this.dataSource.asObservable();

    constructor() { }

    setup(type: InformationShowType) {
        this.dataSource.next(type);
    }
}