import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { InformationShowService } from './information-show.service';
import { InformationShowType } from './information-show.type';
import { messages } from './information-show.messages';

@Component({
    selector: 'gmat-information-show',
    templateUrl: './information-show.component.html',
    styleUrls: ['./information-show.component.less'],
    encapsulation: ViewEncapsulation.None
})
export class InformationShowComponent implements OnInit {
    icon: string;
    message: string;
    constructor(private informationShowService: InformationShowService) { }

    ngOnInit() {
        this.informationShowService.status.subscribe((type: InformationShowType) => {
            switch (type) {
                case InformationShowType.NO_SHOW_PRACTICE:
                    this.icon = 'fab fa-creative-commons-share';
                    this.message = messages.NO_SHOW_PRACTICE
                    break;

                default:
                    break;
            }
        });
    }

}
