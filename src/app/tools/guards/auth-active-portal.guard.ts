import { Injectable } from '@angular/core';
import {
    CanActivateChild,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../../authentication/authentication.service';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthActivePortalGuard implements CanActivateChild {
    constructor(private auth: AuthenticationService, private router: Router) {}
    canActivateChild(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        if (!this.auth.getUserClient()) {
            this.router.navigate(['/']);
            return false;
        }
        return true;
    }
}
