import { Injectable } from '@angular/core';
import {
    CanActivateChild,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree
} from '@angular/router';
import { Observable } from 'rxjs';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class AuthInactivePortalGuard implements CanActivateChild {
    constructor(private auth: AuthenticationService, private router: Router) {}
    // virify router => Validate all children
    canActivateChild(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        if (this.auth.getUserClient()) {
            this.router.navigate(['/portal']);
            return false;
        }
        return true;
    }
}
