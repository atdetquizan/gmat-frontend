export enum ChronometerType {
    START = 'START',
    STOP = 'STOP',
    RESTART = 'RESTART'
}