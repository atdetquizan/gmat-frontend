import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ChronometerType } from './chronometer.type';

@Injectable({
    providedIn: 'root'
})
export class ChronometerService {
    dataSource = new BehaviorSubject<ChronometerType>(ChronometerType.START);
    onSeconds = new BehaviorSubject<number>(0);
    status = this.dataSource.asObservable();

    constructor() { }

    changeSeconds(seconds: any) {
        this.onSeconds.next(seconds);
    }

    start() {
        this.dataSource.next(ChronometerType.START);
    }

    stop() {
        this.dataSource.next(ChronometerType.STOP);
    }

    restart() {
        this.dataSource.next(ChronometerType.RESTART);
    }
}