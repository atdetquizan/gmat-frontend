import {
    Component,
    OnInit,
    ViewChild,
    ElementRef,
    Output,
    EventEmitter
} from '@angular/core';
import { timer, Observable, Subscription } from 'rxjs';
import { format } from '../common/gmat-functions';
import { ChronometerService } from './chronometer.service';
import { ChronometerType } from './chronometer.type';
import { takeWhile, tap } from 'rxjs/operators';

@Component({
    selector: 'gmat-chronometer',
    templateUrl: './chronometer.component.html',
    styleUrls: ['./chronometer.component.less']
})
export class ChronometerComponent implements OnInit {
    secons = 0;
    timerSubscribe: Subscription;
    // TIPO ACTUAL
    typeCurrent: ChronometerType;
    // ELEMENTO TIMMER
    @ViewChild('timer') timer: ElementRef;
    // VALOR DE SALIDA SEGUNDOS
    @Output() onValueSecons: EventEmitter<any> = new EventEmitter();
    constructor(private chronometerService: ChronometerService) {}

    ngOnInit() {
        this.chronometerService.onSeconds.subscribe(
            (seconds: number) => ((this.secons = seconds), this.setSecons())
        );
        this.chronometerService.status.subscribe((type: ChronometerType) => {
            this.typeCurrent = type;
            switch (type) {
                case ChronometerType.START:
                    this.startTimer();
                    break;

                case ChronometerType.STOP:
                    this.stopTimer();
                    break;

                case ChronometerType.RESTART:
                    this.restart();
                    break;

                default:
                    break;
            }
        });
    }

    startTimer() {
        this.timerSubscribe = timer(1000, 1000)
            .pipe(tap(() => this.secons++))
            .subscribe(() => {
                this.setSecons();
            });
    }

    stopTimer() {
        this.timerSubscribe.unsubscribe();
    }

    restart() {
        this.timerSubscribe.unsubscribe();
        this.secons = 0;
        this.setSecons();
        this.startTimer();
    }

    setSecons() {
        this.onValueSecons.emit(this.secons);
        this.timer.nativeElement.innerHTML = format(this.secons);
    }
}
