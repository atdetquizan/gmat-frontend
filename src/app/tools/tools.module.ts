import { NgModule } from '@angular/core';
import { AlertMessageComponent } from './alert-message/alert-message.component';
import { AlertModule, TabsModule } from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { AlternativeBooleanComponent } from './alternative-boolean/alternative-boolean.component';
import { AlternativeDropdowncomplementComponent } from './alternative-dropdowncomplement/alternative-dropdowncomplement.component';
import { AlternativeExplanationComponent } from './alternative-explanation/alternative-explanation.component';
import { AlternativeSelectionComponent } from './alternative-selection/alternative-selection.component';
import { AlternativeTablesimpleselectionComponent } from './alternative-tablesimpleselection/alternative-tablesimpleselection.component';
import { AlternativeYesnomultiplechoiceComponent } from './alternative-yesnomultiplechoice/alternative-yesnomultiplechoice.component';
import { ControlMessagesModule } from './control-messages/control-messages.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AlternativeRenderComponent } from './alternative-render/alternative-render.component';
import { ChronometerComponent } from './chronometer/chronometer.component';
import { ChronometerService } from './chronometer/chronometer.service';
import { InformationShowComponent } from './information-show/information-show.component';
import { InformationShowService } from './information-show/information-show.service';
import { ModalMessagesComponent } from './modal-messages/modal-messages.component';
import { SanitizeHtmlPipe } from './pipes/samitize-html.pipe';
import { TrimDirective } from './directives/trim.directive';

const components = [
    AlertMessageComponent,
    AlternativeBooleanComponent,
    AlternativeDropdowncomplementComponent,
    AlternativeExplanationComponent,
    AlternativeSelectionComponent,
    AlternativeTablesimpleselectionComponent,
    AlternativeYesnomultiplechoiceComponent,
    AlternativeRenderComponent,
    ChronometerComponent,
    InformationShowComponent,
    ModalMessagesComponent,
    SanitizeHtmlPipe,
    TrimDirective
];

@NgModule({
    imports: [CommonModule, AlertModule.forRoot(), TabsModule.forRoot(), ReactiveFormsModule, ControlMessagesModule],
    declarations: components,
    exports: [...components],
    entryComponents: [ModalMessagesComponent],
    providers: [ChronometerService, InformationShowService]
})
export class ToolsModule { }
