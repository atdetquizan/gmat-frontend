import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { isValidateAlternative, isValidateAlternatives } from '../common/alternatives-functions';

@Component({
    selector: 'gmat-alternative-dropdowncomplement',
    templateUrl: './alternative-dropdowncomplement.component.html',
    styleUrls: ['./alternative-dropdowncomplement.component.less']
})
export class AlternativeDropdowncomplementComponent implements OnInit, AfterViewInit {
    alternatives = [];
    formAlternatives: FormGroup;
    @Input() question: any;
    @Input() form: FormGroup;

    constructor(private _fb: FormBuilder) {
        this.createform();
    }

    ngOnInit() {
        this.form.controls.answersId.setValidators([Validators.required]);
        this.form.controls.answersId.updateValueAndValidity();
        this.alternatives = this.question.alternatives;
        for (const item of this.alternatives) {
            this.addAlternatives(item);
        }
    }

    ngAfterViewInit() {
        if (this.question.clientAnswer) {
            this.formAlternatives.disable();
        }
    }

    onChangeAlternative(index: any, value: string) {
        const formArrayAlt = this.formAlternatives.controls.alternatives as FormArray;
        const formGroupAlt = formArrayAlt.controls[index] as FormGroup;
        formGroupAlt.controls.answerId.setValue([value]);
        if (isValidateAlternatives(this.formAlternatives)) {
            this.form.controls.answersId.setValue(this.formAlternatives.value.alternatives);
        }
    }

    controlsByIndex(index: any) {
        const formArrayAlt = this.formAlternatives.controls.alternatives as FormArray;
        const formGroupAlt = formArrayAlt.controls[index] as FormGroup;
        formGroupAlt.controls.answerId.markAsTouched();
        return formGroupAlt.controls.answerId;
    }

    isTouchedAlternative(formCurrent: FormGroup) {
        if (formCurrent.controls.answersId && formCurrent.controls.answersId.touched) {
            return true;
        }
        return false;
    }

    isValidate(index: any) {
        return isValidateAlternative(this.formAlternatives, index);
    }

    addAlternatives(alternative: any) {
        const controls = this.formAlternatives.controls.alternatives as FormArray;
        controls.push(
            this._fb.group({
                alternativeId: [alternative.id, null],
                answerId: [null, [Validators.required]]
            })
        );
    }
    
    alternativeIsChecked(value: any) {
        if (this.question.clientAnswer) {
            if (this.question.clientAnswer.find((x) => Number(x) === value)) {
                return true;
            }
        }
        return false;
    }

    private createform() {
        this.formAlternatives = this._fb.group({
            alternatives: this._fb.array([])
        });
    }
}
