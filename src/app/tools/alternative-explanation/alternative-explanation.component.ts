import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { fromEventValue } from '../common/gmat-functions';

@Component({
    selector: 'gmat-alternative-explanation',
    templateUrl: './alternative-explanation.component.html',
    styleUrls: ['./alternative-explanation.component.less']
})
export class AlternativeExplanationComponent implements OnInit, AfterViewInit {
    @Input() question: any;
    @Input() form: FormGroup;

    @ViewChild('inputtext') inputtext: ElementRef;

    constructor() { }

    ngOnInit() {
        this.form.controls.answersId.setValidators([Validators.required]);
        this.form.controls.answersId.updateValueAndValidity();
    }

    ngAfterViewInit() {
        this.onKeyup();
        this.form.controls.answersId.valueChanges.subscribe((value) => {
            if (value === null) {
                this.inputtext.nativeElement.value = value;
            }
        });     
        if (this.question.clientAnswer) {
            this.inputtext.nativeElement.value = this.question.clientAnswer;
            this.inputtext.nativeElement.disabled = true;
        }
    }

    onKeyup() {
        const input = fromEventValue(this.inputtext.nativeElement, 'keyup');
        input.subscribe((value: any) => {
            this.form.controls.answersId.setValue(value);
        });
    }
}
