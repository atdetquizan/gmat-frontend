import { Injectable, EventEmitter } from "@angular/core";

@Injectable()
export class AuthEventService {
    changePhoto: EventEmitter<any> = new EventEmitter();
}