import { Injectable } from '@angular/core';

declare const swal: any;

@Injectable({
    providedIn: 'root'
})
export class SweetalertService {

    constructor() { }

    swal(...args) {
        return args.length > 1 ? swal(args.shift(), args.pop()) : swal(args.shift())
    }

}
