import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class ReactiveFormsService {

    constructor() { }

    markFormGroupTouched(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach((key: any) => {
            // formGroup.controls[key].markAsTouched();
            const control: any = formGroup.get(key);
            if (!control.controls) {
                control.markAsTouched({ onlySelf: true });
            } else {
                const formArray: any = control.controls;
                formArray.forEach(field => {
                    Object.keys(field.controls).forEach(subkey => {
                        const nestedControl = field.get(subkey);
                        nestedControl.markAsTouched({ onlySelf: true });
                    });
                });
            }
        });
    }

    isValid(formControl: FormControl) {
        return formControl.touched && !formControl.valid ? 'is-invalid' : '';
    }
}
