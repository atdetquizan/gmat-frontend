import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-alert-message',
    templateUrl: './alert-message.component.html',
    styles: []
})
export class AlertMessageComponent implements OnInit {
    @Input() dismissible = false;
    @Input() setting: Alert;
    constructor() {}

    ngOnInit() {
        //
    }

    onClosedAlert() {
        this.setting = null;
        this.dismissible = false;
    }
}

export interface Alert {
    title: string;
    msg: string;
    type: AlertType;
}

export enum AlertType {
    warning = 'warning',
    danger = 'danger',
    info = 'info',
    success = 'success'
}
