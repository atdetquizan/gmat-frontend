import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { isValidateAlternatives, isValidateAlternative } from '../common/alternatives-functions';

@Component({
    selector: 'gmat-alternative-tablesimpleselection',
    templateUrl: './alternative-tablesimpleselection.component.html',
    styleUrls: ['./alternative-tablesimpleselection.component.less']
})
export class AlternativeTablesimpleselectionComponent implements OnInit {
    formAlternatives: FormGroup;
    @Input() question: any;
    @Input() form: FormGroup;

    constructor(private _fb: FormBuilder) {
        this.createform();
    }

    ngOnInit() {
        this.form.controls.answersId.setValidators([Validators.required]);
        this.form.controls.answersId.updateValueAndValidity();
        if (this.question.alternatives && this.question.alternatives.length > 0) {
            for (const item of this.question.alternatives[0].subAlternatives) {
                this.addAlternatives(item);
            }
        }
    }

    ngAfterViewInit() {
        if (this.question.clientAnswer) {
            this.formAlternatives.disable();
        }
    }

    onChangeAlternative(index: any, value: string) {
        const formArrayAlt = this.formAlternatives.controls.alternatives as FormArray;
        const formGroupAlt = formArrayAlt.controls[index] as FormGroup;
        formGroupAlt.controls.answerId.setValue([value]);
        if (isValidateAlternatives(this.formAlternatives)) {
            this.form.controls.answersId.setValue(this.formAlternatives.value.alternatives);
        }
    }

    isValidate(index: any) {
        return isValidateAlternative(this.formAlternatives, index);
    }

    addAlternatives(alternative: any) {
        const controls = this.formAlternatives.controls.alternatives as FormArray;
        controls.push(
            this._fb.group({
                alternativeId: [alternative.id, null],
                answerId: [null, [Validators.required]]
            })
        );
    }

    alternativeIsChecked(value: any) {
        if (this.question.clientAnswer) {
            if (this.question.clientAnswer.find((x) => Number(x) === value)) {
                return true;
            }
        }
        return false;
    }

    private createform() {
        this.formAlternatives = this._fb.group({
            alternatives: this._fb.array([])
        });
    }
}
