import {
    Directive,
    ElementRef,
    HostListener,
    OnInit,
    Input
} from '@angular/core';
import { ControlContainer, FormGroupDirective } from '@angular/forms';

@Directive({
    selector: '[appTrimGmat]'
})
export class TrimDirective implements OnInit {
    @Input('appTrimGmat') nameControl: string;

    constructor(
        private el: ElementRef,
        private controlContainer: ControlContainer
    ) {
        // el.nativeElement.style.backgroundColor = 'yellow';
    }

    ngOnInit() {
        //
    }

    @HostListener('blur')
    onkeydown() {
        const value = this.el.nativeElement.value;
        if (this.nameControl) {
            this.form
                .get(this.nameControl)
                .setValue(value.trim().replace(/ +/g, ' '));
        } else {
            this.el.nativeElement.value = value;
        }
    }

    get form() {
        return this.controlContainer.formDirective
            ? (this.controlContainer.formDirective as FormGroupDirective).form
            : null;
    }
}
