import { ValidatorFn, FormArray } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppHttpInterceptorClient } from 'src/app/interceptors/AppHttpInterceptorClient';
import * as CryptoJS from 'crypto-js';
import { environment } from '../../../environments/environment';
import { AppHttpErrorHandle } from '../interceptors/http-error-handle';
import { Observable, fromEvent, OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';

export function emptyToNull(value: any | any[]) {
    const val = value ? value : null;
    return val;
}

export const toDataURL = file =>
    new Promise((resolve, reject) => {
        var fr = new FileReader();
        fr.onload = resolve; // CHANGE to whatever function you want which would eventually call resolve
        fr.readAsDataURL(file);
    });

export const abecedario = i => {
    const abcArray = [
        'A',
        'B',
        'C',
        'D',
        'E',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'O',
        'P',
        'Q',
        'R',
        'S',
        'U',
        'V',
        'W',
        'X',
        'Y',
        'Z'
    ];
    return abcArray[i];
};

export function formatMMSS(s: any) {
    const mm = Math.trunc((s - (s %= 60)) / 60);
    return (mm < 10 ? '0' + mm : mm) + (9 < Math.trunc(s) ? ':' : ':0') + Math.trunc(s);
}

export function $(content) {
    return document.querySelector(content);
}

export const loading = (status: boolean) => {
    const loading: any = document.querySelector('.loading-page-content');
    // valid element exist
    if (!loading) return;
    // hide and show element
    if (!status) loading.style = 'display: none!important';
    else loading.style = 'display: flex!important';
};

export const minSelectedCheckboxes = (min = 1) => {
    const validator: ValidatorFn = (formArray: FormArray) => {
        const totalSelected = formArray.controls
            .map(control => control.value)
            .reduce((prev, next) => (next ? prev + next : prev), 0);

        return totalSelected >= min ? null : { required: true };
    };

    return validator;
};

export const loadScript = (url: any) => {
    return new Promise(resolve => {
        const scriptElement = document.createElement('script');
        scriptElement.src = url;
        scriptElement.onload = resolve;
        document.body.appendChild(scriptElement);
    });
};

export const INTERCEPTOR_PORTAL_CLIENT = {
    provide: HTTP_INTERCEPTORS,
    useClass: AppHttpInterceptorClient,
    multi: true
};

// INTERCEPTOR ERROR FOR VIEW IN CONTENT
export const INTERCEPTOR_ERRORS = {
    provide: HTTP_INTERCEPTORS,
    useClass: AppHttpErrorHandle,
    multi: true
};

export const format = (s: any) => {
    const secs = s % 60;
    s = (s - secs) / 60;
    const mins = s % 60;
    const hrs = (s - mins) / 60;

    return (hrs > 9 ? hrs : '0' + hrs) + ':' + (mins > 9 ? mins : '0' + mins) + ':' + (secs > 9 ? secs : '0' + secs);
};

export const isEmptyObject = objct => {
    for (const member in objct) {
        if (objct[member] === null || objct[member] === '') {
            delete objct[member];
        }
    }
    return objct;
};

export const encryptJS = (value: any) => {
    return CryptoJS.AES.encrypt(JSON.stringify(value), environment.secreteKey).toString();
};

export const decryptJS = (value: any) => {
    const bytes = CryptoJS.AES.decrypt(value, environment.secreteKey);
    const userDecrypt = bytes.toString(CryptoJS.enc.Utf8);
    return JSON.parse(userDecrypt);
};

export const newGuid = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        const r = (Math.random() * 16) | 0,
            v = c === 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
};

export function fromEventValue(document: any, event: string): Observable<any> {
    return fromEvent(document, event).pipe(map((e: any) => e.target.value));
}


export function MapSubscription(): OperatorFunction<Object, any> {
    return map((res: any) =>
        res.map((x: any) => {
            x.amountRender = x.amount as number / 100;
            return x;
        })
    );
}