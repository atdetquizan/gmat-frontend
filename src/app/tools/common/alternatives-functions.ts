import { FormGroup, FormArray } from '@angular/forms';

export function isValidateAlternatives(form: FormGroup): boolean {
    const formArrayAlt = form.controls.alternatives as FormArray;
    for (const item of formArrayAlt.controls) {
        console.log(!(item as FormGroup).controls.answerId.valid);
        if (!(item as FormGroup).controls.answerId.valid) {
            return false;
        }
    }
    return true;
}

export function isValidateAlternative(form: FormGroup, index: any): any {
    const formArrayAlt = form.controls.alternatives as FormArray;
    if (formArrayAlt.length > 0) {
        const formGroupAlt = formArrayAlt.controls[index] as FormGroup;
        if (formGroupAlt) {
            return !formGroupAlt.controls.answerId.valid && formGroupAlt.controls.answerId.touched
                ? { 'text-danger': true }
                : null;
        }
    }
    return null;
}
