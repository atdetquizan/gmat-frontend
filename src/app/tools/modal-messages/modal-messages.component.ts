import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
    selector: 'gmat-modal-messages',
    templateUrl: './modal-messages.component.html',
    styleUrls: ['./modal-messages.component.less']
})
export class ModalMessagesComponent implements OnInit {

    title: string;
    message: string;
    constructor(private bsModalRef: BsModalRef) { }

    ngOnInit() {
    }

    onClickAccept() {
        this.bsModalRef.hide();
    }

}
