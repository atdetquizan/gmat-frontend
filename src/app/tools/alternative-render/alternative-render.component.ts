import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'gmat-alternative-render',
    templateUrl: './alternative-render.component.html',
    styleUrls: ['./alternative-render.component.less']
})
export class AlternativeRenderComponent implements OnInit {
    @Input() data: any;
    constructor() {}

    ngOnInit() {}
}
