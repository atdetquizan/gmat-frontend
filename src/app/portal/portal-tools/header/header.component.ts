import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { Router } from '@angular/router';
import { AuthEventService } from 'src/app/tools/services/auth-event.service';

@Component({
    selector: 'gmat-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.less']
})
export class HeaderComponent implements OnInit {

    infouser: any;
    constructor(
        private auth: AuthenticationService,
        private router: Router,
        private authEventService: AuthEventService
    ) {
        this.loadinfouser();
    }

    ngOnInit() {
        this.authEventService.changePhoto.subscribe((res) => {
            this.loadinfouser();
        });
    }

    onClickSignOut() {
        localStorage.removeItem('xser');
        this.router.navigate(['/']);
    }

    loadinfouser() {
        this.infouser = this.auth.getUserClient();
    }

}
