import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Inject, Input } from '@angular/core';
import { query } from '@angular/core/src/render3';
import { DOCUMENT } from '@angular/platform-browser';
import { formatMMSS } from 'src/app/tools/common/gmat-functions';

@Component({
    selector: 'gmat-video',
    templateUrl: './video.component.html',
    styleUrls: ['./video.component.less']
})
export class VideoComponent implements OnInit {
    juice: any;
    btn: any;
    juicebar: any;
    duration: number;
    timedurationcurrent: any;
    canvas: any;
    ctx: any;
    videoContainer: any;
    video: any;
    content: any;
    btnplay: any;
    videoPause = true;
    @Input() mediaSource: string;
    @Output() result: EventEmitter<any> = new EventEmitter();
    constructor(
        @Inject(DOCUMENT) private document: any
    ) {
    }

    ngOnInit() {
        if (this.mediaSource) {
            this.content = document.querySelector('.component-videos');
            this.canvas = document.querySelector(".video-item");
            this.ctx = this.canvas.getContext('2d');
            this.video = document.createElement('video');
            this.video.src = this.mediaSource;
            this.video.autoPlay = false; // ensure that the video does not auto play
            this.video.loop = false; // set the video to loop.
            this.video.muted = false;
            this.videoContainer = {  // we will add properties as needed
                video: this.video,
                ready: false,
            };
            this.eventVideos();
        }
        // this.eventJuicebar();
    }

    updateCanvas() {
        this.playStop(false);
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        if (this.videoContainer !== undefined && this.videoContainer.ready) {
            this.video.muted = false;
            this.ctx.drawImage(this.videoContainer.video, 0, 0, this.canvas.width, this.canvas.height);
            if (this.videoContainer.video.paused) {
                this.playStop(true);
            }
        }
        const a = requestAnimationFrame(() => this.updateCanvas());
        if (this.video.currentTime == this.video.duration) {
            window.cancelAnimationFrame(a);
        }
    }

    playStop(status: boolean) {
        if (this.videoPause) {
            this.btnplay = document.querySelector('.action-video-play');
            this.btnplay.style = status ? 'display: flex !important' : 'display: none !important';
        }
        this.btn.className = status ? 'play' : 'pause';
    }

    playPauseClick() {
        if (this.mediaSource) {
            this.videoPause = true;
            if (this.videoContainer !== undefined && this.videoContainer.ready) {
                if (this.videoContainer.video.paused) {
                    this.content.classList.add('content-vide-active');
                    this.videoContainer.video.play();
                } else {
                    this.content.classList.remove('content-vide-active');
                    this.videoContainer.video.pause();
                    this.result.emit({
                        play: this.video.ended ? false : true,
                        time: this.video.currentTime
                    });
                }
            }
        }
    }

    eventVideos() {
        this.juicebar = document.querySelector('.time-bar');
        this.juice = document.querySelector('.time-juice');
        this.timedurationcurrent = document.querySelector('.times-duration-current');
        this.btn = document.querySelector('.play-pause');

        // errors
        this.video.onerror = function (e) {
            // console.log(e);
        }

        // init video
        this.video.oncanplay = () => {
            this.videoContainer.scale = Math.min(
                this.canvas.width / this.video.videoWidth,
                this.canvas.height / this.video.videoHeight
            );
            this.videoContainer.ready = true;
            requestAnimationFrame(() => this.updateCanvas());
        };

        this.video.onend

        // video start
        this.video.onloadeddata = () => {
            this.duration = this.video.duration;
            this.timedurationcurrent.innerHTML = '00:00 / ' + formatMMSS(this.duration);
        };
        // time play
        this.video.ontimeupdate = () => {
            // console.log(this.video.ended);
            const juicePos = this.video.currentTime / this.video.duration;
            this.timedurationcurrent.innerHTML = formatMMSS(this.video.currentTime) + ' / ' + formatMMSS(this.duration);
            this.juice.style.width = juicePos * 100 + '%';
            if (this.video.ended) {
                this.btn.className = 'play';
            }
        };
        // video progress
        this.video.onprogress = (e) => {
            // target.buffered.end(0)
            // console.log(e);
        };
        // bar click time video
        this.juicebar.onclick = (e) => {
            const percent = e.offsetX / this.juicebar.offsetWidth;
            this.video.currentTime = percent * this.duration;
        };
        // tiempo info
        this.juicebar.onmousemove = (e) => {
            const timeinfo: any = document.querySelector('.time-info');
            const percent = e.offsetX / this.juicebar.offsetWidth;
            const percentTotal = percent * 100 - 4;
            timeinfo.innerHTML = formatMMSS(this.duration * (e.offsetX / this.juicebar.offsetWidth));
            timeinfo.style.left = percentTotal < 0 ? 0 : percentTotal > 93 ? 93 : percentTotal + '%';
        };
        // event change fullscreen
        this.document.onfullscreenchange = () => {
            if (!this.document.fullscreenElement) {
                this.content.classList.remove('expan-fullscreen')
            }
        };
        // canvas        
        this.canvas.onclick = () => this.playPauseClick();
    }

    onClickTogglePlayPause() {
        this.videoPause = false;
        if (this.video.paused) {
            this.btn.className = 'pause';
            this.video.play();
        } else {
            this.btn.className = 'play';
            this.video.pause();
            this.result.emit({
                play: this.video.ended ? false : true,
                time: this.video.currentTime
            });
        }
    }

    onToggleFullScree() {
        let elem: any = document.querySelector(".component-videos");
        if (!this.document.fullscreenElement) {
            this.content.classList.add('expan-fullscreen')
            elem.requestFullscreen().then({}).catch(err => {
                console.log(`Error attempting to enable full-screen mode: ${err.message} (${err.name})`);
            });
        } else {
            this.content.classList.remove('expan-fullscreen')
            this.document.exitFullscreen();
        }
    }
}
