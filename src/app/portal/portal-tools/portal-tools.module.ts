import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { VideoComponent } from './video/video.component';
import { RouterModule } from '@angular/router';
import { ContentComponent } from './content/content.component';
import { INTERCEPTOR_PORTAL_CLIENT } from 'src/app/tools/common/gmat-functions';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        HttpClientModule
    ],
    declarations: [
        HeaderComponent,
        VideoComponent,
        ContentComponent
    ],
    exports: [
        HeaderComponent,
        VideoComponent,
        ContentComponent
    ],
    providers: [
        AuthenticationService,
        INTERCEPTOR_PORTAL_CLIENT,
        // AuthGuardClient,
        // { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorClient, multi: true }
    ]
})
export class PortalToolsModule { }
