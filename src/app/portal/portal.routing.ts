import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortalComponent } from './portal.component';
import { AuthActivePortalGuard } from '../tools/guards/auth-active-portal.guard';

const routes: Routes = [
    {
        path: '',
        component: PortalComponent,
        canActivateChild: [AuthActivePortalGuard],
        children: [
            {
                path: 'home',
                loadChildren: './portal-home/portal-home.module#PortalHomeModule'
            },
            {
                path: 'platform',
                loadChildren: './portal-platform/portal-platform.module#PortalPlatformModule'
            },
            {
                path: 'profile',
                loadChildren: './portal-profile/portal-profile.module#PortalProfileModule'
            },
            {
                path: '',
                redirectTo: 'platform'
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PortalRouting { }

