import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortalProfileComponent } from './portal-profile.component';
import { PortalProfileIndexComponent } from './portal-profile-index/portal-profile-index.component';
import { PortalProfileClipnotesComponent } from './portal-profile-clipnotes/portal-profile-clipnotes.component';
import { PortalProfileVideosComponent } from './portal-profile-videos/portal-profile-videos.component';
import { PortalProfileYourplanComponent } from './portal-profile-yourplan/portal-profile-yourplan.component';
import { PortalProfileScoreComponent } from './portal-profile-score/portal-profile-score.component';

const routes: Routes = [
    {
        path: '',
        component: PortalProfileComponent,
        children: [
            {
                path: '',
                component: PortalProfileIndexComponent
            },
            {
                path: 'videos',
                component: PortalProfileVideosComponent
            },
            {
                path: 'clipnotes/:id',
                component: PortalProfileClipnotesComponent
            },
            {
                path: 'upgrade-plan',
                component: PortalProfileYourplanComponent
            },
            {
                path: 'target-score',
                component: PortalProfileScoreComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PortalProfileRouting { }
