import { Component, OnInit, DoCheck } from '@angular/core';
import { AdminPricingService } from 'src/app/admin/admin-pricing/admin-pricing.service';
import { MapSubscription, loadScript } from 'src/app/tools/common/gmat-functions';
import { environment } from 'src/environments/environment';
import { PricingType } from 'src/app/tools/types/pricing.type';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { AdminUsersService } from 'src/app/admin/admin-users/admin-users.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { Router } from '@angular/router';

declare const Culqi: any;

@Component({
    selector: 'gmat-portal-profile-yourplan',
    templateUrl: './portal-profile-yourplan.component.html',
    styleUrls: ['./portal-profile-yourplan.component.less']
})
export class PortalProfileYourplanComponent implements OnInit, DoCheck {
    subscription: any;
    subscriptions: any;
    culqiInit: any;
    // hasCard = false;
    checkcard = false;
    card: any;
    type: PricingType;
    form: FormGroup;
    user: any;
    constructor(
        private authenticationService: AuthenticationService,
        private adminUsersService: AdminUsersService,
        private adminPricingService: AdminPricingService,
        private fb: FormBuilder,
        private loadingBarService: LoadingBarService,
        private sweetalertService: SweetalertService,
        private router: Router
    ) {
        this.user = this.authenticationService.getUserClient().user;
        this.createform();
    }

    ngOnInit() {
        this.showData();
        // this.showCardQulqi();showCardQulqi()
        loadScript('https://checkout.culqi.com/js/v3').then(e => {
            this.culqiInit = Culqi;
            this.culqiInit.publicKey = environment.qulqiKey;
        });
    }

    ngDoCheck() {
        if (this.culqiInit && this.culqiInit.token) {
            this.checkcard = true;
            this.card = {};
            this.card.number = this.culqiInit.token.card_number;
            this.card.tokenId = this.culqiInit.token.id;
            this.form.controls.tokenId.setValue(this.culqiInit.token.id);
        }
    }

    onChangeSubscription(item: any) {
        console.log(item);
        this.subscription = item;
        this.type = item.type;
        this.culqiInit.settings({
            title: item.name,
            currency: item.currency_code,
            description: item.description,
            amount: item.amount
        });
        this.form.controls.subscriptionId.setValue(item.id);
    }

    onClickPaymet(e) {
        e.preventDefault();
        if (!this.checkcard) {
            this.sweetalertService.swal({
                title: 'Warning',
                text: 'You must enter a credit card',
                icon: environment.messages.iconwarning
            });
            return;
        }
        this.loadingBarService.start();
        this.adminUsersService.postSubscriptionRenew(this.form.value).subscribe((res: any) => {
            this.loadingBarService.complete();
            this.sweetalertService.swal({
                title: environment.messages.save.title,
                text: res.message,
                icon: environment.messages.iconsuccess
            }).then(() => {
                this.router.navigate(['../portal/profile']);
            })
        }, (error) => {
            this.loadingBarService.complete();
            this.sweetalertService.swal({
                title: environment.messages.error.title,
                text: error,
                icon: environment.messages.iconerror
            })
        });
    }

    onClickAddPaymet(e) {
        this.culqiInit.open();
        e.preventDefault();
    }

    // private showCardQulqi() {
    //     this.adminUsersService.postCardQulqi(this.user.id).subscribe((res: any) => {
    //         console.log(res);
    //         if (res.culqi_card_id) {
    //             this.hasCard = true;
    //         }
    //     });
    // }

    private showData() {
        this.adminPricingService
            .getByUserId(this.user.id)
            .pipe(MapSubscription())
            .subscribe(res => {
                this.subscriptions = res;
            });
    }

    private createform() {
        this.form = this.fb.group({
            clientId: [this.user.id, null],
            subscriptionId: [null, null],
            tokenId: [null, null],
            cardId: [null, null]
        });
    }
}
