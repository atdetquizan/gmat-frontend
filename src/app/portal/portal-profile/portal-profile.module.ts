import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { PortalToolsModule } from './../portal-tools/portal-tools.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalProfileComponent } from './portal-profile.component';
import { PortalProfileRouting } from './portal-profile.routing';
import { PortalProfileIndexComponent } from './portal-profile-index/portal-profile-index.component';
import { PortalProfileClipnotesComponent } from './portal-profile-clipnotes/portal-profile-clipnotes.component';
import { NguCarouselModule } from '@ngu/carousel';
import { PortalProfileVideosComponent } from './portal-profile-videos/portal-profile-videos.component';
import { AdminUsersService } from 'src/app/admin/admin-users/admin-users.service';
import { AdminUsersEditModule } from 'src/app/admin/admin-users/admin-users-edit/admin-users-edit.module';
import { ModalModule } from 'ngx-bootstrap';
import { PortalProfileScoreComponent } from './portal-profile-score/portal-profile-score.component';
import { PortalProfileYourplanComponent } from './portal-profile-yourplan/portal-profile-yourplan.component';
import { AdminVideolessonsService } from 'src/app/admin/admin-videolessons/admin-videolessons.service';
import { PortalPlatformLessonsClipnotesService } from '../portal-platform/portal-shared/portal-platform-lessons-clipnotes.service';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { INTERCEPTOR_PORTAL_CLIENT, INTERCEPTOR_ERRORS } from 'src/app/tools/common/gmat-functions';
import { HttpClientModule } from '@angular/common/http';
import { AdminPricingService } from 'src/app/admin/admin-pricing/admin-pricing.service';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

const PROFILE_PROVIDERES = [
    AdminUsersService,
    AdminVideolessonsService,
    AuthenticationService,
    PortalPlatformLessonsClipnotesService,
    AdminPricingService
]

@NgModule({
    imports: [
        CommonModule,
        PortalProfileRouting,
        PortalToolsModule,
        PerfectScrollbarModule,
        NguCarouselModule,
        ModalModule.forRoot(),
        AdminUsersEditModule,
        HttpClientModule
    ],
    declarations: [
        PortalProfileComponent,
        PortalProfileIndexComponent,
        PortalProfileClipnotesComponent,
        PortalProfileVideosComponent,
        PortalProfileScoreComponent,
        PortalProfileYourplanComponent
    ],
    entryComponents: [
    ],
    providers: [
        PROFILE_PROVIDERES,
        INTERCEPTOR_ERRORS,
        INTERCEPTOR_PORTAL_CLIENT,
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ]
})
export class PortalProfileModule { }
