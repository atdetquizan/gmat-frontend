import { Component, OnInit } from '@angular/core';
import { AdminUsersService } from 'src/app/admin/admin-users/admin-users.service';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import Highcharts from 'highcharts';
declare var require: any;
require('highcharts/highcharts-more')(Highcharts);

@Component({
    selector: 'gmat-portal-profile-score',
    templateUrl: './portal-profile-score.component.html',
    styleUrls: ['./portal-profile-score.component.less']
})
export class PortalProfileScoreComponent implements OnInit {
    user: any;
    sections: any;
    constructor(
        private adminUsersService: AdminUsersService,
        private authenticationService: AuthenticationService
    ) {
        this.user = this.authenticationService.getUserClient().user;
    }

    ngOnInit() {
        this.showData();
    }

    loadGraphics(setting: any) {
        Highcharts.chart('chart-demo', {
            chart: {
                polar: true,
            },

            credits: {
                enabled: false
            },

            title: {
                text: setting.title
            },

            pane: {
                size: '80%'
            },

            xAxis: {
                categories: setting.categories,
                tickmarkPlacement: 'on',
                lineWidth: 0
            },

            yAxis: {
                gridLineInterpolation: 'polygon',
                lineWidth: 0,
                min: 0
            },

            tooltip: {
                shared: true,
                pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}%</b><br/>'
            },

            legend: {
                verticalAlign: 'bottom',
            },

            series: [{
                name: 'See target score',
                data: setting.data,
                pointPlacement: 'on',
                color: '#ffb536',
                events: {
                    click: (e) => {
                        if (setting.isFather)
                            this.showDetailData(this.sections[e.point.index].id);
                    }
                }
            }]
        });
    }

    private showData() {
        this.adminUsersService.getReportsList(this.user.id)
            .subscribe((res: any) => {
                this.sections = res;
                this.loadGraphics({
                    title: 'Sections',
                    categories: res.map((x) => x.sectionName),
                    data: res.map((x) => x.knowledgePercent * 100),
                    isFather: true
                });
            });
    }

    private showDetailData(sectionId: any) {
        this.adminUsersService.getReportsDetailList(sectionId)
            .subscribe((res: any) => {
                this.loadGraphics({
                    title: 'Topics',
                    categories: res.map((x) => x.sectionName),
                    data: res.map((x) => x.knowledgePercent * 100),
                    isFather: false
                });
            });
    }
}
