import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { toDataURL, $ } from 'src/app/tools/common/gmat-functions';
import { AdminUsersService } from 'src/app/admin/admin-users/admin-users.service';
import { BsModalService } from 'ngx-bootstrap';
import { AdminUsersEditComponent } from 'src/app/admin/admin-users/admin-users-edit/admin-users-edit.component';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { AuthEventService } from 'src/app/tools/services/auth-event.service';

@Component({
    selector: 'gmat-portal-profile-index',
    templateUrl: './portal-profile-index.component.html',
    styleUrls: ['./portal-profile-index.component.less']
})
export class PortalProfileIndexComponent implements OnInit {

    userinfo: any;
    constructor(
        private auth: AuthenticationService,
        private adminUsersService: AdminUsersService,
        private modalService: BsModalService,
        private loadingBarService: LoadingBarService,
        private authEventService: AuthEventService
    ) {
        this.userinfo = this.auth.getUserClient().user;
    }

    ngOnInit() {
        this.authEventService.changePhoto.subscribe(() => {
            this.userinfo = this.auth.getUserClient().user;
        });
    }

    onClickChangeInfo() {
        const initialState = {
            title: 'Update Information',
            user: this.userinfo
        };
        const modal = this.modalService.show(AdminUsersEditComponent, {
            class: 'modal-create-section modal-dialog-centered',
            initialState
        });
        modal.content.eventClosed.subscribe((res: boolean) => {
            if (res) {
                this.userinfo = this.auth.getUserClient().user;
            }
        });
    }

    onChangeFile(event: any) {
        const file = event.target.files[0];
        toDataURL(file)
            .then((dataUrl: any) => {
                this.loadingBarService.start();
                const base64 = dataUrl.target.result;
                // const image = $("#user-img-profile");
                // image.setAttribute('src', base64);
                this.adminUsersService.patch(this.userinfo.id, {
                    imageName: file.name,
                    imageFile: base64.substr(base64.indexOf(',') + 1)
                }).subscribe((res) => {
                    this.auth.updateUserClient(res);
                    this.authEventService.changePhoto.emit(true);
                    this.loadingBarService.complete();
                })
            });
    }
}
