import { Component, OnInit } from '@angular/core';
import { PortalPlatformLessonsClipnotesService } from '../../portal-platform/portal-shared/portal-platform-lessons-clipnotes.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { formatMMSS } from 'src/app/tools/common/gmat-functions';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'gmat-portal-profile-clipnotes',
    templateUrl: './portal-profile-clipnotes.component.html',
    styleUrls: ['./portal-profile-clipnotes.component.less']
})
export class PortalProfileClipnotesComponent implements OnInit {

    videoId: number;
    video: any;
    clipnotes: any = [];
    constructor(
        private activatedRoute: ActivatedRoute,
        private portalPlatformLessonsClipnotesService: PortalPlatformLessonsClipnotesService,
        private authenticationService: AuthenticationService,
        private sweetalertService: SweetalertService
    ) { }

    ngOnInit() {
        this.videoId = this.activatedRoute.snapshot.params.id;
        this.showClipnotes();
    }

    onClickDelete(clipnoteId: number) {
        this.sweetalertService.swal({
            title: environment.messages.delete.title,
            text: environment.messages.delete.text,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                this.portalPlatformLessonsClipnotesService.delete(clipnoteId)
                    .subscribe((res: any) => {
                        this.sweetalertService.swal(environment.messages.delete.text2, {
                            icon: "success",
                        });
                        this.showClipnotes();
                    })
            }
        });
    }

    formattime(time: any) {
        return formatMMSS(time) + ' min';
    }

    showClipnotes() {
        const user = this.authenticationService.getUserClient().user;
        this.portalPlatformLessonsClipnotesService.get(this.videoId, user.id)
            .subscribe((res: any) => {
                this.video = res.video;
                this.clipnotes = res.clipnotes;
            });
    }


}
