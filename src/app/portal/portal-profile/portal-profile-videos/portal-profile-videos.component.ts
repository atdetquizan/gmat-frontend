import { Component, OnInit, ViewChild } from '@angular/core';
import { NguCarouselConfig, NguCarouselStore } from '@ngu/carousel';
import { AdminVideolessonsService } from 'src/app/admin/admin-videolessons/admin-videolessons.service';
import { AuthenticationService } from 'src/app/authentication/authentication.service';

@Component({
    selector: 'gmat-portal-profile-videos',
    templateUrl: './portal-profile-videos.component.html',
    styleUrls: ['./portal-profile-videos.component.less']
})
export class PortalProfileVideosComponent implements OnInit {
    videos: any;
    constructor(
        private authenticationService: AuthenticationService,
        private adminVideolessonsService: AdminVideolessonsService
    ) {
    }

    ngOnInit() {
        this.showVideos();
    }

    showVideos() {
        let user = this.authenticationService.getUserClient().user;
        console.log(user);
        this.adminVideolessonsService.getByClientId({
            clientId: user.id
        }).subscribe((res: any) => {
            this.videos = res;
        });
    }
}

