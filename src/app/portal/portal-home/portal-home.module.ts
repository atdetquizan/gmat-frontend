import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalHomeComponent } from './portal-home.component';
import { PortalHomeRouting } from './portal-home.routing';

@NgModule({
    imports: [
        CommonModule,
        PortalHomeRouting
    ],
    declarations: [
        PortalHomeComponent
    ]
})
export class PortalHomeModule { }
