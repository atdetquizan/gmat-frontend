import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortalHomeComponent } from './portal-home.component';

const routes: Routes = [
    {
        path: '',
        component: PortalHomeComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PortalHomeRouting { }

