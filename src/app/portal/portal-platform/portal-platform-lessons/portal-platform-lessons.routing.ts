import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortalPlatformLessonsComponent } from './portal-platform-lessons.component';
const routes: Routes = [
    {
        path: '',
        component: PortalPlatformLessonsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PortalPlatformLessonsRouting { }
