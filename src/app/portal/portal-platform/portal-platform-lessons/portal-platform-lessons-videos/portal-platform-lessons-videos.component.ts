import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { TypeLessons } from '../portal-platform-lessons-shared/TypeLessons.enum';

@Component({
    selector: 'gmat-portal-platform-lessons-videos',
    templateUrl: './portal-platform-lessons-videos.component.html',
    styleUrls: ['./portal-platform-lessons-videos.component.less']
})
export class PortalPlatformLessonsVideosComponent implements OnInit {

    parameters: any;
    videos: any = [];
    navigation: any[] = [];
    @Output() eventStart: EventEmitter<any> = new EventEmitter();
    constructor() { }

    ngOnInit() {
        this.videos = this.parameters.videos;
    }

    clickBack(item: any) {
        const obj: any = {
            data: item.parameters,
            component: item.component
        };
        if (item.navigation)
            obj.navigation = item.navigation
        this.eventStart.emit(obj);
    }

    onClickDetailVideoLesson(item: any) {
        item.component = TypeLessons.videos;
        item.parameters = this.parameters;
        item.navigation = this.navigation;
        item.name = item.title;
        let newnavigation = [...this.navigation, ...item];
        this.eventStart.emit({
            data: item,
            component: TypeLessons.clipnotes,
            navigation: newnavigation
        });
    }

}
