import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PortalPlatformService } from '../../portal-platform.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NguCarouselConfig } from '@ngu/carousel';
import { TypeLessons } from '../portal-platform-lessons-shared/TypeLessons.enum';
import { AdminSectionService } from 'src/app/admin/admin-section/admin-section.service';

@Component({
    selector: 'gmat-portal-platform-lessons-sections',
    templateUrl: './portal-platform-lessons-sections.component.html',
    styleUrls: ['./portal-platform-lessons-sections.component.less']
})
export class PortalPlatformLessonsSectionsComponent implements OnInit {
    parameters: any;
    sections: any;
    form: FormGroup;
    @Output() eventStart: EventEmitter<any> = new EventEmitter();
    public carouselTileItems: Array<any>;
    public carouselTile: NguCarouselConfig = {
        grid: { xs: 1, sm: 1, md: 4, lg: 4, all: 0 },
        slide: 4,
        speed: 250,
        point: {
            visible: false
        },
        load: 2,
        velocity: 0,
        touch: true,
        easing: 'cubic-bezier(0, 0, 0.2, 1)'
    };

    constructor(
        private adminSectionService: AdminSectionService,
        private fb: FormBuilder
    ) {
        this.createform();
        this.showData();
    }

    ngOnInit() {
        //
    }

    clickStart() {
        const item = this.form.value.section;
        item.component = TypeLessons.sections;
        this.eventStart.emit({
            data: item,
            component: item.hasNextNode ? TypeLessons.topics : TypeLessons.videos,
            navigation: [item]
        });
    }

    showData() {
        this.adminSectionService.get().subscribe((res: any) => {
            this.sections = res;
            this.carouselTileItems = Array(res.length);
        });
    }

    private createform() {
        this.form = this.fb.group({
            section: [null, [Validators.required]]
        });
    }

}
