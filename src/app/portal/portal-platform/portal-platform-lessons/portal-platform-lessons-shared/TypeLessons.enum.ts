export enum TypeLessons {
    sections = 1,
    topics = 2,
    subjects = 3,
    subsubjects = 4,
    videos = 5,
    clipnotes = 6
}
