import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[ad-lessons]'
})
export class AdLessonsDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
