import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AdminSectionService } from 'src/app/admin/admin-section/admin-section.service';
import { TypeLessons } from '../portal-platform-lessons-shared/TypeLessons.enum';

@Component({
    selector: 'gmat-portal-platform-lessons-topics',
    templateUrl: './portal-platform-lessons-topics.component.html',
    styleUrls: ['./portal-platform-lessons-topics.component.less']
})
export class PortalPlatformLessonsTopicsComponent implements OnInit {
    parameters: any;
    topics: any;
    navigation: any[];
    @Output() eventStart: EventEmitter<any> = new EventEmitter();
    constructor(
        private adminSectionService: AdminSectionService
    ) { }

    ngOnInit() {
        this.showData();
    }

    clickBack(item: any) {
        const obj: any = {
            data: item.parameters,
            component: item.component
        };
        if (item.navigation)
            obj.navigation = item.navigation
        this.eventStart.emit(obj);
    }

    clickStart(item: any) {
        item.component = TypeLessons.topics;
        item.parameters = this.parameters;
        item.navigation = this.navigation;
        this.eventStart.emit({
            data: item,
            component: item.hasNextNode ? TypeLessons.subjects : TypeLessons.videos,
            navigation: [...this.navigation, item]
        });
    }

    showData() {
        this.adminSectionService.getById(this.parameters.id).subscribe((res: any) => {
            this.topics = res.detail;
        });
    }
}
