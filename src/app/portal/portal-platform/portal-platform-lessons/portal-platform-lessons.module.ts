import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalPlatformLessonsComponent } from './portal-platform-lessons.component';
import { PortalPlatformLessonsClipnotesComponent } from './portal-platform-lessons-clipnotes/portal-platform-lessons-clipnotes.component';
import { PortalPlatformLessonsVideosComponent } from './portal-platform-lessons-videos/portal-platform-lessons-videos.component';
import { PortalPlatformLessonsSubsubjectsComponent } from './portal-platform-lessons-subsubjects/portal-platform-lessons-subsubjects.component';
import { PortalPlatformLessonsSubjectsComponent } from './portal-platform-lessons-subjects/portal-platform-lessons-subjects.component';
import { PortalPlatformLessonsTopicsComponent } from './portal-platform-lessons-topics/portal-platform-lessons-topics.component';
import { PortalPlatformLessonsSectionsComponent } from './portal-platform-lessons-sections/portal-platform-lessons-sections.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { NguCarouselModule } from '@ngu/carousel';
import { AdLessonsDirective } from './portal-platform-lessons-shared/ad-lessons.directive';
import { PortalPlatformLessonsRouting } from './portal-platform-lessons.routing';
import { AdminSectionService } from 'src/app/admin/admin-section/admin-section.service';
import { AdminTopicsService } from 'src/app/admin/admin-topics/admin-topics.service';
import { AdminSubjectsService } from 'src/app/admin/admin-subjects/admin-subjects.service';
import { AdminSubsubjectsService } from 'src/app/admin/admin-subsubjects/admin-subsubjects.service';
import { PortalPlatformLessonsClipnotesService } from '../portal-shared/portal-platform-lessons-clipnotes.service';
import { AdminUsersService } from 'src/app/admin/admin-users/admin-users.service';
import { PortalToolsModule } from '../../portal-tools/portal-tools.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { INTERCEPTOR_PORTAL_CLIENT } from '../../../tools/common/gmat-functions';


const PROVIDERS_PORTAL_PLATFORM = [
    // PortalPlatformService,
    AdminSectionService,
    AdminTopicsService,
    AdminSubjectsService,
    AdminSubsubjectsService,
    PortalPlatformLessonsClipnotesService,
    // AdminQuestionsService,
    AdminUsersService
];

@NgModule({
    imports: [
        CommonModule,
        PortalPlatformLessonsRouting,
        HttpClientModule,
        ReactiveFormsModule,
        NguCarouselModule,
        PortalToolsModule,
        PerfectScrollbarModule,
        ControlMessagesModule
    ],
    declarations: [
        PortalPlatformLessonsComponent,
        PortalPlatformLessonsSectionsComponent,
        PortalPlatformLessonsTopicsComponent,
        PortalPlatformLessonsSubjectsComponent,
        PortalPlatformLessonsSubsubjectsComponent,
        PortalPlatformLessonsVideosComponent,
        PortalPlatformLessonsClipnotesComponent,
        AdLessonsDirective,
    ],
    entryComponents: [
        PortalPlatformLessonsComponent,
        PortalPlatformLessonsSectionsComponent,
        PortalPlatformLessonsTopicsComponent,
        PortalPlatformLessonsSubjectsComponent,
        PortalPlatformLessonsSubsubjectsComponent,
        PortalPlatformLessonsVideosComponent,
        PortalPlatformLessonsClipnotesComponent
    ],
    providers: [
        PROVIDERS_PORTAL_PLATFORM,
        INTERCEPTOR_PORTAL_CLIENT,
    ]
})
export class PortalPlatformLessonsModule { }
