import { Component, OnInit, EventEmitter, Output } from '@angular/core';
// import { AdminSubjectsService } from 'src/app/admin/admin-subjects/admin-subjects.service';
import { AdminTopicsService } from 'src/app/admin/admin-topics/admin-topics.service';
import { TypeLessons } from '../portal-platform-lessons-shared/TypeLessons.enum';
// import { PortalPlatformLessonsSubsubjectsComponent } from '../portal-platform-lessons-subsubjects/portal-platform-lessons-subsubjects.component';
// import { PortalPlatformLessonsSectionsComponent } from '../portal-platform-lessons-sections/portal-platform-lessons-sections.component';

@Component({
    selector: 'gmat-portal-platform-lessons-subjects',
    templateUrl: './portal-platform-lessons-subjects.component.html',
    styleUrls: ['./portal-platform-lessons-subjects.component.less']
})
export class PortalPlatformLessonsSubjectsComponent implements OnInit {

    parameters: any;
    topics: any;
    navigation: any[];
    @Output() eventStart: EventEmitter<any> = new EventEmitter();
    constructor(
        private adminTopicsService: AdminTopicsService
    ) { }

    ngOnInit() {
        this.showData();
    }

    clickBack(item: any) {
        const obj: any = {
            data: item.parameters,
            component: item.component
        };
        if (item.navigation)
            obj.navigation = item.navigation
        this.eventStart.emit(obj);
    }

    clickStart(item: any) {
        item.component = TypeLessons.subjects;
        item.parameters = this.parameters;
        item.navigation = this.navigation;
        let newnavigation = [...this.navigation, ...item];
        this.eventStart.emit({
            data: item,
            component: item.hasNextNode ? TypeLessons.subsubjects : TypeLessons.videos,
            navigation: newnavigation
        });
    }

    showData() {
        this.adminTopicsService.getById(this.parameters.id).subscribe((res: any) => {
            this.topics = res.detail;
        });
    }
}
