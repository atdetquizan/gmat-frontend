import { Component, OnInit, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { TypeLessons } from './portal-platform-lessons-shared/TypeLessons.enum';
import { PortalPlatformLessonsSectionsComponent } from './portal-platform-lessons-sections/portal-platform-lessons-sections.component';
import { PortalPlatformLessonsTopicsComponent } from './portal-platform-lessons-topics/portal-platform-lessons-topics.component';
import { PortalPlatformLessonsSubjectsComponent } from './portal-platform-lessons-subjects/portal-platform-lessons-subjects.component';
import { PortalPlatformLessonsClipnotesComponent } from './portal-platform-lessons-clipnotes/portal-platform-lessons-clipnotes.component';
import { PortalPlatformLessonsVideosComponent } from './portal-platform-lessons-videos/portal-platform-lessons-videos.component';
import { PortalPlatformLessonsSubsubjectsComponent } from './portal-platform-lessons-subsubjects/portal-platform-lessons-subsubjects.component';
import { AdLessonsDirective } from './portal-platform-lessons-shared/ad-lessons.directive';

@Component({
    selector: 'gmat-portal-platform-lessons',
    templateUrl: './portal-platform-lessons.component.html',
    styleUrls: ['./portal-platform-lessons.component.less']
})
export class PortalPlatformLessonsComponent implements OnInit {

    @ViewChild(AdLessonsDirective) adHost: AdLessonsDirective;
    constructor(
        private componentFactoryResolver: ComponentFactoryResolver
    ) { }

    ngOnInit() {
        this.loadComponent(TypeLessons.sections);
    }

    loadComponent(typeLessons: TypeLessons, parameters?: any, navigation?: any) {
        const component: any = this.searchCompoentn(typeLessons);
        let componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);

        let viewContainerRef = this.adHost.viewContainerRef;
        viewContainerRef.clear();

        let componentRef = viewContainerRef.createComponent(componentFactory);
        let componentInstance = (<any>componentRef.instance);

        if (parameters) {
            componentInstance.parameters = parameters;
            parameters.typeLessons = typeLessons;
        }
        if (navigation)
            componentInstance.navigation = navigation;

        componentInstance.eventStart.subscribe((res) => {
            this.loadComponent(res.component, res.data, res.navigation);
        });
    }

    searchCompoentn(typeLessons: TypeLessons) {
        switch (typeLessons) {
            case TypeLessons.sections:
                return PortalPlatformLessonsSectionsComponent;
            case TypeLessons.topics:
                return PortalPlatformLessonsTopicsComponent;
            case TypeLessons.subjects:
                return PortalPlatformLessonsSubjectsComponent;
            case TypeLessons.subsubjects:
                return PortalPlatformLessonsSubsubjectsComponent;
            case TypeLessons.videos:
                return PortalPlatformLessonsVideosComponent;
            case TypeLessons.clipnotes:
                return PortalPlatformLessonsClipnotesComponent;
        }
    }


}
