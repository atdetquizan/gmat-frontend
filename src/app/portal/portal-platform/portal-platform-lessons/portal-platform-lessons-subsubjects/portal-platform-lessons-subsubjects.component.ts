import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AdminSubjectsService } from 'src/app/admin/admin-subjects/admin-subjects.service';
import { TypeLessons } from '../portal-platform-lessons-shared/TypeLessons.enum';


@Component({
    selector: 'gmat-portal-platform-lessons-subsubjects',
    templateUrl: './portal-platform-lessons-subsubjects.component.html',
    styleUrls: ['./portal-platform-lessons-subsubjects.component.less']
})
export class PortalPlatformLessonsSubsubjectsComponent implements OnInit {

    parameters: any;
    subsubject: any;
    navigation: any[];
    @Output() eventStart: EventEmitter<any> = new EventEmitter();
    constructor(
        private adminSubjectsService: AdminSubjectsService,
    ) { }

    ngOnInit() {
        this.showData();
    }

    clickBackSections() {
        this.eventStart.emit({
            component: TypeLessons.sections
        })
    }

    clickBack(item: any) {
        const obj: any = {
            data: item.parameters,
            component: item.component
        };
        if (item.navigation)
            obj.navigation = item.navigation
        this.eventStart.emit(obj);
    }

    clickStart(item: any) {
        item.component = TypeLessons.subsubjects;
        item.parameters = this.parameters;
        item.navigation = this.navigation;
        let newnavigation = [...this.navigation, ...item];
        this.eventStart.emit({
            data: item,
            component: TypeLessons.videos,
            navigation: newnavigation
        });
    }

    showData() {
        this.adminSubjectsService.getById(this.parameters.id).subscribe((res: any) => {
            this.subsubject = res.detail;
        });
    }

}
