import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { formatMMSS } from 'src/app/tools/common/gmat-functions';
import { PortalPlatformLessonsClipnotesService } from '../../portal-shared/portal-platform-lessons-clipnotes.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { environment } from 'src/environments/environment.prod';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';

@Component({
    selector: 'gmat-portal-platform-lessons-clipnotes',
    templateUrl: './portal-platform-lessons-clipnotes.component.html',
    styleUrls: ['./portal-platform-lessons-clipnotes.component.less']
})
export class PortalPlatformLessonsClipnotesComponent implements OnInit {
    parameters: any;
    navigation: any[];
    currentTime = '00:00 min';
    form: FormGroup;
    clipnotes: any;
    @Output() eventStart: EventEmitter<any> = new EventEmitter();
    @ViewChild(PerfectScrollbarComponent) componentRef?: PerfectScrollbarComponent;
    constructor(
        private portalPlatformLessonsClipnotesService: PortalPlatformLessonsClipnotesService,
        private auth: AuthenticationService,
        private sweetalertService: SweetalertService,
        private fb: FormBuilder
    ) {
        this.createform();
    }

    ngOnInit() {
        this.showData();
    }

    clickBack(item: any) {
        const obj: any = {
            data: item.parameters,
            component: item.component
        };
        if (item.navigation)
            obj.navigation = item.navigation
        this.eventStart.emit(obj);
    }

    onClickSave() {
        this.portalPlatformLessonsClipnotesService.post(this.form.value).subscribe((res: any) => {
            this.sweetalertService.swal({
                title: environment.messages.save.title,
                text: environment.messages.save.text,
                icon: environment.messages.iconsuccess
            }).then(() => {
                this.form.controls.description.setValue('');
                this.showData();
                // this.currentTime = '00:00 min';
            })
        });
    }

    onChangeTimeOut(video: any) {
        const userinfo = this.auth.getUserClient();
        this.currentTime = formatMMSS(video.time) + ' min';
        this.form.controls.clientId.setValue(userinfo.user.id);
        this.form.controls.videoId.setValue(this.parameters.id);
        this.form.controls.seconds.setValue(video.time);
    }

    formattime(time: any) {
        return formatMMSS(time) + ' min';
    }

    private showData() {
        const userinfo = this.auth.getUserClient();
        this.portalPlatformLessonsClipnotesService.get(this.parameters.id, userinfo.user.id).subscribe((res: any) => {
            if (res.clipnotes) {
                this.clipnotes = res.clipnotes;
                setTimeout(() => {
                    this.componentRef.directiveRef.scrollToBottom()
                }, 2000);
            }
        })
    }
    private createform() {
        this.form = this.fb.group({
            clientId: [null, [Validators.required]],
            videoId: [null, [Validators.required]],
            description: ['', [Validators.required, Validators.maxLength(145)]],
            seconds: [null, [Validators.required]]
        })
    }
}
