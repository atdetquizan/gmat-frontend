import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/authentication/authentication.service';

@Component({
    selector: 'gmat-portal-platform',
    templateUrl: './portal-platform.component.html',
    styleUrls: ['./portal-platform.component.less']
})
export class PortalPlatformComponent implements OnInit {
    hasActive = true;
    user: any;
    constructor(private authenticationService: AuthenticationService) {}
    ngOnInit() {
        this.user = this.authenticationService.getUserClient().user;
        this.authenticationService.postCheckClient(this.user.id).subscribe((res: any) => {
            if (res.subscription !== 'active') {
                this.hasActive = false;
            }
        });
    }
}
