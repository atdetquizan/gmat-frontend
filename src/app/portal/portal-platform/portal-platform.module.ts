import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalPlatformComponent } from './portal-platform.component';
import { PortalPlatformRouting } from './portal-platform.routing';
import { PortalToolsModule } from '../portal-tools/portal-tools.module';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { INTERCEPTOR_PORTAL_CLIENT } from 'src/app/tools/common/gmat-functions';

// const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
//     suppressScrollX: true
// };


@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        PortalPlatformRouting,
        // TabsModule.forRoot(),
        // PortalToolsModule,
        // PerfectScrollbarModule,
        // CollapseModule.forRoot(),
        // ReactiveFormsModule,
        // NguCarouselModule,
        // ControlMessagesModule,
        // AccordionModule.forRoot(),
        // ModalModule.forRoot(),
    ],
    declarations: [
        PortalPlatformComponent,
        // PortalPlatformPracticeComponent,
        // PortalPlatformPracticeConfigComponent,
        // PortalPlatformPracticeExamComponent,
        // PortalPlatformPracticeSettingComponent,  
        // AdPracticeDirective
    ],
    entryComponents: [
        // PortalPlatformPracticeConfigComponent,
        // PortalPlatformPracticeSettingComponent,
    ],
    providers: [
        AuthenticationService,
        INTERCEPTOR_PORTAL_CLIENT
    ]
})
export class PortalPlatformModule { }
