import { Component, OnInit } from '@angular/core';
import { QuestionFormatType } from 'src/app/admin/admin-questions/admin-questions-shared/question-format-type.enum';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { minSelectedCheckboxes } from 'src/app/tools/common/gmat-functions';
import { BsModalRef } from 'ngx-bootstrap';
import { AdminQuestionsService } from 'src/app/admin/admin-questions/admin-questions.service';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'gmat-portal-platform-reports-explanation',
    templateUrl: './portal-platform-reports-explanation.component.html',
    styleUrls: ['./portal-platform-reports-explanation.component.less']
})
export class PortalPlatformReportsExplanationComponent implements OnInit {

    title: string;
    message: string;
    // question info
    question: any;
    // type question format default
    questionFormatType = QuestionFormatType;
    // alternatives list
    alternatives = [];
    // check of question exam
    clientAnswer: any = [];
    // answer question exam
    answer: any = [];
    // exam type format
    typeFormatQuestion: QuestionFormatType;
    form: FormGroup;
    constructor(
        public bsModalRef: BsModalRef,
        private fb: FormBuilder,
        private adminQuestionsService: AdminQuestionsService
    ) {
        this.createform();
    }

    ngOnInit() {
        console.log('question-modal-clientAnswer', this.clientAnswer)
        console.log('question-modal', this.question)
        this.showAlternativeAnswer().subscribe(() => {
            this.createFormAlternatives(this.question.alternatives, this.question.type);
        })
    }

    isCorrectAlternative(item: any) {
        if (this.clientAnswer.length > 0) {
            if (item['isTheAnswer'] != undefined) {
                if (item['isTheAnswer']) {
                    return 'alternative-success';
                } else if (!item['isTheAnswer'] && this.clientAnswer.filter(x => x == item.id).length > 0) {
                    return 'alternative-error';
                }
            }
        }
        return '';
    }

    showAlternativeAnswer() {
        return this.adminQuestionsService.postQuestionsAnswers({
            questionId: this.question.id
        }).pipe(
            tap((res: any) => {
                this.answer = res;
            })
        );
    }

    private createFormAlternatives(alternativeList: any, type: QuestionFormatType) {
        this.alternatives = alternativeList;
        this.typeFormatQuestion = type;
        // async alternatives
        switch (type) {
            case QuestionFormatType.selection:
                alternativeList.map((o, i) => {
                    const control = new FormControl({
                        value: this.clientAnswer.filter(x => x == o.id).length > 0 ? true : false,
                        disabled: true
                    }); // if first item set to true, else false
                    (this.form.controls.alternatives as FormArray).push(control);
                });
                break;

            case QuestionFormatType.boolean:
                const answerId: number = this.clientAnswer.map(x => x).find(x => x);
                (this.form.controls.alternatives as FormArray).push(new FormControl({ value: answerId, disabled: true }));
                break;

            default:
                break;
        }
    }

    private createform() {
        this.form = this.fb.group({
            alternatives: new FormArray([], minSelectedCheckboxes())
        });
    }

}
