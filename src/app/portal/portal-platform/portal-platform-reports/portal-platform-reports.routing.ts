import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortalPlatformReportsComponent } from './portal-platform-reports.component';
import { PortalPlatformReportsListComponent } from './portal-platform-reports-list/portal-platform-reports-list.component';
const routes: Routes = [
    {
        path: '',
        component: PortalPlatformReportsComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PortalPlatformReportsRouting { }
