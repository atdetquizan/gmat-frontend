import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AdminQuestionsService } from 'src/app/admin/admin-questions/admin-questions.service';
import { TypeCorrect } from '../../portal-shared/TypeCorrect.enum';
import { formatMMSS } from 'src/app/tools/common/gmat-functions';
import { BsModalService } from 'ngx-bootstrap';
import { PortalPlatformReportsChartComponent } from '../portal-platform-reports-chart/portal-platform-reports-chart.component';
import { PortalPlatformReportsExplanationComponent } from '../portal-platform-reports-explanation/portal-platform-reports-explanation.component';
import { Router } from '@angular/router';
import { TypeReports } from '../portal-platform-reports-shared/TypeReports.enum';
import { ExamResolveType } from 'src/app/tools/types/exam-resolve.type';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { AppliesToType } from '../portal-platform-reports-shared/question-applies-to.type';

@Component({
    selector: 'gmat-portal-platform-reports-questions',
    templateUrl: './portal-platform-reports-questions.component.html',
    styleUrls: ['./portal-platform-reports-questions.component.less']
})
export class PortalPlatformReportsQuestionsComponent implements OnInit {
    questionsIds: any[];
    parameters: any;
    exam: any;
    questionList: any;
    typeCorrect = TypeCorrect;
    appliesTo: AppliesToType;
    @Output() eventStart: EventEmitter<any> = new EventEmitter();
    constructor(
        private adminQuestionsService: AdminQuestionsService,
        private bsModalService: BsModalService,
        private loadingBarService: LoadingBarService,
        private router: Router
    ) {}

    ngOnInit() {
        this.showQuestionsList();
        // console.log(this.parameters);
    }

    onClickBack() {
        this.eventStart.emit({
            component: TypeReports.List,
            data: null,
            navigation: []
        });
    }
    showQuestionsList() {
        this.loadingBarService.start();
        this.adminQuestionsService
            .postClientReportsDetail({
                clientTestId: this.parameters.clientTestId.id
            })
            .subscribe((res: any) => {
                this.loadingBarService.stop();
                this.exam = res;
                this.questionList = res.questions;
                // console.log('this.questionList', this.questionList);
            });
    }

    onChangeCheckAll(target: any) {
        let questionsIds = [];
        const questionChecks: any = document.querySelectorAll(
            "input[name='question[]']"
        );
        questionChecks.forEach((x: any) => {
            x.checked = target.checked ? true : false;
        });
        questionChecks.forEach((x: any) => questionsIds.push(x.value));
        this.questionsIds = questionsIds;
    }

    onClickResolveMark() {
        let questionsIds = [];
        const questionChecks: any = document.querySelectorAll(
            "input[name='question[]']:checked"
        );
        questionChecks.forEach((x: any) => questionsIds.push(x.value));
        this.router.navigate([
            this.routeAppliesTo(),
            btoa(
                JSON.stringify({
                    type: ExamResolveType.RESOLVE_MARK,
                    clientTestId: this.parameters.clientTestId.id,
                    testDetailIds: questionsIds
                })
            )
        ]);
    }

    onClickTryAgain() {
        let questionsIds = [];
        const questionChecks = document.querySelectorAll(
            "input[name='question[]']"
        );
        questionChecks.forEach((x: any) => questionsIds.push(x.value));
        this.router.navigate([
            this.routeAppliesTo(),
            btoa(
                JSON.stringify({
                    type: ExamResolveType.TRY_AGAIN,
                    testId: this.parameters.clientTestId.id,
                })
            )
        ]);
    }

    onClickOpenChart() {
        const initialState = {
            title: 'Report of Graph',
            exam: this.exam
        };
        this.bsModalService.show(PortalPlatformReportsChartComponent, {
            class: 'modal-create-section modal-dialog-centered',
            initialState
        });
    }

    onClickOpenExplanation(item: any) {
        const initialState = {
            title: 'Question Explanation',
            question: item.question,
            clientAnswer: item.clientAnswer
        };
        this.bsModalService.show(PortalPlatformReportsExplanationComponent, {
            class: 'modal-create-section modal-dialog-centered modal-lg',
            initialState
        });
    }

    onChangeQuestion() {
        let questionsIds = [];
        const questionChecks: any = document.querySelectorAll(
            "input[name='question[]']:checked"
        );
        questionChecks.forEach((x: any) => questionsIds.push(x.value));
        this.questionsIds = questionsIds;
    }

    hasResolveMark() {
        return this.questionsIds && this.questionsIds.length ? true : false;
    }

    renderTime(secons: any) {
        return formatMMSS(secons);
    }

    private routeAppliesTo() {
        return this.parameters.appliesTo === AppliesToType.exam ? './exam/repeat-resolve' : 'portal/platform/practice';
    }
}
