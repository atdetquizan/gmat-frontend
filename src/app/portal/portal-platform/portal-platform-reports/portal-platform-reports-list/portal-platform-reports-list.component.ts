import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AdminQuestionsService } from 'src/app/admin/admin-questions/admin-questions.service';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TypeReports } from '../portal-platform-reports-shared/TypeReports.enum';
import { AppliesToType } from '../portal-platform-reports-shared/question-applies-to.type';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
    selector: 'gmat-portal-platform-reports-list',
    templateUrl: './portal-platform-reports-list.component.html',
    styleUrls: ['./portal-platform-reports-list.component.less']
})
export class PortalPlatformReportsListComponent implements OnInit {
    questionsReportsExam = [];
    questionsReportsPractice = [];
    form: FormGroup;
    appliesTo: AppliesToType;
    appliesToType = AppliesToType;
    @Output() eventStart: EventEmitter<any> = new EventEmitter();
    constructor(
        private adminQuestionsService: AdminQuestionsService,
        private auth: AuthenticationService,
        private fb: FormBuilder,
        private ladingBarService: LoadingBarService
    ) {
        this.createform();
    }

    ngOnInit() {
        this.showQuestionReports();
    }

    onClickQuestion() {
        this.eventStart.emit({
            component: TypeReports.Questions,
            data: {
                clientTestId: this.form.value.clientTestId,
                appliesTo: this.appliesTo,
            },
            navigation: []
        })
    }

    onChangeExamOrPractice(type: AppliesToType) {
        this.appliesTo = type;
        // console.log(this.appliesTo);
    }

    showQuestionReports() {
        this.ladingBarService.start();
        const user = this.auth.getUserClient().user;
        this.adminQuestionsService.postClientQuestionsReports({
            clientId: user.id
        }).subscribe((res: any) => {
            this.ladingBarService.complete();
            this.questionsReportsExam = res.exam;
            this.questionsReportsPractice = res.practice;
        });
    }

    private createform() {
        this.form = this.fb.group({
            clientTestId: [null, [Validators.required]]
        })
    }

}
