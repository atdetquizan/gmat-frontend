import { Component, OnInit, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { TypeReports } from './portal-platform-reports-shared/TypeReports.enum';
import { PortalPlatformReportsListComponent } from './portal-platform-reports-list/portal-platform-reports-list.component';
import { PortalPlatformReportsQuestionsComponent } from './portal-platform-reports-questions/portal-platform-reports-questions.component';
import { AdReportsDirective } from './portal-platform-reports-shared/ad-reports.directive';

@Component({
    selector: 'gmat-portal-platform-reports',
    templateUrl: './portal-platform-reports.component.html',
    styleUrls: ['./portal-platform-reports.component.less']
})
export class PortalPlatformReportsComponent implements OnInit {

    @ViewChild(AdReportsDirective) adHost: AdReportsDirective;
    constructor(
        private componentFactoryResolver: ComponentFactoryResolver
    ) { }

    ngOnInit() {
        this.loadComponent(TypeReports.List);
    }

    loadComponent(typeReports: TypeReports, parameters?: any, navigation?: any) {
        const component: any = this.searchCompoentn(typeReports);
        let componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);

        let viewContainerRef = this.adHost.viewContainerRef;
        viewContainerRef.clear();

        let componentRef = viewContainerRef.createComponent(componentFactory);
        let componentInstance = (<any>componentRef.instance);

        if (parameters) {
            componentInstance.parameters = parameters;
            parameters.typeLessons = typeReports;
        }
        if (navigation)
            componentInstance.navigation = navigation;

        componentInstance.eventStart.subscribe((res) => {
            this.loadComponent(res.component, res.data, res.navigation);
        });
    }

    searchCompoentn(typeReports: TypeReports) {
        switch (typeReports) {
            case TypeReports.List:
                return PortalPlatformReportsListComponent;
            case TypeReports.Questions:
                return PortalPlatformReportsQuestionsComponent;
        }
    }

}
