import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalPlatformReportsComponent } from './portal-platform-reports.component';
import { PortalPlatformReportsRouting } from './portal-platform-reports.routing';
import { PortalPlatformReportsListComponent } from './portal-platform-reports-list/portal-platform-reports-list.component';
import { PortalPlatformReportsQuestionsComponent } from './portal-platform-reports-questions/portal-platform-reports-questions.component';
import { PortalPlatformReportsChartComponent } from './portal-platform-reports-chart/portal-platform-reports-chart.component';
import { PortalPlatformReportsExplanationComponent } from './portal-platform-reports-explanation/portal-platform-reports-explanation.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AccordionModule, ModalModule } from 'ngx-bootstrap';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { HttpClientModule } from '@angular/common/http';
import { AdminQuestionsService } from 'src/app/admin/admin-questions/admin-questions.service';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { AdminUsersService } from 'src/app/admin/admin-users/admin-users.service';
import { AdReportsDirective } from './portal-platform-reports-shared/ad-reports.directive';
import { INTERCEPTOR_PORTAL_CLIENT } from '../../../tools/common/gmat-functions';
import { ToolsModule } from 'src/app/tools/tools.module';

const PROVIDERS_REPORTS = [
    AdminQuestionsService,
    AuthenticationService,
    AdminUsersService
];

@NgModule({
    declarations: [
        PortalPlatformReportsComponent,
        PortalPlatformReportsListComponent,
        PortalPlatformReportsQuestionsComponent,
        PortalPlatformReportsChartComponent,
        PortalPlatformReportsExplanationComponent,
        AdReportsDirective
    ],
    entryComponents: [
        PortalPlatformReportsListComponent,
        PortalPlatformReportsQuestionsComponent,
        PortalPlatformReportsChartComponent,
        PortalPlatformReportsExplanationComponent
    ],
    imports: [
        CommonModule,
        PortalPlatformReportsRouting,
        ReactiveFormsModule,
        AccordionModule.forRoot(),
        ModalModule.forRoot(),
        PerfectScrollbarModule,
        HttpClientModule,
        ToolsModule
    ],
    providers: [
        PROVIDERS_REPORTS,
        INTERCEPTOR_PORTAL_CLIENT
    ]
})
export class PortalPlatformReportsModule { }
