import { Component, OnInit } from '@angular/core';
import Highcharts from 'highcharts';
import { BsModalRef } from 'ngx-bootstrap';
import { AdminUsersService } from 'src/app/admin/admin-users/admin-users.service';
declare var require: any;
require('highcharts/highcharts-more')(Highcharts);

@Component({
    selector: 'gmat-portal-platform-reports-chart',
    templateUrl: './portal-platform-reports-chart.component.html',
    styleUrls: ['./portal-platform-reports-chart.component.less']
})
export class PortalPlatformReportsChartComponent implements OnInit {
    exam: any;
    returnGraph = false;
    title: string;
    message: string;
    constructor(
        public bsModalRef: BsModalRef,
        private adminUsersService: AdminUsersService
    ) { }

    ngOnInit() {
        if (this.exam) {
            const graphic = this.exam.graphic.topicPercents
            this.loadGraphics({
                title: 'Topics',
                categories: graphic.map((x) => x.sectionName || x.topicName || x.subjectName || x.subSubjectName),
                data: graphic.map((x) => x.knowledgePercent * 100),
                isFather: true
            });
        }
    }

    onClickReturnGraph() {
        const graphic = this.exam.graphic.topicPercents
        this.loadGraphics({
            title: 'Topics',
            categories: graphic.map((x) => x.sectionName || x.topicName || x.subjectName || x.subSubjectName),
            data: graphic.map((x) => x.knowledgePercent * 100),
            isFather: true
        });
        this.returnGraph = false;
    }

    loadGraphics(setting: any) {
        Highcharts.chart('chart-demo', {
            chart: {
                polar: true,
            },

            credits: {
                enabled: false
            },

            title: {
                text: setting.title
            },

            // pane: {
            //     size: '80%'
            // },

            xAxis: {
                categories: setting.categories,
                tickmarkPlacement: 'on',
                lineWidth: 0
            },

            yAxis: {
                gridLineInterpolation: 'polygon',
                lineWidth: 0,
                min: 0
            },

            tooltip: {
                shared: true,
                pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}%</b><br/>'
            },

            legend: {
                verticalAlign: 'bottom',
            },

            series: [{
                name: 'See target score',
                data: setting.data,
                pointPlacement: 'on',
                color: '#ffb536',
                events: {
                    click: (e) => {
                        if (setting.isFather) {
                            const topics = this.exam.graphic.topicPercents;
                            this.showDetailData(topics[e.point.index].id);
                        }
                    }
                }
            }]
        });
    }

    private showDetailData(topicId: any) {
        this.adminUsersService.getTestTopicReportSubjects(topicId)
            .subscribe((res: any) => {
                this.loadGraphics({
                    title: 'Subjects',
                    categories: res.map((x) => x.subjectName),
                    data: res.map((x) => x.knowledgePercent * 100),
                    isFather: false
                });
                this.returnGraph = true;
            });
    }
}
