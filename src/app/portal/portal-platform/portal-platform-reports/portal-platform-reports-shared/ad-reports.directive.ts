import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[ad-reports]'
})
export class AdReportsDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }
}
