import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PortalPlatformPracticeService {
    constructor(private http: HttpClient) {}

    getNodos() {
        return this.http.get(
            `${environment.apiurl}/questions/practice/by-node`
        );
    }

    getQuestion(params: any) {
        return this.http.post(
            `${environment.apiurl}/questions/practice`,
            params
        );        
        // return this.http.post('http://demo5538380.mockable.io/questions', params);
    }

    postFinish(params: any) {
        return this.http.post(
            `${environment.apiurl}/questions/practice/qualify`,
            params
        );
    }

    postResetQuestionsByNodoDifficult(parameters: any) {
        return this.http.post(`${environment.apiurl}/client-questions/reset`, parameters);
    }
}
