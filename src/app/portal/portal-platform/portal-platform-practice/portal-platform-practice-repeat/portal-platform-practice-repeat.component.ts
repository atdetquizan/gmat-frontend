import {
    Component,
    OnInit,
    ViewChild,
    Output,
    EventEmitter
} from '@angular/core';
import { QuestionFormatType } from '../../../../admin/admin-questions/admin-questions-shared/question-format-type.enum';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { PortalPlatformPracticeService } from '../portal-platform-practice.service';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { ChronometerService } from 'src/app/tools/chronometer/chronometer.service';
import { InformationShowService } from 'src/app/tools/information-show/information-show.service';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { BsModalService } from 'ngx-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { ExamResolveType } from 'src/app/tools/types/exam-resolve.type';
import { ExamService } from 'src/app/exam/exam.service';
import { ExamIndexModalComponent } from 'src/app/exam/exam-tools/exam-index-modal/exam-index-modal.component';
import { ModalMessagesComponent } from 'src/app/tools/modal-messages/modal-messages.component';
import { TypePractice } from '../portal-platform-practice-shared/TypePractice.enum';

@Component({
    selector: 'gmat-portal-platform-practice-repeat',
    templateUrl: './portal-platform-practice-repeat.component.html'
})
export class PortalPlatformPracticeRepeatComponent implements OnInit {
    form: FormGroup;
    parameters: any;
    investedTime = 0;
    questionList: any;
    indexCurrent = 0;
    questionActive: any;
    questionType = QuestionFormatType;
    user: any;
    answers: any;
    queryParams: any;
    @ViewChild('timer') timer: any;
    @Output() eventStart: EventEmitter<any> = new EventEmitter();
    constructor(
        private portalPlatformPracticeService: PortalPlatformPracticeService,
        private fb: FormBuilder,
        private reactiveFormsService: ReactiveFormsService,
        private authenticationService: AuthenticationService,
        private chronometerService: ChronometerService,
        private informationShowService: InformationShowService,
        private loadingBarService: LoadingBarService,
        private bsModalService: BsModalService,
        private activatedRoute: ActivatedRoute,
        private examService: ExamService
    ) {
        this.user = this.authenticationService.getUserClient().user;
        this.createform();
    }

    ngOnInit() {
        this.loadingBarService.start();
        this.activatedRoute.params.subscribe((params: any) => {
            this.queryParams = JSON.parse(atob(params.question));
            this.loadQuestionList().subscribe((res: any) => {
                this.chronometerService.start();
                console.log(res);
                this.questionList = res;
                this.form.controls.clientId.setValue(this.user.id);
                this.form.controls.clientTestId.setValue(
                    this.queryParams.type === ExamResolveType.RESOLVE_MARK
                        ? this.queryParams.clientTestId
                        : this.queryParams.testId
                );
                this.createQuestion();
                this.loadingBarService.stop();
            });
        });
    }

    onClickConfirm() {
        const question = this.form.controls.questions as FormArray;
        const answersId = (question.controls[this.indexCurrent] as FormGroup)
            .controls.answersId;
        answersId.markAsTouched();
        if (this.form.valid) {
            this.setInvestedTime();
            this.indexCurrent++;
            this.questionActive = this.questionList[this.indexCurrent];
            this.questionList[this.indexCurrent - 1].question.clientAnswer =
                answersId.value;
        }

        if (this.hasClientAnswer()) {
            this.indexCurrent++;
            this.setChronometerTime();
            this.questionActive = this.questionList[this.indexCurrent];
        }
    }

    onClickBack() {
        this.indexCurrent--;
        this.setChronometerTime();
    }

    onClickClose() {
        this.eventStart.emit({
            component: TypePractice.setting,
            data: []
        });
    }

    onClickFinish() {
        if (this.form.valid) {
            this.setInvestedTime();
            const controls: any = this.form.value;
            const params = {
                clientId: controls.clientId,
                clientTestId: controls.clientTestId,
                questions: controls.questions.map((x: any) => {
                    x.clientAnswer = x.answersId;
                    delete x.answersId;
                    return x;
                }),
                totalInvestedTime: controls.totalInvestedTime
            };
            this.loadingBarService.start();
            this.examService.postValidateTest(params).subscribe(
                (res: any) => {
                    this.loadingBarService.stop();
                    this.onClickClose();
                    const initialState = {
                        title: 'Success',
                        message: res.message
                    };
                    this.bsModalService.show(ModalMessagesComponent, {
                        class: 'modal-dialog-centered',
                        initialState
                    });;
                },
                error => {
                    this.loadingBarService.complete();
                    const response = error.error.error;
                    if (error.status === 400) {
                        const initialState = {
                            title: response.name,
                            message: response.message
                        };
                        this.bsModalService.show(ModalMessagesComponent, {
                            class: 'modal-dialog-centered',
                            backdrop: 'static',
                            keyboard: false,
                            initialState
                        });
                    }
                }
            );
        }
    }

    formArrayQuestionByIndex(index: number) {
        const formArray = this.form.controls.questions as FormArray;
        return formArray.controls[index] as FormGroup;
    }

    formArrayQuestion() {
        return (this.form.controls.questions as FormArray).controls;
    }

    private hasClientAnswer() {
        return this.questionList[this.indexCurrent] &&
            this.questionList[this.indexCurrent].question &&
            this.questionList[this.indexCurrent].question.clientAnswer
    }

    private setChronometerTime() {
        const question = this.form.controls.questions as FormArray;
        const investedTime = (question.controls[this.indexCurrent] as FormGroup).controls.investedTime.value;
        this.chronometerService.stop();
        this.chronometerService.changeSeconds(investedTime);
    }

    private loadQuestionList() {
        const params: any = {
            clientId: this.user.id
        };
        switch (this.queryParams.type) {
            case ExamResolveType.RESOLVE_MARK:
                params.testDetailIds = this.queryParams.testDetailIds;
                params.clientTestId = this.queryParams.clientTestId;
                return this.examService.postResolveMark(params);

            default:
                params.testId = this.queryParams.testId;
                return this.examService.postTryAgain(params);
        }
    }

    private setInvestedTime() {
        this.chronometerService.stop();
        const question = this.form.controls.questions as FormArray;
        (question.controls[
            this.indexCurrent
        ] as FormGroup).controls.investedTime.setValue(this.investedTime);
        const totalInvestedTime = this.form.controls.totalInvestedTime.value;
        this.form.controls.totalInvestedTime.setValue(
            totalInvestedTime + this.investedTime
        );
        this.chronometerService.restart();
    }

    private createQuestion() {
        const questions = this.form.controls.questions as FormArray;
        for (const item of this.questionList) {
            questions.push(this.addQuestion(item));
        }
    }

    private addQuestion(question: any) {
        return this.fb.group({
            clientTestDetailId: [question.id, null],
            answersId: [null, null],
            investedTime: [null, null],
            questionId: [question.question.id, null]
        });
    }

    private createform() {
        this.form = this.fb.group({
            clientId: [null, null],
            clientTestId: [null, null],
            questions: this.fb.array([]),
            totalInvestedTime: [null, null]
        });
    }
}
