import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortalPlatformPracticeComponent } from './portal-platform-practice.component';
const routes: Routes = [
    {
        path: '',
        component: PortalPlatformPracticeComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PortalPlatformPracticeRouting { }
