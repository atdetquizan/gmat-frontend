import {
    Component,
    OnInit,
    EventEmitter,
    Output,
    ViewChild
} from '@angular/core';
import { PortalPlatformPracticeService } from '../portal-platform-practice.service';
import { format } from '../../../../tools/common/gmat-functions';
import { QuestionFormatType } from '../../../../admin/admin-questions/admin-questions-shared/question-format-type.enum';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
import { ReactiveFormsService } from '../../../../tools/services/reactive-forms.service';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { timer } from 'rxjs'
import { ChronometerService } from 'src/app/tools/chronometer/chronometer.service';
import { InformationShowService } from 'src/app/tools/information-show/information-show.service';
import { InformationShowType } from 'src/app/tools/information-show/information-show.type';
import { TypePractice } from '../portal-platform-practice-shared/TypePractice.enum';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { BsModalService } from 'ngx-bootstrap';
import { ModalMessagesComponent } from 'src/app/tools/modal-messages/modal-messages.component';

@Component({
    selector: 'gmat-portal-platform-practice-exam',
    templateUrl: './portal-platform-practice-exam.component.html'
})
export class PortalPlatformPracticeExamComponent implements OnInit {
    form: FormGroup;
    parameters: any;
    // chronometer: any;
    investedTime = 0;
    // practice: any;
    questionList: any;
    indexCurrent = 0;
    questionActive: any;
    questionType = QuestionFormatType;
    // questionAnswers: any = [];
    user: any;
    answers: any;
    @ViewChild('timer') timer: any;
    @Output() eventStart: EventEmitter<any> = new EventEmitter();
    constructor(
        private portalPlatformPracticeService: PortalPlatformPracticeService,
        private fb: FormBuilder,
        private reactiveFormsService: ReactiveFormsService,
        private authenticationService: AuthenticationService,
        private chronometerService: ChronometerService,
        private informationShowService: InformationShowService,
        private loadingBarService: LoadingBarService,
        private bsModalService: BsModalService
    ) {
        this.user = this.authenticationService.getUserClient().user;
        this.createform();
    }

    ngOnInit() {
        this.informationShowService.setup(InformationShowType.NO_SHOW_PRACTICE);
        this.showQuestion();
    }

    onClickClose() {
        this.eventStart.emit({
            component: TypePractice.setting,
            data: []
        });
    }

    showQuestion() {
        this.loadingBarService.start();
        this.portalPlatformPracticeService
            .getQuestion(this.parameters)
            .subscribe((res: any) => {
                this.loadingBarService.complete();
                this.questionList = res;
                this.createQuestion();
                this.questionActive = this.questionList[this.indexCurrent];
                this.form.controls.clientTestId.setValue(this.questionList.clientTestId);
                this.form.controls.uniqueTestId.setValue(this.questionList.uniqueTestId);
                if (this.questionActive) {
                    this.chronometerService.start();
                }
            });
    }

    formArrayQuestionByIndex(index: number) {
        const formArray = this.form.controls.questions as FormArray;
        return formArray.controls[index] as FormGroup;
    }

    formArrayQuestion() {
        return (this.form.controls.questions as FormArray).controls;
    }

    onClickConfirm() {
        const question = this.form.controls.questions as FormArray
        const answersId = (question.controls[this.indexCurrent] as FormGroup).controls.answersId
        answersId.markAsTouched();
        if (this.form.valid) {
            this.setInvestedTime();
            this.indexCurrent++;
            this.questionActive = this.questionList.questions[this.indexCurrent];
            this.questionList.questions[this.indexCurrent - 1].clientAnswer = answersId.value;
        }

        if (this.questionList.questions[this.indexCurrent].clientAnswer) {
            this.setInvestedTime();
            this.indexCurrent++;
            this.questionActive = this.questionList.questions[this.indexCurrent];
        }
    }

    onClickBack() {
        this.chronometerService.stop();
        // const question = this.form.controls.questions as FormArray
        // const answersId = (question.controls[this.indexCurrent] as FormGroup).controls.answersId.value
        // console.log(this.questionActive);
        this.indexCurrent--;
    }

    onClickFinish() {
        // this.loadingBarService.start();
        const question = this.form.controls.questions as FormArray
        (question.controls[this.indexCurrent] as FormGroup).controls.answersId.markAsTouched();
        if (this.form.valid) {
            this.loadingBarService.start();
            this.setInvestedTime();
            this.portalPlatformPracticeService
                .postFinish(this.form.value)
                .subscribe((res: any) => {
                    this.loadingBarService.complete();
                    this.onClickClose();
                    const initialState = {
                        title: 'Success',
                        message: res.message
                    };
                    this.bsModalService.show(ModalMessagesComponent, {
                        class: 'modal-dialog-centered',
                        initialState
                    });;
                });
        }
    }

    private setInvestedTime() {
        this.chronometerService.stop();
        const question = this.form.controls.questions as FormArray
        (question.controls[this.indexCurrent] as FormGroup).controls.investedTime.setValue(this.investedTime);
        const totalInvestedTime = this.form.controls.totalInvestedTime.value;
        this.form.controls.totalInvestedTime.setValue(totalInvestedTime + this.investedTime);
        this.chronometerService.restart();
    }


    private createQuestion() {
        const questions = (this.form.controls.questions as FormArray);
        for (const question of this.questionList.questions) {
            questions.push(this.addQuestion(question));
        }
    }

    private addQuestion(question: any) {
        const itemGroup = this.fb.group({
            id: [question.id, null],
            investedTime: [null, null],
            questionOrder: [question.questionOrder, null],
            answersId: [null, null]
        });
        return itemGroup;
    }

    private createform() {
        this.form = this.fb.group({
            uniqueTestId: [null, null],
            clientId: [this.user.id, null],
            clientTestId: [null, null],
            totalInvestedTime: [null, null],
            questions: this.fb.array([])
        });
    }
}
