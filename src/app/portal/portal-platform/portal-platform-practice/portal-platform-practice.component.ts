import {
    Component,
    OnInit,
    ViewChild,
    ComponentFactoryResolver
} from '@angular/core';
import { TypePractice } from './portal-platform-practice-shared/TypePractice.enum';
import { PortalPlatformPracticeSettingComponent } from './portal-platform-practice-setting/portal-platform-practice-setting.component';
import { PortalPlatformPracticeExamComponent } from './portal-platform-practice-exam/portal-platform-practice-exam.component';
import { AdPracticeDirective } from './portal-platform-practice-shared/ad-practice.directive.directive';
import { ActivatedRoute } from '@angular/router';
import { PortalPlatformPracticeRepeatComponent } from './portal-platform-practice-repeat/portal-platform-practice-repeat.component';

@Component({
    selector: 'gmat-portal-platform-practice',
    templateUrl: './portal-platform-practice.component.html'
})
export class PortalPlatformPracticeComponent implements OnInit {
    defaultComponent = TypePractice.setting;
    @ViewChild(AdPracticeDirective) adHost: AdPracticeDirective;
    constructor(
        private componentFactoryResolver: ComponentFactoryResolver,
        private activatedRoute: ActivatedRoute
    ) {
        this.activatedRoute.params.subscribe(res => {
            if (res.question) {
                this.defaultComponent = TypePractice.repeat;
            }
        });
    }

    ngOnInit() {
        this.loadComponent(this.defaultComponent);
    }

    loadComponent(typePractice: TypePractice, parameters?: any) {
        const component: any = this.searchCompoentn(typePractice);
        let componentFactory = this.componentFactoryResolver.resolveComponentFactory(
            component
        );

        let viewContainerRef = this.adHost.viewContainerRef;
        viewContainerRef.clear();

        let componentRef = viewContainerRef.createComponent(componentFactory);
        let componentInstance = <any>componentRef.instance;

        if (parameters) {
            componentInstance.parameters = parameters;
            parameters.typePractice = typePractice;
        }

        componentInstance.eventStart.subscribe(res =>
            this.loadComponent(res.component, res.data)
        );
    }

    searchCompoentn(typePractice: TypePractice) {
        switch (typePractice) {
            case TypePractice.setting:
                return PortalPlatformPracticeSettingComponent;
            case TypePractice.exam:
                return PortalPlatformPracticeExamComponent;
            case TypePractice.repeat:
                return PortalPlatformPracticeRepeatComponent

        }
    }
}
