import {
    Component,
    OnInit,
    EventEmitter,
    Output,
    ViewChild
} from '@angular/core';
import { PortalPlatformPracticeService } from '../portal-platform-practice.service';
import { PracticeNodesContentComponent } from '../portal-platform-practice-shared/practice-nodes-content/practice-nodes-content.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../../authentication/authentication.service';
import { TypePractice } from '../portal-platform-practice-shared/TypePractice.enum';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
    selector: 'gmat-portal-platform-practice-setting',
    templateUrl: './portal-platform-practice-setting.component.html'
})
export class PortalPlatformPracticeSettingComponent implements OnInit {
    levels = [
        { id: 'very_easy', title: 'Very Easy' },
        { id: 'easy', title: 'Easy' },
        { id: 'medium', title: 'Medium' },
        { id: 'hard', title: 'Hard' },
        { id: 'very_hard', title: 'Very Hard' }
    ];
    nodesList: any;
    form: FormGroup;
    user: any;
    level: any;
    opacity = true;
    @Output() eventStart: EventEmitter<any> = new EventEmitter();
    @ViewChild(PracticeNodesContentComponent)
    nodescontent: PracticeNodesContentComponent;
    constructor(
        private fb: FormBuilder,
        private auth: AuthenticationService,
        private portalPlatformPracticeService: PortalPlatformPracticeService,
        private reactiveFormsService: ReactiveFormsService,
        private loadingBarService: LoadingBarService
    ) {
        this.user = this.auth.getUserClient();
        this.createform();
    }

    ngOnInit() {
        this.showSetting();
        
    }
    onClickLevel(item) {
        if (item) {
            this.opacity = false;
            this.level = item;
            this.form.controls.level.setValue(item);
        }
    }
    onClickConfirm() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            const detail = this.nodescontent.nodedetail;
            const params: any = Object.assign(detail.form.value, this.form.value);
            this.eventStart.emit({
                component: TypePractice.exam,
                data: params
            });
        }
    }

    private showSetting() {
        this.loadingBarService.start();
        this.portalPlatformPracticeService.getNodos().subscribe(res => {
            this.loadingBarService.complete();
            this.nodesList = res;
    
        });
    }

    private createform() {
        this.form = this.fb.group({
            level: [null, [Validators.required]],
            timed: [false, [Validators.required]],
            clientId: [this.user.userId, [Validators.required]]
        });
    }
}
