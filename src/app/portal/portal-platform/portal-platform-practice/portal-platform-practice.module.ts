import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalPlatformPracticeComponent } from './portal-platform-practice.component';
import { PortalPlatformPracticeExamComponent } from './portal-platform-practice-exam/portal-platform-practice-exam.component';
import { PortalPlatformPracticeSettingComponent } from './portal-platform-practice-setting/portal-platform-practice-setting.component';
import { ModalModule, AccordionModule, CollapseModule, TooltipModule } from 'ngx-bootstrap';
import { AdPracticeDirective } from './portal-platform-practice-shared/ad-practice.directive.directive';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PortalPlatformPracticeRouting } from './portal-platform-practice.routing';
import { PortalPlatformPracticeService } from './portal-platform-practice.service';
import { HttpClientModule } from '@angular/common/http';
import { INTERCEPTOR_PORTAL_CLIENT } from '../../../tools/common/gmat-functions';
import { PracticeNodesContentComponent } from './portal-platform-practice-shared/practice-nodes-content/practice-nodes-content.component';
import { PracticeInputIncrementComponent } from './portal-platform-practice-shared/practice-input-increment/practice-input-increment.component';
import { PracticeNodesDetailComponent } from './portal-platform-practice-shared/practice-nodes-detail/practice-nodes-detail.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ControlMessagesModule } from '../../../tools/control-messages/control-messages.module';
import { PracticeDescriptionQuestionComponent } from './portal-platform-practice-shared/practice-description-question/practice-description-question.component';
import { ToolsModule } from 'src/app/tools/tools.module';
import { ExamService } from 'src/app/exam/exam.service';
import { PortalPlatformPracticeRepeatComponent } from './portal-platform-practice-repeat/portal-platform-practice-repeat.component';

@NgModule({
    declarations: [
        PortalPlatformPracticeComponent,
        PortalPlatformPracticeExamComponent,
        PortalPlatformPracticeSettingComponent,
        AdPracticeDirective,
        PracticeNodesContentComponent,
        PracticeInputIncrementComponent,
        PracticeNodesDetailComponent,
        PracticeDescriptionQuestionComponent,
        PortalPlatformPracticeRepeatComponent
    ],
    entryComponents: [
        PortalPlatformPracticeComponent,
        PortalPlatformPracticeExamComponent,
        PortalPlatformPracticeSettingComponent,
        PortalPlatformPracticeRepeatComponent
    ],
    imports: [
        CommonModule,
        PerfectScrollbarModule,
        CollapseModule.forRoot(),
        AccordionModule.forRoot(),
        ModalModule.forRoot(),
        PortalPlatformPracticeRouting,
        ReactiveFormsModule,
        HttpClientModule,
        ControlMessagesModule,
        ToolsModule,
        TooltipModule.forRoot()
    ],
    providers: [PortalPlatformPracticeService, ExamService, INTERCEPTOR_PORTAL_CLIENT]
})
export class PortalPlatformPracticeModule {}
