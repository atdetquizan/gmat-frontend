import { Component, OnInit, ElementRef, ViewChild, Input } from '@angular/core';
import Quill from 'quill';

@Component({
    selector: 'gmat-practice-description-question',
    templateUrl: './practice-description-question.component.html',
    styleUrls: ['./practice-description-question.component.less']
})
export class PracticeDescriptionQuestionComponent implements OnInit {
    @ViewChild('question') questioncontent: ElementRef;
    @Input() description: any;
    constructor() { }

    ngOnInit() {
        const txtQuill = new Quill(this.questioncontent.nativeElement);
    }

}
