import {
    Component,
    OnInit,
    Input,
    EventEmitter,
    OnChanges
} from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { PortalPlatformPracticeService } from '../../portal-platform-practice.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'gmat-practice-nodes-detail',
    templateUrl: './practice-nodes-detail.component.html',
    styles: []
})
export class PracticeNodesDetailComponent implements OnInit, OnChanges {
    @Input() item: any;
    selectNode: any;
    form: FormGroup;
    resetNeeded: any;
    user: any;
    constructor(private fb: FormBuilder,
        private auth: AuthenticationService,
        private portalPlatformPracticeService: PortalPlatformPracticeService,
        private sweetalertService: SweetalertService) {
        this.createform();
    }

    ngOnInit() {
        this.user = this.auth.getUserClient();
        // console.log(this.user);
        if (this.item) {
            this.createSelections();
        }
    }

    ngOnChanges(data: any) {
        if (!data.item.firstChange) {
            this.createSelections();
        }
    }

    createform() {
        this.form = this.fb.group({
            selection: this.fb.array([])
        });
    }

    getFormArraySelection() {
        const formArray = this.form.controls.selection as FormArray;
        return formArray.controls;
    }

    // onClickRemove(index: number) {
    //     const formArray = this.form.controls.selection as FormArray;
    //     formArray.removeAt(index);
    // }

    onClickReset(value: any) {
        // console.log('value', value);
        if (value || value !== undefined) {
            this.sweetalertService.swal({
                title: 'Are you sure to restore your questions?',
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    const parameters: any = {};
                    parameters.node = this.item.node.node;
                    parameters.id = this.item.node.id;
                    parameters.clientId = this.user.userId;
                    // parameters.difficulty = this.item.level;
                    this.portalPlatformPracticeService.postResetQuestionsByNodoDifficult(parameters).subscribe((res: any) => {
                        this.sweetalertService.swal('Your questions have been restored correctly.', {
                            icon: "success",
                        }).then(() => {
                            window.location.reload();
                        });
                    });
                }
            });
        }

        // console.log('value', value, this.item);
    }

    private createSelections() {
        const formArray = this.form.controls.selection as FormArray;
        if (this.item.select) {
            // console.log('this.item', this.item);
            this.selectNode = this.item;
            formArray.push(this.addSelection());
        } else {
            for (let i = 0; i < formArray.controls.length; i++) {
                const formGroup = formArray.controls[i] as FormGroup;
                if (
                    formGroup.value.id === this.item.node.id &&
                    formGroup.value.type === this.item.node.node
                )
                    formArray.removeAt(i);
            }
        }
    }

    private addSelection(): FormGroup {
        return this.fb.group({
            id: [this.item.node.id, null],
            name: [this.item.node.name, null],
            limit: [0, null],
            reset_needed: [this.item.node.reset_needed, null],
            type: [this.item.node.node]
        });
    }
}
