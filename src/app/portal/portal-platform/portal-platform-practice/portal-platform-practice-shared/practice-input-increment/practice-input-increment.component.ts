import { Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'gmat-practice-input-increment',
    templateUrl: './practice-input-increment.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => PracticeInputIncrementComponent),
            multi: true
        }
    ]
})
export class PracticeInputIncrementComponent implements ControlValueAccessor {
    @Input() range: any;
    @Input('value') _value = 0;
    onChange: any = () => { };
    onTouched: any = () => { };
    showInputIncrement = true;

    get value(): number {
        return this._value;
    }

    set value(val) {
        this._value = val;
        this.onChange(val);
        this.onTouched();
    }

    constructor() {
    }

    ngOnInit(): void {
        if (this.range.node.practiceQuestions === undefined || this.range.node.practiceQuestions.total === 0) {
            this.showInputIncrement = false;
        } else {
            this.showInputIncrement = true;
        }
        // console.log('range', this.range);
        // console.log('range2', this.range.node.practiceQuestions);
    }

    increaseValue() {
        if (this.range.node.practiceQuestions !== undefined) {
            let value2 = this.range.node.practiceQuestions.total;
            if (this.value < value2) {
                this.value++
            }
        }

    }

    decreaseValue() {
        if (this.range.node.practiceQuestions !== undefined) {
            let value = this.value;
            value--;
            this.value = value < 0 ? 0 : value;
        }

    }

    registerOnChange(fn) {
        this.onChange = fn;
    }

    writeValue(value) {
        if (value) this.value = value;
    }

    registerOnTouched(fn) {
        this.onTouched = fn;
    }
}
