import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[ad-practice]'
})
export class AdPracticeDirective {

    constructor(public viewContainerRef: ViewContainerRef) { }

}
