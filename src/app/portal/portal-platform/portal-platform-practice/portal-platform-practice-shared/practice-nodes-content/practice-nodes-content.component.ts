import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter,
    ViewChild
} from '@angular/core';
import { PracticeNodesDetailComponent } from '../practice-nodes-detail/practice-nodes-detail.component';

@Component({
    selector: 'gmat-practice-nodes-content',
    templateUrl: './practice-nodes-content.component.html',
    styles: []
})
export class PracticeNodesContentComponent implements OnInit {
    selectNode: any;
    showLevel: any;
    @Input() nodes: any;
    @Input() level: any;
    @ViewChild(PracticeNodesDetailComponent) nodedetail: PracticeNodesDetailComponent;
    constructor() {}

    ngOnInit(): void {
        // this.showLevel = this.level;
        // console.log(this.showLevel);
    }

    onChangeNode(checked: boolean, node: any) {
        this.selectNode = {
            select: checked,
            node: node,
            level: this.level
        };
    }
}
