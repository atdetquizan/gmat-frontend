import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PortalPlatformLessonsClipnotesService {

    constructor(
        private http: HttpClient
    ) { }

    post(paramters: any) {
        return this.http.post(`${environment.apiurl}/clipnotes`, paramters)
    }

    get(videoId: number, clientId: number) {
        return this.http.get(`${environment.apiurl}/videos/clipnotes/${videoId}/client/${clientId}`);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiurl}/clipnotes/${id}`);
    }

}
