import { Component, OnInit } from '@angular/core';
import {
    FormGroup,
    FormBuilder,
    Validators,
    AbstractControl
} from '@angular/forms';
import { ExamService } from '../../../../exam/exam.service';
import { ActivatedRoute, Router } from '@angular/router';
import { encryptJS } from 'src/app/tools/common/gmat-functions';
import { ReactiveFormsService } from '../../../../tools/services/reactive-forms.service';

@Component({
    selector: 'gmat-portal-platform-exam-index',
    templateUrl: './portal-platform-exam-index.component.html',
    styleUrls: ['./portal-platform-exam-index.component.less']
})
export class PortalPlatformExamIndexComponent implements OnInit {
    form: FormGroup;
    listConfigs: any = [];
    constructor(
        private examService: ExamService,
        private fb: FormBuilder,
        private router: Router,
        private reactiveFormsService: ReactiveFormsService
    ) {
        this.createform();
    }

    ngOnInit() {
        this.showListConfigs();
    }

    onClickStart() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            this.router.navigate(['/exam'], {
                queryParams: { code: encryptJS(this.form.value) }
            });
        }
    }

    renderValue(value: any) {
        return value;
    }

    ngClassExam() {
        const code: AbstractControl = this.form.controls.code;
        return { 'error-exam-type': code.touched && !code.valid };
    }

    private createform() {
        this.form = this.fb.group({
            code: [null, [Validators.required]]
        });
    }

    private showListConfigs() {
        this.examService.getListConfigs().subscribe(res => {
            this.listConfigs = res;
        });
    }
}
