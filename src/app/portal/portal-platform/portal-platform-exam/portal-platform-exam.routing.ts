import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortalPlatformExamComponent } from './portal-platform-exam.component';
import { PortalPlatformExamIndexComponent } from './portal-platform-exam-index/portal-platform-exam-index.component';

const routes: Routes = [
    {
        path: '',
        component: PortalPlatformExamComponent,
        children: [
            {
                path: '',
                component: PortalPlatformExamIndexComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PortalPlatformExamRouting { }
