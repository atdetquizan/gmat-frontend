import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalPlatformExamComponent } from './portal-platform-exam.component';
import { PortalPlatformExamRouting } from './portal-platform-exam.routing';
import { PortalPlatformExamIndexComponent } from './portal-platform-exam-index/portal-platform-exam-index.component';
import { HttpClientModule } from '@angular/common/http';
import { INTERCEPTOR_PORTAL_CLIENT } from '../../../tools/common/gmat-functions';
import { ExamService } from '../../../exam/exam.service';
import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        PortalPlatformExamRouting,
        HttpClientModule,
        ControlMessagesModule,
        ReactiveFormsModule
    ],
    declarations: [
        PortalPlatformExamComponent,
        PortalPlatformExamIndexComponent
    ],
    providers: [
        ExamService,
        INTERCEPTOR_PORTAL_CLIENT
    ]
})
export class PortalPlatformExamModule { }
