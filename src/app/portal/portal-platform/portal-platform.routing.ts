import { PortalPlatformComponent } from './portal-platform.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PortalPlatformExamComponent } from './portal-platform-exam/portal-platform-exam.component';
const routes: Routes = [
    {
        path: '',
        component: PortalPlatformComponent,
        children: [
            {
                path: '',
                redirectTo: 'lesson'
            },
            {
                path: 'lesson',
                loadChildren: './portal-platform-lessons/portal-platform-lessons.module#PortalPlatformLessonsModule'
            },
            {
                path: 'practice',
                loadChildren: './portal-platform-practice/portal-platform-practice.module#PortalPlatformPracticeModule'
            },
            {
                path: 'practice/:question',
                loadChildren: './portal-platform-practice/portal-platform-practice.module#PortalPlatformPracticeModule'
            },
            {
                path: 'exam',
                loadChildren: './portal-platform-exam/portal-platform-exam.module#PortalPlatformExamModule'
            },
            {
                path: 'report',
                loadChildren: './portal-platform-reports/portal-platform-reports.module#PortalPlatformReportsModule'
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PortalPlatformRouting { }
