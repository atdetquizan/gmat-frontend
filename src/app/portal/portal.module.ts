import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortalComponent } from './portal.component';
import { PortalRouting } from './portal.routing';
import { HttpClientModule } from '@angular/common/http';
import { PortalToolsModule } from './portal-tools/portal-tools.module';

@NgModule({
    imports: [
        PortalRouting,
        CommonModule,
        HttpClientModule,
        PortalToolsModule,
    ],
    declarations: [
        PortalComponent
    ],
    providers: [
        
    ]
})
export class PortalModule { }
