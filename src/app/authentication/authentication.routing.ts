import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivateChild } from '@angular/router';
import { AuthenticationLoginComponent } from './authentication-login/authentication-login.component';
import { AuthenticationComponent } from './authentication.component';
import { AuthenticationSignupComponent } from './authentication-signup/authentication-signup.component';
import { AuthenticationConfirmComponent } from './authentication-confirm/authentication-confirm.component';
import { AuthenticationRecoverkeyComponent } from './authentication-recoverkey/authentication-recoverkey.component';
import { AuthenticationChangepasswordComponent } from './authentication-changepassword/authentication-changepassword.component';
import { AuthenticationSubscriptionComponent } from './authentication-subscription/authentication-subscription.component';
import { AuthInactivePortalGuard } from '../tools/guards/auth-inactive-portal.guard';

const routes: Routes = [
    {
        path: '',
        component: AuthenticationComponent,
        canActivateChild: [AuthInactivePortalGuard],
        children: [
            {
                path: 'signup/:id',
                component: AuthenticationSignupComponent
            },
            {
                path: 'subscription',
                component: AuthenticationSubscriptionComponent
            },
            {
                path: 'confirm/:user',
                component: AuthenticationConfirmComponent
            },
            {
                path: 'recoverkey',
                component: AuthenticationRecoverkeyComponent
            },
            {
                path: 'changepassword/:key',
                component: AuthenticationChangepasswordComponent
            },
            {
                path: '',
                component: AuthenticationLoginComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthenticationRoutes {}
