import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { CustomValidators } from 'ngx-custom-validators';

@Component({
    selector: 'gmat-authentication-recoverkey',
    templateUrl: './authentication-recoverkey.component.html',
    styleUrls: ['./authentication-recoverkey.component.less']
})
export class AuthenticationRecoverkeyComponent implements OnInit {
    alert: any;
    form: FormGroup;
    constructor(
        private fb: FormBuilder,
        private authenticationService: AuthenticationService,
        private route: Router,
        private sweetalertService: SweetalertService,
        private reactiveFormsService: ReactiveFormsService
    ) {
        this.createfrm();
    }

    ngOnInit() {
    }

    recoverkey() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            this.authenticationService.reset(this.form.value)
                .subscribe(res => {
                    this.sweetalertService.swal({
                        title: environment.messages.recoverkey.title,
                        text: environment.messages.recoverkey.text,
                        icon: "success"
                    }).then((willDelete) => {
                        if (willDelete) {
                            this.form.reset();
                            //console.log(res);
                            this.route.navigate(['/']);
                        }
                    });
                }, error => {
                    this.alert = {
                        type: 'danger',
                        title: environment.messages.error.title,
                        msg: error.message
                    };
                });
        }
    }

    private createfrm() {
        this.form = this.fb.group({
            email: [null, [
                Validators.required,
                CustomValidators.email
            ]]
        });
    }

}
