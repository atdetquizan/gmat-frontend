import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './authentication.component';
import { AuthenticationRoutes } from './authentication.routing';
import { AuthenticationLoginComponent } from './authentication-login/authentication-login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthenticationSignupComponent } from './authentication-signup/authentication-signup.component';
import { AuthenticationConfirmComponent } from './authentication-confirm/authentication-confirm.component';
import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider, LinkedInLoginProvider } from 'angularx-social-login';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { AuthenticationRecoverkeyComponent } from './authentication-recoverkey/authentication-recoverkey.component';
import { AuthenticationChangepasswordComponent } from './authentication-changepassword/authentication-changepassword.component';
import { ControlMessagesModule } from '../tools/control-messages/control-messages.module';
// import { AppHttpInterceptorClient } from '../interceptors/AppHttpInterceptorClient';
import { ReactiveFormsService } from '../tools/services/reactive-forms.service';
import { AuthenticationSubscriptionComponent } from './authentication-subscription/authentication-subscription.component';
import { AuthenticationToolsModule } from './authentication-tools/authentication-tools.module';
import { AdminPricingService } from '../admin/admin-pricing/admin-pricing.service';
// import { AuthGuardAuth } from '../tools/auth-guard-auth';
import { AlertModule, ModalModule } from 'ngx-bootstrap';
import { INTERCEPTOR_PORTAL_CLIENT, INTERCEPTOR_ERRORS } from '../tools/common/gmat-functions';
import { AuthenticationResendComponent } from './authentication-resend/authentication-resend.component';
import { AppHttpErrorAuth } from './authentication-tools/interceptors/app-http-error-auth';
import { ToolsModule } from '../tools/tools.module';
import { AppHttpInterceptorClient } from '../interceptors';

const config = new AuthServiceConfig([
    // https://developers.google.com/identity/sign-in/web/sign-in
    {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('503770431225-ercq5bf0981istibak36tph047amn8q3.apps.googleusercontent.com')
    },
    {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider('476791632422910')
    },
    {
        id: LinkedInLoginProvider.PROVIDER_ID,
        provider: new LinkedInLoginProvider('78k9qfzytmqjwm', false, 'en_US')
    }
]);

export function provideConfig() {
    return config;
}

const PROVIDERS_AUTH = [AuthenticationService, ReactiveFormsService, AdminPricingService, AdminPricingService];

@NgModule({
    imports: [
        CommonModule,
        AuthenticationRoutes,
        ReactiveFormsModule,
        HttpClientModule,
        SocialLoginModule,
        ControlMessagesModule,
        AuthenticationToolsModule,
        AlertModule.forRoot(),
        ModalModule.forRoot(),
        ToolsModule
    ],
    declarations: [
        AuthenticationComponent,
        AuthenticationLoginComponent,
        AuthenticationSignupComponent,
        AuthenticationConfirmComponent,
        AuthenticationRecoverkeyComponent,
        AuthenticationRecoverkeyComponent,
        AuthenticationChangepasswordComponent,
        AuthenticationSubscriptionComponent,
        AuthenticationResendComponent
    ],
    entryComponents: [AuthenticationResendComponent],
    providers: [
        PROVIDERS_AUTH,
        // AuthGuardAuth,
        INTERCEPTOR_PORTAL_CLIENT,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AppHttpErrorAuth,
            multi: true
        },
        { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorClient, multi: true },
        {
            provide: AuthServiceConfig,
            useFactory: provideConfig
        }
    ]
})
export class AuthenticationModule { }
