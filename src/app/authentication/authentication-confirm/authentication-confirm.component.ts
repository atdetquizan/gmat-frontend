import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment.prod';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { BsModalService } from 'ngx-bootstrap';
import { AuthenticationResendComponent } from '../authentication-resend/authentication-resend.component';

@Component({
    selector: 'gmat-authentication-confirm',
    templateUrl: './authentication-confirm.component.html',
    styleUrls: ['./authentication-confirm.component.less']
})
export class AuthenticationConfirmComponent implements OnInit {
    form: FormGroup;
    user: any;
    loading = false;
    constructor(
        private fb: FormBuilder,
        private route: Router,
        private activatedRoute: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private sweetalertService: SweetalertService,
        private reactiveFormsService: ReactiveFormsService,
        private modalService: BsModalService,
        private router: Router
    ) {
        this.createfrm();
        try {
            // debugger;
            this.user = JSON.parse(atob(this.activatedRoute.snapshot.params.user));
        } catch (error) {
            this.route.navigate(['/']);
        }
    }

    ngOnInit() {
        // this.user = this.authenticationService.getUserClient();
        // if (this.user) {
        this.form.controls.id.setValue(this.user.userId);
        // } else {
        //     this.route.navigate(['/']);
        // }
    }

    onClickResend() {
        const initialState = {};
        const bsModalRef = this.modalService.show(AuthenticationResendComponent, {
            class: 'modal-dialog-centered',
            initialState
        });
    }

    confirm() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            this.loading = true;
            this.authenticationService
                .confirm_code({
                    client: this.form.value
                })
                .subscribe(
                    (res: any) => {
                        this.loading = false;
                        // if (res.status === 'success') {
                        localStorage.clear();
                        this.sweetalertService
                            .swal({
                                title:'Success',
                                text: 'The account was successfully activated.',
                                icon: environment.messages.iconsuccess
                            })
                            .then(() => {
                                localStorage.setItem('xser', btoa(JSON.stringify(res)));
                                this.router.navigate(['/portal']);
                            });
                        // if (res.status === 'error') {
                        //     localStorage.clear();
                        //     this.sweetalertService.swal({
                        //         title: res.status,
                        //         text: res.message,
                        //         icon: res.status
                        //     });
                        // }
                    },
                    (error: any) => {
                        this.loading = false;
                        if (error) {
                            this.sweetalertService.swal({
                                title: error.name,
                                text: error.message,
                                icon: environment.messages.iconerror
                            });
                        }
                    }
                );
        }
    }

    // private authentication() {
    //     let values: any;
    //     values.ttl = environment.ttl;
    //     values.type = environment.userstype.client;
    //     values.email = this.user.email;
    //     values.password = this.user.password;
    //     this.authenticationService.login(values).subscribe(res => {
    //         localStorage.setItem('xser', btoa(JSON.stringify(res)));
    //         this.router.navigate(['/portal']);
    //     });
    // }

    private createfrm() {
        this.form = this.fb.group({
            id: [null, null],
            code: [null, [Validators.required]]
        });
    }
}
