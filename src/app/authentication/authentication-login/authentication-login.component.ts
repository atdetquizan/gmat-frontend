import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { AuthenticationService } from '../authentication.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { tap } from 'rxjs/operators';
import { pipe } from 'rxjs';

@Component({
    selector: 'gmat-authentication-login',
    templateUrl: './authentication-login.component.html',
    styleUrls: ['./authentication-login.component.less']
})
export class AuthenticationLoginComponent implements OnInit {
    form: FormGroup;
    // erros: any;
    alert: any;
    loading = false;
    constructor(
        private fb: FormBuilder,
        private reactiveFormsService: ReactiveFormsService,
        private authenticationService: AuthenticationService,
        private router: Router,
        private sweetalertService: SweetalertService
    ) {
        this.creacionform();
    }

    ngOnInit() {}

    login() {
        // this.erros = null;
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            this.alert = null;
            this.loading = true;
            const values = this.form.value;
            values.ttl = environment.ttl;
            values.type = environment.userstype.client;
            this.authenticationService.login(values).subscribe(
                res => {
                    localStorage.setItem('xser', btoa(JSON.stringify(res)));
                    this.router.navigate(['/portal']);
                },
                err => {
                    this.loading = false;
                    console.log(err);
                    switch (err.code) {
                        case 'LOGIN_FAILED_EMAIL_NOT_VERIFIED':
                            this.router.navigate(['confirm/', btoa(JSON.stringify(err.details))]);
                            break;

                        default:
                            this.alert = {
                                type: 'danger',
                                title: environment.messages.error.title,
                                msg: err.message
                            };
                            break;
                    }
                    // if (err.name == 'HttpErrorResponse') {
                    //     // const error = err.error.error;
                    //     this.routerErrors('HttpErrorResponse', {
                    //         type: '',
                    //         name: 'Error, ',
                    //         message: 'There was a mishap try again in a few minutes'
                    //     });
                    // } else {
                    //     const error = err.error.error;
                    //     this.routerErrors(error['code'], error);
                    // }
                }
            );
        }
    }

    routerErrors(type, error) {
        switch (type) {
            case 'LOGIN_FAILED_EMAIL_NOT_VERIFIED':
                this.router.navigate([
                    './confirm',
                    btoa(
                        JSON.stringify({
                            userId: error.details.userId,
                            email: this.form.controls.email.value
                        })
                    )
                ]);
                // this.router.navigate(['confirm']);
                break;
            case 'HttpErrorResponse':
                this.alert = {
                    type: 'danger',
                    title: 'Error, ',
                    msg: 'Connection or response problems with the server.'
                };
                break;
            case 'LOGIN_FAILED':
                this.alert = {
                    type: 'danger',
                    title: 'Authentication, ',
                    msg: 'The user and / or password are incorrect.'
                };
                break;
            default:
                this.alert = {
                    type: 'warning m-0',
                    title: error.name,
                    msg: error.message
                };
                break;
        }
    }

    private creacionform() {
        this.form = this.fb.group({
            email: [null, [Validators.email, Validators.required]],
            password: [null, [Validators.required]]
        });
    }
}
