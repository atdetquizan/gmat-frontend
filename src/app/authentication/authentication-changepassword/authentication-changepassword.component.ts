import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment';
import { Route } from '@angular/compiler/src/core';
import { CustomValidators } from 'ngx-custom-validators';
import { ControlMessagesService } from 'src/app/tools/control-messages/control-messages.service';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';

@Component({
    selector: 'gmat-authentication-changepassword',
    templateUrl: './authentication-changepassword.component.html',
    styleUrls: ['./authentication-changepassword.component.less']
})
export class AuthenticationChangepasswordComponent implements OnInit {
    form: FormGroup;
    access_token: any;
    alert: any;
    loading = false;
    constructor(
        private fb: FormBuilder,
        private router: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private route: Router,
        private sweetalertService: SweetalertService,
        private reactiveFormsService: ReactiveFormsService,
    ) {
        this.access_token = this.router.snapshot.params.key;
        this.createfrm();
    }
    ngOnInit() { }

    changepassword() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            this.loading = true;
            this.authenticationService
                .reset_password(this.access_token, {
                    newPassword: this.form.value.newPassword
                })
                .subscribe(
                    res => {
                        this.sweetalertService
                            .swal({
                                title: environment.messages.changepassword.title,
                                icon: environment.messages.iconsuccess
                            })
                            .then(willDelete => {
                                if (willDelete) {
                                    this.loading = true;
                                    this.form.reset();
                                    this.route.navigate(['/']);
                                }
                            });
                    },
                    error => {
                        if (error.statusCode) {
                            this.loading = false;
                            switch (error.statusCode) {
                                case 422:
                                case 404:
                                    this.alert = {
                                        type: 'danger',
                                        title: environment.messages.error.title,
                                        msg: error.message
                                    };
                                    break;

                                default:
                                    break;
                            }
                        } else {
                            this.alert = {
                                type: 'danger',
                                title: environment.messages.error.title,
                                msg: environment.messages.error.text
                            };
                        }
                    }
                );
        }
    }

    private createfrm() {
        // let password = new FormControl(null, [Validators.required, Validators.minLength(6)]);
        // let certainPassword = new FormControl(null, [
        //     Validators.required,
        //     Validators.minLength(6),
        //     CustomValidators.equalTo(password),
        //     ControlMessagesService.matchOtherValidator('password')
        // ]);
        // this.form = this.fb.group({
        //     password: password,
        //     newPassword: certainPassword
        // });
        this.form = this.fb.group({
            password: [null, [Validators.required, Validators.minLength(6)]],
            newPassword: [null,
                [
                    Validators.required,
                    Validators.minLength(6),
                    ControlMessagesService.matchOtherValidator('password')
                ]]
        });
    }
}
