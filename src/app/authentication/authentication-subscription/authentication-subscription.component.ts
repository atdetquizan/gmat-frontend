import { Component, OnInit } from '@angular/core';
import { AdminPricingService } from 'src/app/admin/admin-pricing/admin-pricing.service';
import { map } from 'rxjs/operators';
import { MapSubscription } from 'src/app/tools/common/gmat-functions';

@Component({
    selector: 'gmat-authentication-subscription',
    templateUrl: './authentication-subscription.component.html',
    styleUrls: ['./authentication-subscription.component.less']
})
export class AuthenticationSubscriptionComponent implements OnInit {
    subscriptions: any;
    constructor(private adminPricingService: AdminPricingService) {
        this.showData();
    }

    ngOnInit() { }

    private showData() {
        this.adminPricingService
            .get()
            .pipe(MapSubscription())
            .subscribe(res => {
                this.subscriptions = res;
            });
    }
}
