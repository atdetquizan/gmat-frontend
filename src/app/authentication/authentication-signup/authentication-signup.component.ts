import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, OnChanges, ɵConsole, DoCheck } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { AdminPricingService } from 'src/app/admin/admin-pricing/admin-pricing.service';
import { loadScript } from 'src/app/tools/common/gmat-functions';
import { ControlMessagesService } from 'src/app/tools/control-messages/control-messages.service';
import { PricingType } from 'src/app/tools/types/pricing.type';
import { interval } from 'rxjs';
import { take } from 'rxjs/operators';
import { CustomValidators } from 'ngx-custom-validators';
import { AuthService, LinkedInLoginProvider, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';

declare const Culqi: any;

@Component({
    selector: 'gmat-authentication-signup',
    templateUrl: './authentication-signup.component.html',
    styleUrls: ['./authentication-signup.component.less']
})
export class AuthenticationSignupComponent implements OnInit, DoCheck {
    form: FormGroup;
    tokenculqi: any;
    pricingId: any;
    checkcard = false;
    culqiInit: any;
    loading = false;
    alert: any;
    pricingType = PricingType;
    type: PricingType;
    card: any;
    constructor(
        private authService: AuthService,
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private adminPricingService: AdminPricingService,
        private sweetalertService: SweetalertService,
        private authenticationService: AuthenticationService,
        private reactiveFormsService: ReactiveFormsService,
        private router: Router
    ) {
        this.pricingId = this.activatedRoute.snapshot.params.id;
        this.createfrm();
        this.form.controls.subscriptionId.setValue(this.pricingId);
        loadScript('https://checkout.culqi.com/js/v3').then(e => {
            this.culqiInit = Culqi;
            this.culqiInit.publicKey = environment.qulqiKey;
            this.loadPricing(this.pricingId);
        });
    }

    ngOnInit() {
        //
    }

    ngDoCheck() {
        if (this.culqiInit && this.culqiInit.token) {
            console.log(this.culqiInit.token);
            this.checkcard = true;
            this.card = {};
            this.card.number = this.culqiInit.token.card_number;
            this.card.tokenId = this.culqiInit.token.id;
        }
    }

    signup() {
        if (!this.checkcard && this.type !== this.pricingType.FREE) {
            this.alert = {
                type: 'danger',
                title: environment.messages.errorcar.title,
                msg: environment.messages.errorcar.text
            };
            return;
        }
        this.alert = null;
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            this.loading = true;
            this.authenticationService
                .register({
                    client: this.form.value,
                    card: this.card
                })
                .subscribe(
                    res => {
                        this.loading = false;
                        this.alert = {
                            type: 'success',
                            title: environment.messages.save.title,
                            msg: environment.messages.save.text
                        };
                        this.form.reset();
                        interval(3000)
                            .pipe(take(1))
                            .subscribe(x => this.router.navigate(['./']));
                    },
                    (error: any) => {
                        this.loading = false;
                        if (error.statusCode) {
                            switch (error.statusCode) {
                                case 422:
                                case 400:
                                    this.alert = {
                                        type: 'danger',
                                        title: environment.messages.error.title,
                                        msg: error.message
                                    };
                                    break;

                                default:
                                    break;
                            }
                        } else {
                            this.alert = {
                                type: 'danger',
                                title: environment.messages.error.title,
                                msg: environment.messages.error.text
                            };
                        }
                    }
                );
        }
    }

    onChangeMethodPaymet(e) {
        this.culqiInit.open();
        e.preventDefault();
    }

    signInWithLinkedIn() {
        // get user with LinkedIn
        this.authService.signIn(LinkedInLoginProvider.PROVIDER_ID);
        // get data user
        this.authService.authState.subscribe((user) => {
            console.log(user);
        });
        // cerrar sesion
        this.authService.signOut();
    }

    signInWithGoogle() {
        // get user with Google
        this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
        // get data user
        this.authService.authState.subscribe((user) => {
            console.log(user);
            if (user) {
                this.form.controls.firstName.setValue(user.firstName);
                this.form.controls.lastName.setValue(user.lastName);
                this.form.controls.email.setValue(user.email);
            }
        });
        // cerrar sesion
        this.authService.signOut();
    }

    signInWithFB() {
        // get user with Facebook
        this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
        // get data user
        this.authService.authState.subscribe((user) => {
            console.log(user);
        });
        // cerrar sesion
        this.authService.signOut();
    }

    private loadPricing(id: any) {
        this.adminPricingService.getById(id).subscribe((res: any) => {
            console.log(res);
            this.type = res.type;
            this.culqiInit.settings({
                title: res.name,
                currency: res.currency_code,
                description: res.description,
                amount: res.amount
            });
        });
    }

    private createfrm() {
        this.form = this.fb.group({
            firstName: [null, [Validators.required]],
            lastName: [null, [Validators.required]],
            email: [null, [Validators.required, CustomValidators.email]],
            password: [null, [Validators.required]],
            repeatpassword: [null, [Validators.required, ControlMessagesService.matchOtherValidator('password')]],
            cellphone: [null, [Validators.required, CustomValidators.digits, Validators.minLength(9), Validators.maxLength(9)]],
            subscriptionId: [null, null]
        });
    }
}
