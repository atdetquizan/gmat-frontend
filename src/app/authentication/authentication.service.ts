import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    // updateuser: EventEmitter<any> = new EventEmitter();
    constructor(private http: HttpClient) {}

    login(parameters: any) {
        return this.http.post(`${environment.apiurl}/clients/login?include=user`, parameters);
    }

    register(parameters: any) {
        return this.http.post(`${environment.apiurl}/clients/register`, { data: parameters });
    }

    confirm_code(parameters: any) {
        return this.http.post(`${environment.apiurl}/clients/confirm-code`, parameters);
    }

    reset(parameters: any) {
        return this.http.post(`${environment.apiurl}/clients/reset`, parameters);
    }

    reset_password(access_token: any, parameters: any) {
        return this.http.post(
            `${environment.apiurl}/clients/reset-password?access_token=${access_token}`,
            parameters
        );
    }

    getResend(params: any) {
        return this.http.get(`${environment.apiurl}/clients/resend-code`, { params });
    }

    postCheckClient(clientId: number) {
        return this.http.post(`${environment.apiurl}/clients/check-client`, { clientId });
    }

    /* LOCAL STORE */
    getUserClient() {
        return localStorage.getItem('xser') ? JSON.parse(atob(localStorage.getItem('xser'))) : null;
    }

    updateUserClient(user: any) {
        let xser = JSON.parse(atob(localStorage.getItem('xser')));
        if (xser) {
            xser['user'] = user;
            localStorage.setItem('xser', btoa(JSON.stringify(xser)));
            // this.updateuser.emit(true);
        }
    }
}
