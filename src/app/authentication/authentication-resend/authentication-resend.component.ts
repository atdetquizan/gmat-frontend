import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment.prod';
import { BsModalRef } from 'ngx-bootstrap';
import { CustomValidators } from 'ngx-custom-validators';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';

@Component({
    selector: 'gmat-authentication-resend',
    templateUrl: './authentication-resend.component.html',
    styleUrls: ['./authentication-resend.component.less']
})
export class AuthenticationResendComponent implements OnInit {
    form: FormGroup;
    loading = false;
    constructor(
        private _fb: FormBuilder,
        private authenticationService: AuthenticationService,
        private sweetalertService: SweetalertService,
        private bsModalRef: BsModalRef,
        private reactiveFormsService: ReactiveFormsService
    ) {
        this.createform();
    }

    ngOnInit() {}

    onClickResend() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (!this.form.valid) return;
        this.loading = true;
        this.form.disable();
        this.authenticationService.getResend(this.form.value).subscribe(
            (res: any) => {
                this.loading = false;
                this.form.enable();
                this.sweetalertService
                    .swal({
                        title: res.status,
                        text: res.message,
                        icon: res.status
                    })
                    .then(() => {
                        this.form.enable();
                        this.loading = true;
                        this.bsModalRef.hide();
                    });
            },
            (error: any) => {
                this.loading = false;
                this.form.enable();
                if (error) {
                    this.sweetalertService.swal({
                        title: error.name,
                        text: error.message,
                        icon: environment.messages.iconerror
                    });
                }
            }
        );
    }

    onClickClose() {
        this.bsModalRef.hide();
    }

    private createform() {
        this.form = this._fb.group({
            email: [null, [Validators.required, CustomValidators.email]]
        });
    }
}
