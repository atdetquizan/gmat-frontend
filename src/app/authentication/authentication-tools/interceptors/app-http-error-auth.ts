import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
    HttpInterceptor,
    HttpHandler,
    HttpEvent,
    HttpRequest
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()

export class AppHttpErrorAuth implements HttpInterceptor {
    constructor() { }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError(err => {
                console.log(err);
                if (err.status === 401) {
                    // auto logout if 401 response returned from api
                    // this.authenticationService.logout();
                    // location.reload(true);
                }
                const error = err.error.error;
                return throwError(error);
            })
        );
    }
}
