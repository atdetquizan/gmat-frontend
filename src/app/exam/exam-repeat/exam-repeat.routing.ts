import { Routes, RouterModule } from '@angular/router';
import { ExamRepeatComponent } from './exam-repeat.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
    {
        path: '',
        component: ExamRepeatComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExamRepeatRouting { }
