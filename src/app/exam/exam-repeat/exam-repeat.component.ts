import { Component, OnInit, ViewChild } from '@angular/core';
import { QuestionFormatType } from 'src/app/admin/admin-questions/admin-questions-shared/question-format-type.enum';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { ChronometerComponent } from 'src/app/tools/chronometer/chronometer.component';
import { ExamService } from '../exam.service';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { BsModalService } from 'ngx-bootstrap';
import { ExamResolveType } from 'src/app/tools/types/exam-resolve.type';
import { QuestionActionType } from '../exam-tools/question-action-type.enum';
import { format } from 'src/app/tools/common/gmat-functions';
import { ExamIndexModalComponent } from '../exam-tools/exam-index-modal/exam-index-modal.component';

@Component({
    selector: 'gmat-exam-repeat',
    templateUrl: './exam-repeat.component.html',
    styleUrls: ['./exam-repeat.component.less']
})
export class ExamRepeatComponent implements OnInit {
    questionIndex = 0;
    user: any;
    // questionActive: any;
    queryParams: any;
    form: FormGroup;
    // maxQuestions: any;
    // questionCodes = [];
    // questionType: QuestionFormatType;
    questionFormatType = QuestionFormatType;
    investedTime = 0;
    totalInvestedTime = 0;
    totalQuestions = 0;
    // uniqueExamId: string;
    questionList = [];

    @ViewChild('chronometerQuestion') chronometerQuestion: ChronometerComponent;
    @ViewChild('chronometerExam') chronometerExam: ChronometerComponent;

    constructor(
        private examService: ExamService,
        private authenticationService: AuthenticationService,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder,
        private reactiveFormsService: ReactiveFormsService,
        private loadingBar: LoadingBarService,
        private bsModalService: BsModalService,
        private router: Router
    ) {
        this.createform();
    }

    ngOnInit() {
        this.loadingBar.start();
        this.user = this.authenticationService.getUserClient().user;
        this.activatedRoute.params.subscribe((params: any) => {
            this.queryParams = JSON.parse(atob(params.questions));
            this.loadQuestionList().subscribe((res: any) => {
                this.questionList = res;
                this.form.controls.clientId.setValue(this.user.id);
                this.form.controls.clientTestId.setValue(
                    this.queryParams.type === ExamResolveType.RESOLVE_MARK
                        ? this.queryParams.clientTestId
                        : this.queryParams.testId
                );
                this.createQuestion();
                this.loadingBar.stop();
            });
        });
    }

    onHasBack() {
        return this.questionIndex === 0;
    }

    onHasNext() {
        return this.questionIndex + 1 === this.questionList.length;
    }

    onHasFinish() {
        return this.questionIndex + 1 !== this.questionList.length;
    }

    onNavExam(event: any) {
        if (QuestionActionType.next === event) {
            const question = this.form.controls.questions as FormArray;
            const answersId = (question.controls[
                this.questionIndex
            ] as FormGroup).controls.answersId;
            answersId.markAsTouched();
            if (this.form.valid) {
                if (!this.hasClientAnswer()) {
                    this.setInvestedTime();
                    this.questionIndex++;
                    this.questionList[
                        this.questionIndex - 1
                    ].question.clientAnswer = answersId.value;
                } else {
                    this.increaseQuestion();
                }
            } else if (this.hasClientAnswer()) {
                this.increaseQuestion();
            }
        }
        if (QuestionActionType.back === event) {
            this.questionIndex--;
            this.setInvestedTimeByQuestion();
        }
    }

    onNextExam() {
        //
    }

    onFinishExam() {
        const controls: any = this.form.value;
        if (this.form.valid) {
            this.chronometerQuestion.stopTimer();
            this.setInvestedTime();
            const params = {
                clientId: controls.clientId,
                clientTestId: controls.clientTestId,
                questions: controls.questions.map((x: any) => {
                    x.clientAnswer = x.answersId;
                    delete x.answersId;
                    return x;
                }),
                totalInvestedTime: controls.totalInvestedTime
            };
            this.loadingBar.start();
            this.examService.postValidateTest(params).subscribe((res) => {
                this.loadingBar.stop();
                console.log(res);
                const initialState = {
                    title: 'Success',
                    message: 'The exam was completed successfully.'
                };
                this.bsModalService.show(ExamIndexModalComponent, {
                    class: 'modal-dialog-centered',
                    backdrop: 'static',
                    keyboard: false,
                    initialState
                });
            }, error => {
                this.loadingBar.complete();
                const response = error.error.error;
                if (error.status === 400) {
                    const initialState = {
                        title: response.name,
                        message: response.message
                    };
                    this.bsModalService.show(ExamIndexModalComponent, {
                        class: 'modal-dialog-centered',
                        backdrop: 'static',
                        keyboard: false,
                        initialState
                    });
                }
            });
        }
    }

    onValueTotalInvestedTime(value: any) {
        this.totalInvestedTime = value;
    }

    onValueIvestedTime(value: any) {
        this.investedTime = value;
    }

    formArrayQuestionByIndex(index: number) {
        const formArray = this.form.controls.questions as FormArray;
        return formArray.controls[index] as FormGroup;
    }

    formArrayQuestion() {
        return (this.form.controls.questions as FormArray).controls;
    }

    private hasClientAnswer() {
        return this.questionList[this.questionIndex] &&
            this.questionList[this.questionIndex].question &&
            this.questionList[this.questionIndex].question.clientAnswer
    }

    private increaseQuestion() {
        this.questionIndex++;
        if (this.hasClientAnswer()) {
            this.setInvestedTimeByQuestion();
        } else {
            this.chronometerQuestion.restart();
        }
    }

    private setInvestedTimeByQuestion() {
        const question = this.form.controls.questions as FormArray;
        const investedTime = (question.controls[
            this.questionIndex
        ] as FormGroup).controls.investedTime;
        this.chronometerQuestion.stopTimer();
        this.chronometerQuestion.timer.nativeElement.innerHTML = format(
            investedTime.value
        );
    }

    private setInvestedTime() {
        this.chronometerQuestion.stopTimer();
        const question = this.form.controls.questions as FormArray;
        (question.controls[
            this.questionIndex
        ] as FormGroup).controls.investedTime.setValue(this.investedTime);
        const totalInvestedTime = this.form.controls.totalInvestedTime.value;
        this.form.controls.totalInvestedTime.setValue(
            totalInvestedTime + this.investedTime
        );
        if (this.questionIndex + 1 !== this.questionList.length) {
            this.chronometerQuestion.restart();
        }
    }

    private loadQuestionList() {
        const params: any = {
            clientId: this.user.id
        };
        switch (this.queryParams.type) {
            case ExamResolveType.RESOLVE_MARK:
                params.testDetailIds = this.queryParams.testDetailIds;
                params.clientTestId = this.queryParams.clientTestId;
                return this.examService.postResolveMark(params);

            default:
                params.testId = this.queryParams.testId;
                return this.examService.postTryAgain(params);
        }
    }

    private createQuestion() {
        const questions = this.form.controls.questions as FormArray;
        for (const item of this.questionList) {
            questions.push(this.addQuestion(item));
        }
    }

    private addQuestion(question: any) {
        console.log(question);
        return this.fb.group({
            clientTestDetailId: [question.id, null],
            answersId: [null, null],
            investedTime: [null, null],
            questionId: [question.question.id, null]
        });
    }

    private createform() {
        this.form = this.fb.group({
            clientId: [null, null],
            clientTestId: [null, null],
            questions: this.fb.array([]),
            totalInvestedTime: [null, null]
        });
    }
}
