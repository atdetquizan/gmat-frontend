import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamRepeatComponent } from './exam-repeat.component';
import { ExamConfirmModalModule } from '../exam-confirm-modal/exam-confirm-modal.module';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng5SliderModule } from 'ng5-slider';
import { ExamService } from '../exam.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ExamIndexRouting } from '../exam-index/exam-index.routing';
import { ModalModule } from 'ngx-bootstrap';
import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { ToolsModule } from 'src/app/tools/tools.module';
import { ExamToolsModule } from '../exam-tools/exam-tools.module';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { AppHttpInterceptorExam } from 'src/app/interceptors';
import { ExamRepeatRouting } from './exam-repeat.routing';

const PROVIDERS_EXAM_REPEAT = [ExamService];

@NgModule({
    declarations: [
        ExamRepeatComponent
    ],
    imports: [
        CommonModule,
        ExamRepeatRouting,
        Ng5SliderModule,
        HttpClientModule,
        ReactiveFormsModule,
        ModalModule.forRoot(),
        ExamConfirmModalModule,
        ControlMessagesModule,
        ToolsModule,
        ExamToolsModule,
        LoadingBarModule
    ],
    providers: [
        PROVIDERS_EXAM_REPEAT,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AppHttpInterceptorExam,
            multi: true
        }
    ]
})
export class ExamRepeatModule { }
