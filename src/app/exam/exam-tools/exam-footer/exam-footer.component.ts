import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { QuestionActionType } from '../question-action-type.enum';

@Component({
    selector: 'gmat-exam-footer',
    templateUrl: './exam-footer.component.html',
    styleUrls: ['./exam-footer.component.less']
})
export class ExamFooterComponent implements OnInit {
    questionActionType = QuestionActionType;
    @Input() hasBack = false;
    @Input() hasNext = false;
    @Input() hasFinish = true;
    @Input() hasQuestionCurrent = false;
    @Output() onNavigation: EventEmitter<any> = new EventEmitter();
    @Output() onFinish: EventEmitter<any> = new EventEmitter();
    @Output() onSubmit: EventEmitter<any> = new EventEmitter();
    constructor() { }

    ngOnInit() { }

    onClickNavigation(type: QuestionActionType) {
        this.onNavigation.emit(type);
    }

    onClickFinish() {
        this.onFinish.emit([]);
    }

    onClickSubmit(type: QuestionActionType) {
        this.onSubmit.emit([]);
    }
}
