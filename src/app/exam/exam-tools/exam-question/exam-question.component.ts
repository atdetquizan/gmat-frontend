import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import Quill from 'quill';

@Component({
    selector: 'gmat-exam-question',
    templateUrl: './exam-question.component.html',
    styleUrls: ['./exam-question.component.less']
})
export class ExamQuestionComponent implements OnInit {
    @ViewChild('question') questioncontent: ElementRef;
    @Input() description: any;
    constructor() {}

    ngOnInit() {
        const txtQuill = new Quill(this.questioncontent.nativeElement);
    }
}
