export enum QuestionActionType {
    next = 'next',
    back = 'back',
    submit = 'submit'
}
