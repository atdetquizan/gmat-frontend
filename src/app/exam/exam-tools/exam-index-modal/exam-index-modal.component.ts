import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
    selector: 'gmat-exam-index-modal',
    templateUrl: './exam-index-modal.component.html',
    styleUrls: ['./exam-index-modal.component.less']
})
export class ExamIndexModalComponent implements OnInit {
    title: string;
    message: string;
    constructor(private router: Router, private bsModalRef: BsModalRef) { }

    ngOnInit() {
    }

    onClickAccept() {
        this.bsModalRef.hide();
        this.router.navigate(['/portal/platform/exam']);
    }

}
