import { Component, OnInit, Input } from '@angular/core';
import { ExamConfirmModalComponent } from '../../exam-confirm-modal/exam-confirm-modal.component';
import { BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';

@Component({
    selector: 'gmat-exam-header',
    templateUrl: './exam-header.component.html',
    styleUrls: ['./exam-header.component.less']
})
export class ExamHeaderComponent implements OnInit {
    constructor(private bsModalService: BsModalService, private router: Router) { }

    ngOnInit() { }

    onClickHideMenu() {
        const header = document.querySelector('.header-content');
        header.classList.toggle('hide-content-menu');
        const showheader = document.querySelector('.show-menu-content');
        showheader.classList.toggle('hide-content-menu');
    }

    onClickExit() {
        const initialState = {
            title: 'CONFIRM EXIT',
            message:
                'You are about to exit this practice question set without completing it. If you choose to exit this now your partial results will be saved.'
        };
        const bsModalRef = this.bsModalService.show(ExamConfirmModalComponent, {
            class: 'modal-dialog-centered',
            initialState
        });
        bsModalRef.content.eventClosed.subscribe((res: any) => {
            if (res) {
                this.router.navigate(['/portal/platform/exam']);
            }
        });
    }
}
