import { NgModule } from '@angular/core';
import { ExamQuestionComponent } from './exam-question/exam-question.component';
import { ExamHeaderComponent } from './exam-header/exam-header.component';
import { ExamFooterComponent } from './exam-footer/exam-footer.component';
import { CommonModule } from '@angular/common';
import { ExamConfirmModalModule } from '../exam-confirm-modal/exam-confirm-modal.module';
import { RouterModule } from '@angular/router';
import { ToolsModule } from 'src/app/tools/tools.module';
import { ExamIndexModalComponent } from './exam-index-modal/exam-index-modal.component';

const components = [ExamQuestionComponent, ExamHeaderComponent, ExamFooterComponent, ExamIndexModalComponent];
@NgModule({
    declarations: [...components],
    imports: [CommonModule, RouterModule, ExamConfirmModalModule, ToolsModule],
    entryComponents: [ExamIndexModalComponent],
    exports: [...components]
})
export class ExamToolsModule { }
