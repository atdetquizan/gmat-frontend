import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamComponent } from './exam.component';
import { ExamRouting } from './exam.routing';

@NgModule({
    imports: [CommonModule, ExamRouting],
    declarations: [ExamComponent],
})
export class ExamModule {}
