import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import {
    FormArray,
    FormControl,
    FormBuilder,
    FormGroup,
    Validators,
    ValidatorFn
} from '@angular/forms';
import {
    minSelectedCheckboxes,
    decryptJS,
    isEmptyObject
} from 'src/app/tools/common/gmat-functions';
import { QuestionFormatType } from 'src/app/admin/admin-questions/admin-questions-shared/question-format-type.enum';
import { ExamConfirmModalComponent } from '../exam-confirm-modal/exam-confirm-modal.component';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { BsModalService } from 'ngx-bootstrap';
import { Options } from 'ng5-slider';
import { QuestionActionType } from '../exam-tools/question-action-type.enum';
import { ExamService } from '../exam.service';
import { format } from '../../tools/common/gmat-functions';
import { tap, map } from 'rxjs/operators';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { OperatorFunction } from 'rxjs';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { ExamIndexModalComponent } from '../exam-tools/exam-index-modal/exam-index-modal.component';
import { ChronometerComponent } from 'src/app/tools/chronometer/chronometer.component';

@Component({
    selector: 'gmat-exam-index',
    templateUrl: './exam-index.component.html',
    styleUrls: ['./exam-index.component.less']
})
export class ExamIndexComponent implements OnInit {
    user: any;
    question: any;
    queryParams: any;
    form: FormGroup;
    maxQuestions: any;
    questionCodes = [];
    questionType: QuestionFormatType;
    questionFormatType = QuestionFormatType;
    investedTime = 0;
    totalInvestedTime = 0;
    totalQuestions = 0;
    uniqueExamId: string;

    @ViewChild('chronometerQuestion') chronometerQuestion: ChronometerComponent;
    @ViewChild('chronometerExam') chronometerExam: ChronometerComponent;
    constructor(
        private examService: ExamService,
        private authenticationService: AuthenticationService,
        private activatedRoute: ActivatedRoute,
        private fb: FormBuilder,
        private reactiveFormsService: ReactiveFormsService,
        private loadingBar: LoadingBarService,
        private bsModalService: BsModalService,
        private router: Router
    ) {
        this.createform();
    }

    ngOnInit() {
        this.activatedRoute.queryParams.subscribe((query: any) => {
            this.queryParams = decryptJS(query.code);
            this.user = this.authenticationService.getUserClient().user;
            this.showQuestion();
        });
    }

    onNavExam(questionActionType: QuestionActionType) {
        this.questionType = null;
        this.loadingBar.start();
        this.navQuestionBack(questionActionType);
    }

    onNextExam() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            this.questionType = null;
            this.form.controls.investedTime.setValue(this.investedTime);
            this.form.controls.totalInvestedTime.setValue(this.totalInvestedTime);
            this.loadingBar.start();
            this.nextQuestion();
        }
    }

    onFinishExam() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            const initialState = {
                title: 'CONFIRM SUBMIT',
                message: 'the exam question set? Yes/No'
            };
            const bsModalRef = this.bsModalService.show(
                ExamConfirmModalComponent,
                {
                    class: 'modal-dialog-centered',
                    initialState
                }
            );
            bsModalRef.content.eventClosed.subscribe((res: any) => {
                if (res) {
                    this.onNextExam();
                }
            });
        }
    }

    hasBack() {
        return this.question && this.question.questionOrder === 1
            ? true
            : false;
    }

    onValueIvestedTime(value: any) {
        this.investedTime = value;
    }

    onValueTotalInvestedTime(value: any) {
        this.totalInvestedTime = value;
    }

    private navQuestionBack(actionType: QuestionActionType) {
        let paramas: any = null;
        if (this.question.currentQuestion) {
            paramas = {
                maxQuestions: this.maxQuestions,
                question: this.question
            };
        }
        this.examService
            .postNavigationQuestion({
                questionOrder:
                    actionType === QuestionActionType.next
                        ? this.question.questionOrder + 1
                        : this.question.questionOrder - 1,
                clientTestId: this.question.clientTestId,
                uniqueExamId: this.uniqueExamId ? this.uniqueExamId : this.question.uniqueExamId,
                action: actionType,
                clientId: this.user.id,
                currentQuestion: paramas
            })
            .pipe(
                map((res: any) => {
                    console.log(res);
                    if (res.status === 'finished_exam') {
                        return res;
                    }
                    res.question.question.clientAnswer = res.clientAnswer;
                    this.uniqueExamId = res.uniqueExamId;
                    return res.question ? res.question : null;
                })
            )
            .pipe(this.pipeQuestion())
            .subscribe((res: any) => {
                if (res) {
                    this.loadingBar.complete();
                    this.subscribeQuestion(res);
                }
            });
    }

    private showQuestion() {
        this.loadingBar.start();
        this.examService
            .postStartQuestion({
                clientId: this.user.id,
                examConfigId: this.queryParams.code
            })
            .pipe(this.pipeQuestion())
            .subscribe(
                (res: any) => this.subscribeQuestion(res),
                error => {
                    this.loadingBar.complete();
                    const response = error.error.error;
                    if (error.status === 400) {
                        const initialState = {
                            title: response.name,
                            message: response.message
                        };
                        this.bsModalService.show(ExamIndexModalComponent, {
                            class: 'modal-dialog-centered',
                            backdrop: 'static',
                            keyboard: false,
                            initialState
                        });
                    }
                }
            );
    }

    private nextQuestion() {
        this.examService
            .postNextQuestion(this.form.value)
            .pipe(this.pipeQuestion())
            .subscribe((res: any) => this.subscribeQuestion(res));
    }

    private pipeQuestion(): OperatorFunction<any, any> {
        return map((res: any) => {
            if (res.status === 'finished_exam') {
                return res;
            }
            this.maxQuestions = res.maxQuestions;
            this.questionType = res.question.type;
            return res.question;
        });
    }

    private subscribeQuestion(res) {
        this.loadingBar.complete();
        if (res.status === 'finished_exam') {
            this.router.navigate(['/portal/platform/exam']);
            return;
        }
        this.question = res;
        if (this.question.totalQuestions) {
            this.totalQuestions = this.question.totalQuestions;
        }
        this.chronometerQuestion.restart();
        if (this.question.isFinalized) {
            this.chronometerExam.stopTimer();
            this.chronometerQuestion.stopTimer();
        }
        this.setValuesQuestion();
    }

    private setValuesQuestion() {
        this.form.reset();
        if (isEmptyObject(this.maxQuestions) && this.question.isAdaptive) {
            this.maxQuestions[this.question.topicInitials] =
                this.maxQuestions[this.question.topicInitials] - 1;
        }
        this.form.patchValue(
            Object.assign(this.question, {
                investedTime: this.investedTime,
                clientId: this.user.id,
                questionId: this.question.id,
                maxQuestions: this.maxQuestions,
                questionCodes: this.questionCodes
            })
        );
        this.questionCodes.push(this.question.code);
        this.form.controls.answersId.clearValidators();
        this.form.controls.answersId.updateValueAndValidity();
    }

    private createform() {
        this.form = this.fb.group({
            uniqueExamId: [null, null],
            clientTestId: [null, null],
            questionOrder: [null, null],
            investedTime: [null, null],
            totalInvestedTime: [null, null],
            type: [null, null],
            sectionType: [null, null],
            isAdaptive: [null, null],
            clientId: [null, null],
            questionId: [null, null],
            isFinalized: [null, null],
            questionCodes: [null, null],
            additionalCodes: [null, null],
            answersId: [null, null],
            totalQuestions: [null, null],
            maxQuestions: [null, null],
            orderByNode: [null, null]
        });
    }
}
