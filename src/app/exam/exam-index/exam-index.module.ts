import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamIndexComponent } from './exam-index.component';
import { ModalModule } from 'ngx-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Ng5SliderModule } from 'ng5-slider';
import { ExamConfirmModalModule } from '../exam-confirm-modal/exam-confirm-modal.module';
import { ExamIndexRouting } from './exam-index.routing';
import { ExamService } from '../exam.service';
import { ControlMessagesModule } from '../../tools/control-messages/control-messages.module';
import { ExamToolsModule } from '../exam-tools/exam-tools.module';
import { ToolsModule } from 'src/app/tools/tools.module';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { AppHttpInterceptorExam } from 'src/app/interceptors';

const PROVIDERS_EXAM_INDEX = [ExamService];
@NgModule({
    imports: [
        CommonModule,
        ExamIndexRouting,
        Ng5SliderModule,
        HttpClientModule,
        ReactiveFormsModule,
        ModalModule.forRoot(),
        ExamConfirmModalModule,
        ControlMessagesModule,
        ToolsModule,
        ExamToolsModule,
        LoadingBarModule
    ],
    declarations: [ExamIndexComponent],
    entryComponents: [],
    providers: [
        PROVIDERS_EXAM_INDEX,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AppHttpInterceptorExam,
            multi: true
        }
    ]
})
export class ExamIndexModule { }
