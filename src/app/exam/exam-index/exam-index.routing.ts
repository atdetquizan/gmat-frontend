import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExamIndexComponent } from './exam-index.component';

const routes: Routes = [
    {
        path: '',
        component: ExamIndexComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExamIndexRouting { }
