import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExamComponent } from './exam.component';
import { AuthActivePortalGuard } from '../tools/guards/auth-active-portal.guard';

const routes: Routes = [
    {
        path: '',
        component: ExamComponent,
        canActivateChild: [AuthActivePortalGuard],
        children: [
            {
                path: '',
                loadChildren: './exam-index/exam-index.module#ExamIndexModule'
            },
            {
                path: 'repeat-resolve/:questions',
                loadChildren: './exam-repeat/exam-repeat.module#ExamRepeatModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ExamRouting { }
