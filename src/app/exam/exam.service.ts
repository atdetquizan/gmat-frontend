import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ExamService {

    constructor(
        private http: HttpClient
    ) { }

    getListConfigs() {
        return this.http.get(`${environment.apiurl}/questions/exam/configs`);
    }

    postStartQuestion(parameters: any) {
        return this.http.post(`${environment.apiurl}/questions/exam`, parameters);
        // return this.http.post('http://demo5538380.mockable.io/questions', parameters);
    }

    postNextQuestion(parameters: any) {
        return this.http.post(`${environment.apiurl}/questions/next`, parameters)
    }

    postNavigationQuestion(parameters: any) {
        return this.http.post(`${environment.apiurl}/questions/navigate`, parameters);
    }

    postResolveMark(parameters: any) {
        return this.http.post(`${environment.apiurl}/client-tests/resolve-mark`, parameters);
    }

    postTryAgain(parameters: any) {
        return this.http.post(`${environment.apiurl}/client-tests/try-again`, parameters);
    }

    postValidateTest(parameters: any) {
        return this.http.post(`${environment.apiurl}/questions/validate-test`, parameters)
    }
}
