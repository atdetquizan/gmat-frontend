import { Component, OnInit, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
    selector: 'gmat-exam-confirm-modal',
    templateUrl: './exam-confirm-modal.component.html',
    styleUrls: ['./exam-confirm-modal.component.less']
})
export class ExamConfirmModalComponent implements OnInit {
    title: string;
    message: string;
    actions = true;
    public eventClosed: EventEmitter<any> = new EventEmitter();
    constructor(
        public bsModalRef: BsModalRef
    ) { }

    ngOnInit() {
    }

    onClickYes() {
        this.eventClosed.emit(true);
        this.bsModalRef.hide();
    }

}
