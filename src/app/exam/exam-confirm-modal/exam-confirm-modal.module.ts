import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap';
import { ExamConfirmModalComponent } from './exam-confirm-modal.component';

const PROVIDERS_EXAM_INDEX = [
];

@NgModule({
    imports: [
        CommonModule,
        ModalModule.forRoot()
    ],
    declarations: [
        ExamConfirmModalComponent
    ],
    entryComponents: [
        ExamConfirmModalComponent
    ],
    providers: [
        PROVIDERS_EXAM_INDEX,
        // { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorClient, multi: true }
    ]
})
export class ExamConfirmModalModule { }
