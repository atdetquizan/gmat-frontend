import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AdminUsersService } from '../admin-users.service';
import { PaginationType } from '../../admin-tools/page-pagination/page-pagination-tools/page-pagination-type.enum';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'gmat-admin-users-list',
    templateUrl: './admin-users-list.component.html',
    styleUrls: ['./admin-users-list.component.less']
})
export class AdminUsersListComponent implements OnInit {

    form: FormGroup;
    users: any;
    totales: any;
    @ViewChild('pagination') pagination: any;
    constructor(
        private fb: FormBuilder,
        private adminUsersService: AdminUsersService,
    ) {
        this.createform();
    }

    ngOnInit() {
        this.showData();
    }

    onKeyupSearch() {
        this.showData();
    }

    setColumns() {
        return [
            {
                title: 'ID',
                field: 'id'
            },
            {
                title: 'Last Name',
                field: 'lastName'
            },
            {
                title: 'Email',
                field: 'email'
            },
            {
                title: 'Phone',
                field: 'cellphone'
            },
            {
                title: 'Plan',
                field: 'subscriptionName'
            },
            {
                title: 'Reg Datos',
                field: 'createdAt',
                type: PaginationType.datetime
            },
        ]
    }

    showData(page?: number) {
        this.pagination.loading = true;
        let parameters: any = {};
        parameters.perPage = environment.pagination.adminpanel;
        parameters.page = page ? page : 1;
        if (this.form.controls.filter.value) {
            parameters.word = this.form.controls.filter.value;
        }
        this.adminUsersService.get(parameters).subscribe((res: any) => {
            if (res.data) {
                this.users = res;
                this.totales = res.data;
            }
        }, () => {
            this.users = [];
        });
    }

    private createform() {
        this.form = this.fb.group({
            filter: [null, null]
        });
    }

}
