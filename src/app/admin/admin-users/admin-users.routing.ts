import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminUsersComponent } from './admin-users.component';
import { AdminUsersListComponent } from './admin-users-list/admin-users-list.component';

const routes: Routes = [
    {
        path: '',
        component: AdminUsersComponent,
        children: [
            {
                path: 'list',
                component: AdminUsersListComponent
            },
            {
                path: '',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminUsersRoutes { }

