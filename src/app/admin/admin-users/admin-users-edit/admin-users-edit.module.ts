import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminUsersEditComponent } from './admin-users-edit.component';
import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { AdminUsersService } from '../admin-users.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';

@NgModule({
    imports: [
        CommonModule,
        ControlMessagesModule,
        ReactiveFormsModule,
        ModalModule.forRoot()
    ],
    declarations: [
        AdminUsersEditComponent
    ],
    entryComponents: [
        AdminUsersEditComponent
    ],
    providers: [
        AdminUsersService,
        { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorAdmin, multi: true }
    ]
})
export class AdminUsersEditModule { }
