import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { AdminUsersService } from '../admin-users.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { CustomValidators } from 'ngx-custom-validators';

@Component({
    selector: 'gmat-admin-users-edit',
    templateUrl: './admin-users-edit.component.html',
    styleUrls: ['./admin-users-edit.component.less']
})
export class AdminUsersEditComponent implements OnInit {
    title: string;
    form: FormGroup;
    user: any;
    public eventClosed: EventEmitter<any> = new EventEmitter();
    constructor(
        private fb: FormBuilder,
        private adminUsersService: AdminUsersService,
        private sweetalertService: SweetalertService,
        private auth: AuthenticationService,
        private reactiveFormsService: ReactiveFormsService,
        public bsModalRef: BsModalRef
    ) {
        this.createform();
    }

    ngOnInit() {
        if (this.user) {
            this.form.patchValue(this.user);
        }
    }

    save() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            this.adminUsersService.patch(this.user.id, this.form.value).subscribe((res) => {
                this.auth.updateUserClient(res);
                this.form.reset();
                this.sweetalertService.swal({
                    title: environment.messages.save.title,
                    text: environment.messages.save.text,
                    icon: environment.messages.iconsuccess
                }).then(() => {
                    this.eventClosed.emit(true);
                    this.bsModalRef.hide();
                });
            });
        }
    }

    onClickClose() {
        this.bsModalRef.hide();
    }

    private createform() {
        this.form = this.fb.group({
            firstName: [null, [Validators.required]],
            lastName: [null, [Validators.required]],
            email: [{ value: null, disabled: true }, [Validators.required]],
            cellphone: [null, [Validators.required, CustomValidators.digits]]
        })
    }

}
