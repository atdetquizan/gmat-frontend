import { AdminUsersService } from './admin-users.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminUsersComponent } from './admin-users.component';
import { AdminUsersRoutes } from './admin-users.routing';
import { AdminUsersListComponent } from './admin-users-list/admin-users-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { AdminToolsModule } from '../admin-tools/admin-tools.module';
import { PerfectScrollbarModule, PerfectScrollbarConfigInterface, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PagePaginationModule } from '../admin-tools/page-pagination/page-pagination.module';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    // suppressScrollX: true
};

const PROVIDERS_USERS = [
    AdminUsersService
]

@NgModule({
    imports: [
        AdminUsersRoutes,
        CommonModule,
        ReactiveFormsModule,
        BsDropdownModule.forRoot(),
        AdminToolsModule,
        HttpClientModule,
        PerfectScrollbarModule,
        PagePaginationModule
    ],
    declarations: [
        AdminUsersComponent,
        AdminUsersListComponent
    ],
    providers: [
        PROVIDERS_USERS,
        { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorAdmin, multi: true },
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ]
})
export class AdminUsersModule { }
