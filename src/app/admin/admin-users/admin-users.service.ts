import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminUsersService {
    constructor(private http: HttpClient) {}

    get(parameters) {
        return this.http.get(`${environment.apiurl}/clients/list`, { params: parameters });
    }

    patch(id: number, parameters: any) {
        return this.http.patch(`${environment.apiurl}/clients/${id}`, parameters);
    }

    getReportsList(id: number) {
        return this.http.get(`${environment.apiurl}/client-section-reports/client/${id}/list`);
    }

    getReportsDetailList(sectionId: number) {
        return this.http.get(`${environment.apiurl}/client-section-reports/detail/${sectionId}`);
    }

    getTestTopicReportSubjects(topicId: number) {
        return this.http.get(`${environment.apiurl}/test-topic-report/${topicId}/test-subjects`);
    }

    // OBTENER DATOS EXTRAS DE LA TARJETA DE CREDITO DESDE QULQI
    postCardQulqi(clientId: number) {
        return this.http.post(`${environment.apiurl}/cards/culqi`, { clientId });
    }

    // RENOVAR SUSCRIPCION DEL CLIENTE
    postSubscriptionRenew(params: any) {
        return this.http.post(`${environment.apiurl}/subscriptions/renew`, params);
    }

    // CANCELAR SUBSCRIPCION
    postSupscriptionCancel(clientId: number) {
        return this.http.post(`${environment.apiurl}/subscriptions/cancel`, { clientId });
    }
}
