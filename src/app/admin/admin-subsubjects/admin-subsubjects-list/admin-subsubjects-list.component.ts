import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AdminSubsubjectsService } from '../admin-subsubjects.service';
import { AdminSubsubjectsNewComponent } from '../admin-subsubjects-new/admin-subsubjects-new.component';
import { environment } from 'src/environments/environment';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { AdminSubjectsService } from '../../admin-subjects/admin-subjects.service';
import { PaginationType } from '../../admin-tools/page-pagination/page-pagination-tools/page-pagination-type.enum';

@Component({
    selector: 'gmat-admin-subsubjects-list',
    templateUrl: './admin-subsubjects-list.component.html',
    styleUrls: ['./admin-subsubjects-list.component.less']
})
export class AdminSubsubjectsListComponent implements OnInit {
    form: FormGroup;
    bsModalRef: BsModalRef;
    subsubjects: any = [];
    subjects: any = [];
    subjectId: any;

    constructor(
        private modalService: BsModalService,
        public fb: FormBuilder,
        private adminSubsubjectsService: AdminSubsubjectsService,
        private sweetalertService: SweetalertService,
        private adminSubjectsService: AdminSubjectsService
    ) {
        this.createForm();
    }

    ngOnInit() {
        this.showData();
        this.showDataSelect();
    }

    clickOpenNew(subsubject?: any) {
        const initialState = {
            title: subsubject ? 'Edit Sub-subject' : 'Create Sub-subject',
            subsubject: subsubject
            // parameters
        };
        const modal = this.modalService.show(AdminSubsubjectsNewComponent, {
            class: 'modal-create-section modal-dialog-centered',
            initialState
        });
        modal.content.eventClosed.subscribe((res: boolean) => {
            if (res) {
                this.showData();
            }
        });
    }

    onClickDelete(item: any) {
        this.sweetalertService
            .swal({
                title: environment.messages.delete.title,
                text: environment.messages.delete.text,
                icon: 'warning',
                buttons: true,
                dangerMode: true
            })
            .then(willDelete => {
                if (willDelete) {
                    this.adminSubsubjectsService.delete(item.id).subscribe((res: any) => {
                        this.sweetalertService.swal(environment.messages.delete.text2, {
                            icon: 'success'
                        });
                        this.showData();
                    });
                }
            });
    }

    changeSubject(value) {
        // if (value) {
        //     this.adminSubsubjectsService.getBySubjectId(value).subscribe(res => {
        //         this.subsubjects = res;
        //     });
        // } else {
        this.subjectId = value;
        this.showData();
        // }
    }

    setColumns() {
        return [
            {
                title: 'Subsubjects',
                field: 'name'
            },
            {
                title: 'Image',
                field: 'image',
                type: PaginationType.image
            },
            {
                title: 'Subject',
                field: 'subjectName'
            },
            {
                title: 'Actions',
                class: 'text-center',
                type: PaginationType.buttons,
                buttons: {
                    delete: {
                        icon: 'far fa-trash-alt'
                    },
                    edit: {
                        icon: 'fas fa-edit'
                    }
                }
            }
        ];
    }

    showData(page?: any) {
        let parameters: any = {};
        parameters.perPage = environment.pagination.adminpanel;
        parameters.page = page ? page : 1;
        if (this.subjectId) {
            parameters.subjectId = this.subjectId;
        }
        if (this.form.controls.filter.value) {
            parameters.word = this.form.controls.filter.value;
        }
        this.adminSubsubjectsService.get(parameters).subscribe((res: any) => {
            this.subsubjects = res;
        });
    }

    private createForm() {
        this.form = this.fb.group({
            filter: [null, null]
        });
    }

    private showDataSelect() {
        this.adminSubjectsService.get().subscribe((res: any) => {
            this.subjects = res;
        });
    }
}
