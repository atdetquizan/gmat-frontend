import { PerfectScrollbarConfigInterface, PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { AdminSubsubjectsComponent } from './admin-subsubjects.component';
import { AdminSubsubjectsService } from './admin-subsubjects.service';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminSubsubjectsListComponent } from './admin-subsubjects-list/admin-subsubjects-list.component';
import { AdminSubsubjectsNewComponent } from './admin-subsubjects-new/admin-subsubjects-new.component';
import { AdminSubsubjectsRoutes } from './admin-subsujects.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminSubjectsService } from '../admin-subjects/admin-subjects.service';
import { AdminToolsModule } from '../admin-tools/admin-tools.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { PagePaginationModule } from '../admin-tools/page-pagination/page-pagination.module';

const PROVIDERS_AUTH = [
    AdminSubsubjectsService,
    AdminSubjectsService
]
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

@NgModule({
    imports: [
        CommonModule,
        AdminSubsubjectsRoutes,
        ReactiveFormsModule,
        AdminToolsModule,
        ModalModule.forRoot(),
        HttpClientModule,
        ControlMessagesModule,
        PerfectScrollbarModule,
        PagePaginationModule
    ],
    declarations: [
        AdminSubsubjectsComponent,
        AdminSubsubjectsListComponent,
        AdminSubsubjectsNewComponent
    ],
    entryComponents: [
        AdminSubsubjectsNewComponent
    ],
    providers: [
        PROVIDERS_AUTH,
        { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorAdmin, multi: true }
        , {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ]
})
export class AdminSubsubjectsModule { }
