import { BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdminSubsubjectsService } from '../admin-subsubjects.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { AdminSubjectsService } from '../../admin-subjects/admin-subjects.service';
import { environment } from 'src/environments/environment';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';

@Component({
    selector: 'gmat-admin-subsubjects-new',
    templateUrl: './admin-subsubjects-new.component.html',
    styleUrls: ['./admin-subsubjects-new.component.less']
})
export class AdminSubsubjectsNewComponent implements OnInit {
    form: FormGroup;
    title: string;
    subjects: any = [];
    subsubject: any;
    fileselection: any;
    namefile: string;
    public eventClosed: EventEmitter<any> = new EventEmitter();
    constructor(
        public bsModalRef: BsModalRef,
        private fb: FormBuilder,
        private adminSubsubjectsService: AdminSubsubjectsService,
        private sweetalertService: SweetalertService,
        private adminSubjectsService: AdminSubjectsService,
        private reactiveFormsService: ReactiveFormsService
    ) {
        this.creacionform();
    }

    ngOnInit() {
        this.showData();
        if (this.subsubject) {
            const file = this.subsubject.image ? (this.subsubject.image).split('/') : [];
            this.namefile = file.pop();
            // this.form.controls.filename.setValue(this.namefile);
            this.form.patchValue(this.subsubject);
            this.form.controls.filename.clearValidators();
            this.form.controls.filename.updateValueAndValidity();
        }
    }

    save() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (!this.form.valid)
            return;

        if (this.form.value.id) {
            this.adminSubsubjectsService.patch(this.form.value).subscribe((res: any) => {
                if (this.fileselection) {
                    this.adminSubsubjectsService.filePost(res.id, this.fileselection)
                        .subscribe(() => {
                            this.success();
                        });
                } else {
                    this.success();
                }
            });
        } else {
            this.adminSubsubjectsService.post(this.form.value).subscribe((res: any) => {
                if (this.fileselection) {
                    this.adminSubsubjectsService.filePost(res.id, this.fileselection)
                        .subscribe(() => {
                            this.success();
                        });
                }
            });
        }
    }

    success() {
        this.form.reset();
        this.sweetalertService.swal({
            title: environment.messages.save.title,
            text: environment.messages.save.text,
            icon: environment.messages.iconsuccess
        }).then(() => {
            this.eventClosed.emit(true);
            this.bsModalRef.hide();
        });
    }

    changeFile(e) {
        this.fileselection = e.target.files[0];
        this.namefile = this.fileselection.name;
    }

    private creacionform() {
        this.form = this.fb.group({
            id: [null, null],
            name: [null, [Validators.required]],
            subjectId: [null, [Validators.required]],
            filename: [null, [Validators.required]]
        });

    }

    private showData() {
        this.adminSubjectsService.get().subscribe((res: any) => {
            this.subjects = res;
        });
    }

}
