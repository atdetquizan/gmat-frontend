import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminSubsubjectsService {

    constructor(
        private http: HttpClient
    ) { }

    post(parameters: any) {
        return this.http.post(`${environment.apiurl}/sub-subjects`, parameters);
    }

    get(parameters?: any) {
        return this.http.get(`${environment.apiurl}/sub-subjects`, { params: parameters });
    }

    patch(parameters: any) {
        return this.http.patch(`${environment.apiurl}/sub-subjects/${parameters.id}`, parameters);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiurl}/sub-subjects/${id}`);
    }

    getBySubjectId(subjectId: number) {
        return this.http.get(`${environment.apiurl}/subjects/${subjectId}/subsubjects`);
    }

    filePost(id: number, file: any) {
        const formData: FormData = new FormData();
        formData.append('file', file);
        return this.http.post(`${environment.apiurl}/images/upload/sub-subjects/${id}`, formData);
    }

}

