import { AdminSubsubjectsComponent } from './admin-subsubjects.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminSubsubjectsListComponent } from './admin-subsubjects-list/admin-subsubjects-list.component';

const routes: Routes = [
    {
        path: '',
        component: AdminSubsubjectsComponent,
        children: [
            {
                path: 'list',
                component: AdminSubsubjectsListComponent
            },
            {
                path: '',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminSubsubjectsRoutes { }

