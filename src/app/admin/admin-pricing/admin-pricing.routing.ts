import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminPricingComponent } from './admin-pricing.component';
import { AdminPricingListComponent } from './admin-pricing-list/admin-pricing-list.component';

const routes: Routes = [
    {
        path: '',
        component: AdminPricingComponent,
        children: [
            {
                path: 'list',
                component: AdminPricingListComponent
            },
            {
                path: '',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminPricingRoutes { }
