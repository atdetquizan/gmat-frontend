import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AdminPricingNewComponent } from '../admin-pricing-new/admin-pricing-new.component';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AdminPricingService } from '../admin-pricing.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'gmat-admin-pricing-list',
    templateUrl: './admin-pricing-list.component.html',
    styleUrls: ['./admin-pricing-list.component.less']
})
export class AdminPricingListComponent implements OnInit {
    form: FormGroup;
    bsModalRef: BsModalRef;
    pricings: any = [];

    constructor(
        private modalService: BsModalService,
        private fb: FormBuilder,
        private adminPricingService: AdminPricingService,
        private sweetalertService: SweetalertService
    ) {
        this.createForm();
    }

    ngOnInit() {
        this.showData();
    }

    clickOpenNew() {
        const initialState = {
            title: 'Create pricing'
            // parameters
        };
        this.bsModalRef = this.modalService.show(AdminPricingNewComponent, 
            { class: 'modal-create-suscription modal-dialog-centered', initialState });
            
        this.bsModalRef.content.eventClosed.subscribe((res) => {
            if (res) {
                this.showData();
            }
        });
    }

    onClickDelete(id: number) {
        this.sweetalertService.swal({
            title: environment.messages.delete.title,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                this.adminPricingService.delete(id).subscribe((res: any) => {
                    this.sweetalertService.swal(environment.messages.delete.text2, {
                        icon: "success",
                    });
                    this.showData();
                })
            }
        });
    }
    search(value: any) {
        const content: any = document.querySelectorAll('.ContentSearch');
        for (const item of content) {
            const index = item.innerText.toUpperCase().indexOf(value.toUpperCase());
            if (index < 0) {
                item.style.display = 'none';
            } else {
                item.style.display = 'table-row';
            }
        }
    }
    
    private createForm() {
        this.form = this.fb.group({
        });
    }

    private showData() {
        this.adminPricingService.get().subscribe((res: any) => {
            this.pricings = res;
        });
    }
}
