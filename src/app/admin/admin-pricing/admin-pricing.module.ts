import { PerfectScrollbarModule, PerfectScrollbarConfigInterface, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPricingComponent } from './admin-pricing.component';
import { AdminPricingRoutes } from './admin-pricing.routing';
import { AdminPricingListComponent } from './admin-pricing-list/admin-pricing-list.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AdminPricingNewComponent } from './admin-pricing-new/admin-pricing-new.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdminPricingService } from './admin-pricing.service';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { AdminToolsModule } from '../admin-tools/admin-tools.module';
import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { INTERCEPTOR_ERRORS } from 'src/app/tools/common/gmat-functions';
import { AlertModule } from 'ngx-bootstrap';
import { ToolsModule } from 'src/app/tools/tools.module';


const PROVIDERS_AUTH = [
    AdminPricingService
]
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};
@NgModule({
    imports: [
        CommonModule,
        AdminPricingRoutes,
        ModalModule.forRoot(),
        ReactiveFormsModule,
        ButtonsModule.forRoot(),
        HttpClientModule,
        AdminToolsModule,
        PerfectScrollbarModule,
        ControlMessagesModule,
        AlertModule.forRoot(),
        ToolsModule
    ],
    declarations: [
        AdminPricingComponent,
        AdminPricingListComponent,
        AdminPricingNewComponent
    ],
    entryComponents: [
        AdminPricingNewComponent
    ],
    providers: [
        PROVIDERS_AUTH,
        INTERCEPTOR_ERRORS,
        { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorAdmin, multi: true }
        , {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ]
})
export class AdminPricingModule { }
