import { Component, OnInit, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminPricingService } from '../admin-pricing.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';

@Component({
    selector: 'gmat-admin-pricing-new',
    templateUrl: './admin-pricing-new.component.html',
    styleUrls: ['./admin-pricing-new.component.less'],
})
export class AdminPricingNewComponent implements OnInit {
    form: FormGroup;
    title: string;
    alert: any;
    public eventClosed: EventEmitter<any> = new EventEmitter();
    constructor(
        public bsModalRef: BsModalRef,
        private fb: FormBuilder,
        private adminPricingService: AdminPricingService,
        private sweetalertService: SweetalertService,
        private reactiveFormsService: ReactiveFormsService
    ) {
        this.creacionform();
    }

    ngOnInit() {}

    save() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            const params: any = this.form.value;
            params.amount = Number(params.amount);
            params.interval_count = Number(params.interval_count);
            this.adminPricingService.post(params).subscribe(
                res => {
                    this.form.reset();
                    this.sweetalertService
                        .swal({
                            title: environment.messages.save.title,
                            text: environment.messages.save.text,
                            icon: environment.messages.iconsuccess,
                        })
                        .then(
                            () => (
                                this.eventClosed.emit(true),
                                this.bsModalRef.hide()
                            )
                        );
                },
                error => {
                    this.alert = {
                        type: 'danger',
                        title: 'Error, ',
                        msg: error,
                    };
                }
            );
        }
    }

    private creacionform() {
        this.form = this.fb.group({
            name: [null, [Validators.required]],
            description: [null, [Validators.required]],
            amount: [null, [Validators.required]],
            currency_code: [null, [Validators.required]],
            interval: [null, [Validators.required]],
            interval_count: [null, [Validators.required]],
            trial_days: [null, [Validators.required]],
            type: [null, [Validators.required]],
        });
    }
}
