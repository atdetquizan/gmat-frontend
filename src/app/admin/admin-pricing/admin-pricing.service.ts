import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminPricingService {

    constructor(
        private http: HttpClient
    ) { }

    post(parameters: any) {
        return this.http.post(`${environment.apiurl}/subscriptions`, parameters);
    }

    get() {
        return this.http.get(`${environment.apiurl}/subscriptions`);
    }

    getById(id: any) {
        return this.http.get(`${environment.apiurl}/subscriptions/${id}`)
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiurl}/subscriptions/${id}`);
    }

    getByUserId(clientId: string) {
        return this.http.post(`${environment.apiurl}/subscriptions/plans`, { clientId });
    }
}
