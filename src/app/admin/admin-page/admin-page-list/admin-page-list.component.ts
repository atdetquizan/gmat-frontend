import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ThrowStmt } from '@angular/compiler';
import { BsModalService } from 'ngx-bootstrap';
import { AdminPageNewComponent } from '../admin-page-new/admin-page-new.component';
import { AdminPageService } from '../admin-page.service';

@Component({
    selector: 'gmat-admin-page-list',
    templateUrl: './admin-page-list.component.html',
    styleUrls: ['./admin-page-list.component.less']
})
export class AdminPageListComponent implements OnInit {

    form: FormGroup;
    pages: any;
    constructor(
        private modalService: BsModalService,
        private fb: FormBuilder,
        private adminPageService: AdminPageService
    ) {
        this.createFrm();
    }

    ngOnInit() {
        this.showData();
    }

    clickOpenNew(item?: any) {
        const initialState = {
            title: item ? 'Edit section page' : 'Create section page',
            page: item
            // parameters
        };
        const modal = this.modalService.show(AdminPageNewComponent, {
            class: 'modal-create-suscription modal-dialog-centered',
            initialState
        });
        modal.content.eventClosed.subscribe((res) => {
            if (res) {
                this.showData();
            }
        });
    }

    search(value: any) {
        const content: any = document.querySelectorAll('.ContentSearch');
        for (const item of content) {
            const index = item.innerText.toUpperCase().indexOf(value.toUpperCase());
            if (index < 0) {
                item.style.display = 'none';
            } else {
                item.style.display = 'table-row';
            }
        }
    }   

    private showData() {
        this.adminPageService.get().subscribe((res) => this.pages = res);
    }

    private createFrm() {
        this.form = this.fb.group({

        });
    }

}
