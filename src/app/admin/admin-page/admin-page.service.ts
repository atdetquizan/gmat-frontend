import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable({
    providedIn: 'root'
})
export class AdminPageService {

    constructor(
        private http: HttpClient
    ) { }


    get() {
        return this.http.get(`${environment.apiurl}/pages`);
    }
    
    post(paramters: any) {
        return this.http.post(`${environment.apiurl}/pages`, paramters);
    }

    filePost(pageId: number, file: any) {
        const formData: FormData = new FormData();
        formData.append('file', file);
        return this.http.post(`${environment.apiurl}/images/upload/pages/${pageId}`, formData);
    }
}
