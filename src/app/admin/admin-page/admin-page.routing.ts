import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminPageComponent } from './admin-page.component';
import { AdminPageListComponent } from './admin-page-list/admin-page-list.component';

const routes: Routes = [
    {
        path: '',
        component: AdminPageComponent,
        children: [
            {
                path: 'list',
                component: AdminPageListComponent
            },
            {
                path: '',
                redirectTo: 'list'
            }
        ]
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminPageRoutes { }

