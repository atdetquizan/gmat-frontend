import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { AdminPageService } from '../admin-page.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment.prod';

@Component({
    selector: 'gmat-admin-page-new',
    templateUrl: './admin-page-new.component.html',
    styleUrls: ['./admin-page-new.component.less']
})
export class AdminPageNewComponent implements OnInit {
    title: string;
    form: FormGroup;
    fileselect: any
    namefile: string;
    page: any;
    public eventClosed: EventEmitter<any> = new EventEmitter();
    constructor(
        public bsModalRef: BsModalRef,
        private fb: FormBuilder,
        private reactiveFormsService: ReactiveFormsService,
        private adminPageService: AdminPageService,
        private sweetalertService: SweetalertService
    ) {
        this.createFrm();
    }

    ngOnInit() {
        if (this.page) {
            const file = this.page.image ? this.page.image.split('/') : [];
            this.namefile = file.pop();
            this.form.patchValue(this.page);
            this.form.controls.section.disable();
        }
    }

    changeFile(event: any) {
        this.namefile = event.target.files[0].name;
        this.fileselect = event.target.files[0];
    }

    save() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            if (this.page) {
                this.adminPageService.filePost(this.page.id, this.fileselect)
                    .subscribe(() => {
                        this.success();
                    });
            } else {
                this.form.controls.type.setValue('image');
                this.adminPageService.post(this.form.value).subscribe((res: any) => {
                    this.adminPageService.filePost(res.id, this.fileselect)
                        .subscribe(() => {
                            this.success();
                        });
                });
            }
        }
    }

    success() {
        this.form.reset();
        this.sweetalertService.swal({
            title: environment.messages.save.title,
            text: environment.messages.save.text,
            icon: environment.messages.iconsuccess
        }).then(() => {
            this.eventClosed.emit(true);
            this.bsModalRef.hide();
        });
    }

    private createFrm() {
        this.form = this.fb.group({
            section: [null, [Validators.required]],
            type: [null, null],
            filename: [null, [Validators.required]]
        });
    }
}
