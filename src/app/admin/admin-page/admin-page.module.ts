import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPageComponent } from './admin-page.component';
import { AdminPageRoutes } from './admin-page.routing';
import { AdminPageListComponent } from './admin-page-list/admin-page-list.component';
import { ModalModule, ButtonsModule } from 'ngx-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdminToolsModule } from '../admin-tools/admin-tools.module';
import { PerfectScrollbarModule, PerfectScrollbarConfigInterface, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { AdminPageNewComponent } from './admin-page-new/admin-page-new.component';
import { AdminPageService } from './admin-page.service';

const PROVIDERS_PAGE = [
    AdminPageService
];

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

@NgModule({
    imports: [
        CommonModule,
        AdminPageRoutes,
        ModalModule.forRoot(),
        ReactiveFormsModule,
        ButtonsModule.forRoot(),
        HttpClientModule,
        AdminToolsModule,
        PerfectScrollbarModule,
        ControlMessagesModule
    ],
    declarations: [
        AdminPageComponent,
        AdminPageListComponent,
        AdminPageNewComponent
    ],
    entryComponents: [
        AdminPageNewComponent
    ],
    providers: [
        PROVIDERS_PAGE,
        { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorAdmin, multi: true }
        , {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ]
})
export class AdminPageModule { }
