import { PerfectScrollbarConfigInterface, PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { AdminTopicsService } from './../admin-topics/admin-topics.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminSubjectsComponent } from './admin-subjects.component';
import { AdminSubjectsRoutes } from './admin-subjects.routing';
import { AdminSubjectsListComponent } from './admin-subjects-list/admin-subjects-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { AdminToolsModule } from '../admin-tools/admin-tools.module';
import { AdminSubjectsNewComponent } from './admin-subjects-new/admin-subjects-new.component';
import { AdminSubjectsService } from './admin-subjects.service';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PagePaginationModule } from '../admin-tools/page-pagination/page-pagination.module';

const PROVIDERS_AUTH = [
    AdminSubjectsService,
    AdminTopicsService
]
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};
@NgModule({
    imports: [
        CommonModule,
        AdminSubjectsRoutes,
        ControlMessagesModule,
        ReactiveFormsModule,
        AdminToolsModule,
        ModalModule.forRoot(),
        HttpClientModule,
        PerfectScrollbarModule,
        PagePaginationModule
    ],
    declarations: [
        AdminSubjectsComponent,
        AdminSubjectsListComponent,
        AdminSubjectsNewComponent
    ],
    entryComponents: [
        AdminSubjectsNewComponent
    ],
    providers: [
        PROVIDERS_AUTH,
        { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorAdmin, multi: true },
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }]
})
export class AdminSubjectsModule { }
