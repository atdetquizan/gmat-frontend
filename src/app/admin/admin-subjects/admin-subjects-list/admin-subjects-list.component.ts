import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AdminSubjectsService } from '../admin-subjects.service';
import { AdminSubjectsNewComponent } from '../admin-subjects-new/admin-subjects-new.component';
import { environment } from 'src/environments/environment';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { AdminTopicsService } from '../../admin-topics/admin-topics.service';
import { PaginationType } from '../../admin-tools/page-pagination/page-pagination-tools/page-pagination-type.enum';

@Component({
    selector: 'gmat-admin-subjects-list',
    templateUrl: './admin-subjects-list.component.html',
    styleUrls: ['./admin-subjects-list.component.less']
})
export class AdminSubjectsListComponent implements OnInit {
    form: FormGroup;
    bsModalRef: BsModalRef;
    subjects: any = [];
    topics: any = [];
    topicId: any;

    constructor(
        private modalService: BsModalService,
        public fb: FormBuilder,
        private adminSubjectsService: AdminSubjectsService,
        private sweetalertService: SweetalertService,
        private adminTopicsService: AdminTopicsService
    ) {
        this.createForm();
    }

    ngOnInit() {
        this.showData();
        this.showDataSelect();
    }

    clickOpenNew(subject?: any) {
        const initialState = {
            title: subject ? 'Edit Subject' : 'Create Subject',
            subject: subject
            // parameters
        };
        const modal = this.modalService.show(AdminSubjectsNewComponent, {
            class: 'modal-create-section modal-dialog-centered',
            initialState
        });
        modal.content.eventClosed.subscribe((res: boolean) => {
            if (res) {
                this.showData();
            }
        });
    }

    onClickDelete(item: any) {
        this.sweetalertService
            .swal({
                title: environment.messages.delete.title,
                text: environment.messages.delete.text,
                icon: 'warning',
                buttons: true,
                dangerMode: true
            })
            .then(willDelete => {
                if (willDelete) {
                    this.adminSubjectsService.delete(item.id).subscribe((res: any) => {
                        this.sweetalertService.swal(environment.messages.delete.text2, {
                            icon: 'success'
                        });
                        this.showData();
                    });
                }
            });
    }

    changeTopic(value) {
        // if (value) {
        //     this.adminSubjectsService.getByTopicId(value).subscribe((res) => {
        //         this.subjects = res;
        //     })
        // } else {
        this.topicId = value;
        this.showData();
        // }
    }

    setColumns() {
        return [
            {
                title: 'Subject',
                field: 'name'
            },
            {
                title: 'Image',
                field: 'image',
                type: PaginationType.image
            },
            {
                title: 'Topic',
                field: 'topicName'
            },
            {
                title: 'Actions',
                class: 'text-center',
                type: PaginationType.buttons,
                buttons: {
                    delete: {
                        icon: 'far fa-trash-alt'
                    },
                    edit: {
                        icon: 'fas fa-edit'
                    }
                }
            }
        ];
    }

    showData(page?: any) {
        let parameters: any = {};
        parameters.perPage = environment.pagination.adminpanel;
        parameters.page = page ? page : 1;
        if (this.topicId) {
            parameters.topicId = this.topicId;
        }
        if (this.form.controls.filter.value) {
            parameters.word = this.form.controls.filter.value;
        }
        this.adminSubjectsService.get(parameters).subscribe((res: any) => {
            this.subjects = res;
        });
    }

    private createForm() {
        this.form = this.fb.group({
            filter: [null, null]
        });
    }

    private showDataSelect() {
        this.adminTopicsService.get().subscribe((res: any) => {
            this.topics = res;
        });
    }
}
