import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminSubjectsComponent } from './admin-subjects.component';
import { AdminSubjectsListComponent } from './admin-subjects-list/admin-subjects-list.component';

const routes: Routes = [
    {
        path: '',
        component: AdminSubjectsComponent,
        children: [
            {
                path: 'list',
                component: AdminSubjectsListComponent
            },
            {
                path: '',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminSubjectsRoutes { }

