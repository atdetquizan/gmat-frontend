import { AdminTopicsService } from './../../admin-topics/admin-topics.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { AdminSubjectsService } from '../admin-subjects.service';
import { environment } from 'src/environments/environment';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';

@Component({
    selector: 'gmat-admin-subjects-new',
    templateUrl: './admin-subjects-new.component.html',
    styleUrls: ['./admin-subjects-new.component.less']
})
export class AdminSubjectsNewComponent implements OnInit {
    form: FormGroup;
    title: string;
    topics: any = [];
    subject: any;
    namefile: string;
    fileselection: any;
    public eventClosed: EventEmitter<any> = new EventEmitter();

    constructor(
        public bsModalRef: BsModalRef,
        private fb: FormBuilder,
        private adminSubjectsService: AdminSubjectsService,
        private sweetalertService: SweetalertService,
        private adminTopicsService: AdminTopicsService,
        private reactiveFormsService: ReactiveFormsService
    ) {
        this.creacionform();
    }

    ngOnInit() {
        this.showData();
        if (this.subject) {
            const file = this.subject.image ? this.subject.image.split('/') : [];
            this.namefile = file.pop();
            // this.form.controls.filename.setValue(this.namefile);
            this.form.patchValue(this.subject);
            this.form.controls.filename.clearValidators();
            this.form.controls.filename.updateValueAndValidity();
        }
    }

    save() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (!this.form.valid)
            return;

        if (this.form.value.id) {
            this.adminSubjectsService.patch(this.form.value).subscribe((res: any) => {
                if (this.fileselection) {
                    this.adminSubjectsService.filePost(res.id, this.fileselection)
                        .subscribe(() => {
                            this.success();
                        });
                } else {
                    this.success();
                }
            });
        } else {
            this.adminSubjectsService.post(this.form.value).subscribe((res: any) => {
                if (this.fileselection) {
                    this.adminSubjectsService.filePost(res.id, this.fileselection)
                        .subscribe(() => {
                            this.success();
                        });
                }
            });
        }
    }

    success() {
        this.form.reset();
        this.sweetalertService.swal({
            title: environment.messages.save.title,
            text: environment.messages.save.text,
            icon: environment.messages.iconsuccess
        }).then(() => {
            this.eventClosed.emit(true);
            this.bsModalRef.hide();
        });
    }

    changeFile(e) {
        this.fileselection = e.target.files[0];
        this.namefile = this.fileselection.name;
    }

    private creacionform() {
        this.form = this.fb.group({
            id: [null, null],
            name: [null, [Validators.required]],
            topicId: ['', [Validators.required]],
            filename: [null, [Validators.required]]
        });
    }

    private showData() {
        this.adminTopicsService.get().subscribe((res: any) => {
            this.topics = res;
        });
    }

}
