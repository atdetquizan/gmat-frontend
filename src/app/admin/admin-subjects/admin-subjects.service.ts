import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminSubjectsService {

    constructor(
        private http: HttpClient
    ) { }

    post(parameters: any) {
        return this.http.post(`${environment.apiurl}/subjects`, parameters);
    }

    get(parameters?: any) {
        return this.http.get(`${environment.apiurl}/subjects`, { params: parameters });
    }

    getById(sectionId: number) {
        return this.http.get(`${environment.apiurl}/subjects/${sectionId}`);
    }

    patch(parameters: any) {
        return this.http.patch(`${environment.apiurl}/subjects/${parameters.id}`, parameters);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiurl}/subjects/${id}`);
    }

    getByTopicId(topicId: number) {
        return this.http.get(`${environment.apiurl}/topics/${topicId}/subjects`);
    }

    filePost(id: number, file: any) {
        const formData: FormData = new FormData();
        formData.append('file', file);
        return this.http.post(`${environment.apiurl}/images/upload/subjects/${id}`, formData);
    }
}
