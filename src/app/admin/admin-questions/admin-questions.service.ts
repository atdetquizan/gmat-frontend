import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminQuestionsService {

    constructor(
        private http: HttpClient
    ) { }

    post(parameters: any) {
        return this.http.post(`${environment.apiurl}/questions/generate`, parameters);
    }

    patch(questionId: any, parameters: any) {
        return this.http.patch(`${environment.apiurl}/questions/${questionId}`, parameters);
    }
    
    get() {
        return this.http.get(`${environment.apiurl}/questions/nodes/last`);
    }

    getList(parameters?: any) {
        return this.http.get(`${environment.apiurl}/questions`, { params: parameters });
    }

    getById(questionId: number) {
        return this.http.get(`${environment.apiurl}/questions/${questionId}?filter[include]=alternatives`)
    }

    getLasNotes() {
        return this.http.get(`${environment.apiurl}/client-questions/last-nodes/count`);
    }

    postQuestionsAnswers(parameters: any) {
        return this.http.post(`${environment.apiurl}/questions/answers`, parameters);
    }

    postClientQuestionsReports(parameters: any) {
        return this.http.post(`${environment.apiurl}/client-tests/reports`, parameters);
    }

    postClientReportsDetail(parameters: any) {
        return this.http.post(`${environment.apiurl}/client-tests/reports/detail`, parameters);
    }

    postClientQuestionDetail(id: number) {
        return this.http.get(`${environment.apiurl}/client-question-detail/${id}`);
    }

    postValidateTest(parameters: any) {
        return this.http.post(`${environment.apiurl}/questions/validate-test`, parameters);
    }

}
