import { AdminQuestionsService } from './../admin-questions.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { TypeLessons } from '../../../portal/portal-platform/portal-platform-lessons/portal-platform-lessons-shared/TypeLessons.enum';
import { ReactiveFormsService } from '../../../tools/services/reactive-forms.service';
import {
    toDataURL,
    isEmptyObject,
    loading,
    emptyToNull
} from '../../../tools/common/gmat-functions';
import { QuestionFormatType } from '../admin-questions-shared/question-format-type.enum';
import { environment } from '../../../../environments/environment';
import { SweetalertService } from '../../../tools/services/sweetalert.service';
import { AdminQuestionAlternativeService } from '../admin-questions-shared/admin-question-alternative.service';
import { Alert, AlertType } from 'src/app/tools/alert-message/alert-message.component';
import { ActivatedRoute, Router } from '@angular/router';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'gmat-admin-questions-new',
    templateUrl: './admin-questions-new.component.html',
    styleUrls: ['./admin-questions-new.component.less']
})
export class AdminQuestionsNewComponent implements OnInit {
    form: FormGroup;
    questionId: number;
    question: any;
    sections = [];
    topics = [];
    subjects = [];
    subsubjects = [];
    questionFormatType = QuestionFormatType;
    settingAlert: Alert;
    show = false;
    constructor(
        public fb: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private adminQuestionsService: AdminQuestionsService,
        private reactiveFormsService: ReactiveFormsService,
        private sweetalertService: SweetalertService,
        private adminQuestionAlternativeService: AdminQuestionAlternativeService
    ) {
        this.createForm();
    }

    ngOnInit() {
        this.questionId = this.activatedRoute.snapshot.params.id;
        this.showDataSection().subscribe(() => {
            this.form.controls.type.valueChanges.subscribe((value: any) => {
                this.onChangeFormat(value);
            });
            if ((this.questionId = this.activatedRoute.snapshot.params.id)) {
                this.show = true;
                this.showQuestion();
            }
        });
    }

    onClickSave() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            this.saveQuestion().subscribe(
                res => {
                    this.form.reset();
                    this.form.controls.alternatives = this.fb.array([]);
                    this.form.controls.headersTable = this.fb.array([]);
                    this.sweetalertService.swal({
                        title: environment.messages.save.title,
                        text: environment.messages.save.text,
                        icon: environment.messages.iconsuccess
                    });
                    if (this.questionId) {
                        this.showQuestion();
                    }
                    this.router.navigate(['/admin/questions']);
                },
                (res: any) => {
                    this.settingAlert = {
                        title: 'Error',
                        msg: res,
                        type: AlertType.danger
                    };
                    window.scroll(0, 0);
                }
            );
        }
    }

    onChangeSections(sectionId: number) {
        // reset lessons controls
        this.onResetLesson(TypeLessons.sections);
        // get section object
        const section = this.sections.find(x => x.id === Number(sectionId));
        if (section) {
            // set object topics
            this.topics = section && section.topics ? section.topics : [];
            // enable controls topics
            if (section.topics && section.topics.length > 0) this.form.controls.topicId.enable();
        }
    }

    onChangeTopics(topicId: number) {
        // reset lessons controls
        this.onResetLesson(TypeLessons.topics);
        // get section object
        const topic = this.topics.find(x => x.id === Number(topicId));
        if (topic) {
            // set object topics
            this.subjects = topic && topic.subjects ? topic.subjects : [];
            // enable controls topics
            if (topic.subjects && topic.subjects.length > 0) this.form.controls.subjectId.enable();
        }
    }

    onChangeSubjects(subjectId: number) {
        // reset lessons controls
        this.onResetLesson(TypeLessons.subjects);
        // get section object
        const subject = this.subjects.find(x => x.id === Number(subjectId));
        if (subject) {
            // set object topics
            this.subsubjects = subject.subSubjects ? subject.subSubjects : [];
            // enable controls topics
            if (subject.subSubjects && subject.subSubjects.length > 0)
                this.form.controls.subSubjectId.enable();
        }
    }

    onChangeFile(e: any) {
        const file = e.target.files[0];
        this.form.controls.imageExplanationName.setValue(file.name);
        toDataURL(file).then((dataUrl: any) => {
            const base64 = dataUrl.target.result;
            this.form.controls.imageExplanation.setValue(base64.substr(base64.indexOf(',') + 1));
        });
    }

    onChangeQuestionFile(e: any) {
        const file = e.target.files[0];
        this.form.controls.questionImageName.setValue(file.name);
        toDataURL(file).then((dataUrl: any) => {
            const base64 = dataUrl.target.result;
            this.form.controls.questionImage.setValue(base64.substr(base64.indexOf(',') + 1));
        });
    }

    onClickReset() {
        this.form.reset();
    }

    onChangeFormat(value: any) {
        this.form.controls.hasHeadersTable.setValue(false);
        this.form.controls.hasTabs.setValue(false);
        this.form.controls.alternatives = this.fb.array([]);
        this.form.controls.headersTable = this.fb.array([]);
        this.form.controls.tabs = this.fb.array([]);
        // ELEMINAR VALIDACIONES
        switch (value) {
            case QuestionFormatType.yesnomultiplechoice:
                this.form.controls.filesDescription.setValidators([Validators.required]);
                break;

            default:
                this.form.controls.filesDescription.clearAsyncValidators();
                break;
        }
        this.form.controls.filesDescription.updateValueAndValidity();
    }

    onClear() {
        this.form.reset();
        this.form.controls.type.setValue(null);
        this.form.controls.alternatives = this.fb.array([]);
        this.form.controls.headersTable = this.fb.array([]);
        this.form.controls.tabs = this.fb.array([]);
    }

    private saveQuestion() {
        // const values: any = isEmptyObject(this.form.getRawValue());
        // values.status = String(values.status ? 'active' : 'inactive');
        const values = isEmptyObject(this.form.getRawValue());
        const values2 = isEmptyObject(this.form.getRawValue());
        values.sectionId =
            this.topics.length === 0
                ? emptyToNull(values.sectionId)
                : null;
        values.topicId =
            this.subjects.length === 0
                ? emptyToNull(values.topicId)
                : null;
        values.subjectId =
            this.subsubjects.length === 0
                ? emptyToNull(values.subjectId)
                : null;
        values.subSubjectId = emptyToNull(values.subSubjectId);
        values.status = String(values.status ? 'active' : 'inactive');
        values2.status = String(values.status ? 'active' : 'inactive');
        // if (this.question) {
        //     delete values['code'];
        // }

        const showUrl = this.questionId ? this.adminQuestionsService.patch(this.questionId, values2) : this.adminQuestionsService.post(values);
        return showUrl;
    }

    private showQuestion() {
        this.adminQuestionsService
            .getById(this.activatedRoute.snapshot.params.id)
            .subscribe(res => {
                this.question = res;
                this.setValuesCustom();
            });
    }

    // SET RESET SELECT LESSONS
    private onResetLesson(lesson: TypeLessons) {
        switch (lesson) {
            case TypeLessons.sections:
                this.topics = [];
                this.subjects = [];
                this.subsubjects = [];
                // set value empty
                this.form.controls.topicId.setValue('');
                this.form.controls.subjectId.setValue('');
                // disabled controlsf
                this.form.controls.topicId.disable();
                this.form.controls.subjectId.disable();
                break;

            case TypeLessons.topics:
                this.subjects = [];
                this.subsubjects = [];
                // set value empty
                this.form.controls.subjectId.setValue('');
                // disabled controls
                this.form.controls.subjectId.disable();
                break;
        }
        // default ya que siempre se va repetir en todas las acciones
        this.subsubjects = [];
        // set value empty
        this.form.controls.subSubjectId.setValue('');
        // disabled controls
        this.form.controls.subSubjectId.disable();
    }

    private showDataSection() {
        return this.adminQuestionsService.get().pipe(
            tap((res: any) => {
                this.sections = res;
            })
        );
    }

    private createForm() {
        this.form = this.fb.group({
            nodeName: [{ value: '', disabled: true }, null],
            sectionId: ['', (this.questionId = this.activatedRoute.snapshot.params.id) ? null : [Validators.required]],
            topicId: [{ value: '', disabled: true }, [Validators.required]],
            subjectId: [{ value: '', disabled: true }, [Validators.required]],
            subSubjectId: [{ value: '', disabled: true }, [Validators.required]],
            appliesTo: [null, [Validators.required]],
            code: [null, [Validators.required]],
            question: [null, [Validators.required]],
            questionImageName: [null, null],
            questionImage: [null, null],
            type: [null, [Validators.required]],
            difficulty: ['', [Validators.required]],
            status: [null, null],
            videoUrl: [null, [Validators.required]],
            videoTitle: [null, [Validators.required]],
            explanation: [null, [Validators.required]],
            imageExplanationName: [null, null],
            imageExplanation: [null, null],
            alternatives: this.fb.array([]),
            hasHeadersTable: [false, null],
            headersTable: this.fb.array([]),
            hasTabs: [false, null],
            tabs: this.fb.array([]),
            filesDescription: [null, null]
        });
    }

    private setValuesCustom() {
        console.log(this.question);
        this.form.patchValue({
            nodeName: emptyToNull(this.question.nodeName),
            sectionId: emptyToNull(this.question.sectionId),
            topicId: emptyToNull(this.question.topicId),
            subjectId: emptyToNull(this.question.subjectId),
            subSubjectId: emptyToNull(this.question.subSubjectId),
            appliesTo: emptyToNull(this.question.appliesTo),
            code: emptyToNull(this.question.code),
            question: emptyToNull(this.question.question),
            type: emptyToNull(this.question.type),
            difficulty: emptyToNull(this.question.difficulty),
            status: emptyToNull(this.question.status === 'active' ? true : false),
            videoUrl: emptyToNull(this.question.videoUrl),
            videoTitle: emptyToNull(this.question.videoTitle),
            explanation: emptyToNull(this.question.explanation),
            questionImage: emptyToNull(this.question.questionImage),
            questionImageName: emptyToNull(this.question.questionImageName),
            imageExplanationName: emptyToNull(this.question.imageExplanationName),
            imageExplanation: emptyToNull(this.question.imageExplanation),
            hasHeadersTable: emptyToNull(this.question.hasHeadersTable),
            // headersTable: (this.question.headerTags ? this.question.headerTags : []),
            hasTabs: emptyToNull(this.question.hasTabs),
            // tabs: (this.question.tabs ? this.question.tabs : []),
            filesDescription: emptyToNull(this.question.filesDescription)
        });
        this.onChangeSections(this.question.sectionId);
        this.form.controls.topicId.setValue(this.question.topicId);
        this.onChangeTopics(this.question.topicId);
        this.form.controls.subjectId.setValue(this.question.subjectId);
        this.onChangeSubjects(this.question.subjectId);
        this.form.controls.subSubjectId.setValue(this.question.subSubjectId);
        console.log(this.form.value);
    }
}
