import { AdminQuestionsService } from './../admin-questions.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RouterLinkActive, ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { PaginationType } from '../../admin-tools/page-pagination/page-pagination-tools/page-pagination-type.enum';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';

@Component({
    selector: 'gmat-admin-questions-list',
    templateUrl: './admin-questions-list.component.html',
    styleUrls: ['./admin-questions-list.component.less']
})
export class AdminQuestionsListComponent implements OnInit {
    form: FormGroup;
    questions: any = [];
    constructor(
        private router: Router,
        private fb: FormBuilder,
        private adminQuestionsService: AdminQuestionsService,
        private sweetalertService: SweetalertService
    ) {
        this.createForm();
    }

    ngOnInit() {
        this.showData();
    }

    setColumns() {
        return [
            {
                title: 'ID',
                field: 'id'
            },
            {
                title: 'Section',
                field: 'sectionName'
            },
            {
                title: 'Topics',
                field: 'topicName'
            },
            {
                title: 'Subject',
                field: 'subjectName'
            },
            {
                title: 'Difficulty',
                field: 'difficulty'
            },
            {
                title: 'For',
                field: 'appliesTo'
            },
            {
                title: 'Title',
                field: 'videoTitle'
            },
            {
                title: 'Video',
                field: 'videoUrl'
            },
            {
                title: 'Content',
                field: 'videoUrl'
            },
            {
                title: 'Actions',
                class: 'text-center',
                type: PaginationType.buttons,
                buttons: {
                    edit: {
                        icon: 'fas fa-edit'
                    }
                }
            }
        ];
    }

    onClickEdit(item: any) {
        this.router.navigate([`./admin/questions/edit/${item.id}`]);
    }

    showData(page?: any) {
        let parameters: any = {};
        parameters.perPage = environment.pagination.adminpanel;
        parameters.page = page ? page : 1;
        if (this.form.controls.filter.value) {
            parameters.word = this.form.controls.filter.value;
        }
        this.adminQuestionsService.getList(parameters).subscribe((res: any) => {
            this.questions = res;
        });
    }

    private createForm() {
        this.form = this.fb.group({
            filter: [null, null]
        });
    }
}
