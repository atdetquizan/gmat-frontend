import { AdminQuestionsService } from './admin-questions.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminQuestionsComponent } from './admin-questions.component';
import { AdminQuestionsRoutes } from './admin-questions.routing';
import { AdminQuestionsListComponent } from './admin-questions-list/admin-questions-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonsModule, TooltipModule } from 'ngx-bootstrap';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { AdminToolsModule } from '../admin-tools/admin-tools.module';
import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { AdminQuestionsNewComponent } from './admin-questions-new/admin-questions-new.component';
import {
    PerfectScrollbarModule,
    PERFECT_SCROLLBAR_CONFIG,
    PerfectScrollbarConfigInterface
} from 'ngx-perfect-scrollbar';
import { AdminQuestionAlternativeService } from './admin-questions-shared/admin-question-alternative.service';
import { PagePaginationModule } from '../admin-tools/page-pagination/page-pagination.module';
import { AdminQuestionsSharedAlternativesComponent } from './admin-questions-shared/admin-questions-shared-alternatives/admin-questions-shared-alternatives.component';
import { AdminQuestionsSharedMultipleChoiceComponent } from './admin-questions-shared/admin-questions-shared-multiple-choice/admin-questions-shared-multiple-choice.component';
import { AdminQuestionsSharedSimpleSelectionComponent } from './admin-questions-shared/admin-questions-shared-simple-selection/admin-questions-shared-simple-selection.component';
import { AdminQuestionsSharedDropdownComponent } from './admin-questions-shared/admin-questions-shared-dropdown/admin-questions-shared-dropdown.component';
import { ToolsModule } from 'src/app/tools/tools.module';
import { INTERCEPTOR_ERRORS } from 'src/app/tools/common/gmat-functions';

const PROVIDERS_AUTH = [AdminQuestionsService, AdminQuestionAlternativeService];
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    // suppressScrollX: true
};

@NgModule({
    imports: [
        CommonModule,
        AdminQuestionsRoutes,
        ReactiveFormsModule,
        ButtonsModule.forRoot(),
        HttpClientModule,
        AdminToolsModule,
        ControlMessagesModule,
        PerfectScrollbarModule,
        TooltipModule.forRoot(),
        PagePaginationModule,
        ToolsModule
    ],
    declarations: [
        AdminQuestionsComponent,
        AdminQuestionsListComponent,
        AdminQuestionsNewComponent,
        AdminQuestionsSharedAlternativesComponent,
        AdminQuestionsSharedMultipleChoiceComponent,
        AdminQuestionsSharedSimpleSelectionComponent,
        AdminQuestionsSharedDropdownComponent
    ],
    providers: [
        PROVIDERS_AUTH,
        INTERCEPTOR_ERRORS,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AppHttpInterceptorAdmin,
            multi: true
        },
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ]
})
export class AdminQuestionsModule {}
