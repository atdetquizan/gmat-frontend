import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminQuestionsComponent } from './admin-questions.component';
import { AdminQuestionsListComponent } from './admin-questions-list/admin-questions-list.component';
import { AdminQuestionsNewComponent } from './admin-questions-new/admin-questions-new.component';

const routes: Routes = [
    {
        path: '',
        component: AdminQuestionsComponent,
        children: [
            {
                path: 'list',
                component: AdminQuestionsListComponent
            },
            {
                path: 'new',
                component: AdminQuestionsNewComponent
            },
            {
                path: 'edit/:id',
                component: AdminQuestionsNewComponent
            },
            {
                path: '',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminQuestionsRoutes { }
