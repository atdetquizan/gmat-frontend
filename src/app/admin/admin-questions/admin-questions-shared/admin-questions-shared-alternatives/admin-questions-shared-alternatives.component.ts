import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { QuestionFormatType } from '../question-format-type.enum';
import {
    FormGroup,
    FormBuilder,
    FormArray,
    Validators,
    Form,
    FormControl
} from '@angular/forms';
import { abecedario, toDataURL } from '../../../../tools/common/gmat-functions';

@Component({
    selector: 'gmat-admin-questions-shared-alternatives',
    templateUrl: './admin-questions-shared-alternatives.component.html',
    styleUrls: ['./admin-questions-shared-alternatives.component.less']
})
export class AdminQuestionsSharedAlternativesComponent implements OnInit {
    // _value;
    questionFormatType = QuestionFormatType;

    @Input() hasSubalternatives = false;
    @Input() question: any;
    @Input() formBand: FormGroup;
    // type: QuestionFormatType;
    controlAlternative = 'alternatives';

    constructor(private _fb: FormBuilder) {
        //
    }

    ngOnInit() {
        switch (this.formBand.controls.type.value) {
            case QuestionFormatType.boolean:
                this.customAlternatives(this.formBand.controls.type.value);
                break;

            default:
                if (this.question) {
                    for (const altv of this.question.alternatives) {
                        this.alternativeAdd(altv);
                    }
                }
                break;
        }
    }

    onClickAdd() {
        this.alternativeAdd();
    }

    onClickSaveAlternative(index: number) { }

    onChangeSubAlternative(subalternatives: FormGroup[], control: FormControl) {
        for (const item of subalternatives) {
            item.controls['isTheAnswer'].setValue(false);
        }
        control.setValue(!control.value);
    }

    onClickDeleteAlternative(index: number) {
        const formarray = this.formBand.get(
            this.controlAlternative
        ) as FormArray;
        formarray.removeAt(index);
    }

    addFileAlternative(event: any, formGroup: FormGroup) {
        const file = event.target.files[0];
        toDataURL(file).then((dataUrl: any) => {
            const base64 = dataUrl.target.result;
            formGroup.get('includeImage').setValue(true);
            formGroup
                .get('file')
                .setValue(base64.substr(base64.indexOf(',') + 1));
            formGroup.get('fileName').setValue(file.name);
        });
    }

    onClickAddSubAlternative(subalternatives: FormArray) {
        subalternatives.push(this.subAlternativeItem());
    }

    onClickDeleteSubAlternative(subalternatives: FormArray, index: number) {
        subalternatives.removeAt(index);
    }

    onChangeTheAnswer(index: number, event: any) {
        const alternatives = this.formBand.controls[this.controlAlternative] as FormArray;
        alternatives.controls.forEach((element: FormGroup) => {
            element.controls.isTheAnswer.setValue(false);
        });
        const formGroup = alternatives.controls[index] as FormGroup;
        formGroup.controls.isTheAnswer.setValue(event.target.checked);
    }

    checkedIsTheAnswer(index: any) {
        if (this.question) {
            if (this.question.alternatives[index].isTheAnswer) {
                return 'checked'
            }
        }
        return '';
    }

    hasDisabledAddAlternatives() {
        switch (this.formBand.controls.type.value) {
            case this.questionFormatType.selection:
            case this.questionFormatType.yesnomultiplechoice:
            case this.questionFormatType.dropdowncomplement:
            case this.questionFormatType.tablesimpleselection:
                return true;

            default:
                return false;
        }
    }

    hasCheckBox() {
        switch (this.formBand.controls.type.value) {
            case this.questionFormatType.selection:
            // case this.questionFormatType.yesnomultiplechoice:
            case this.questionFormatType.dropdowncomplement:
            case this.questionFormatType.tablesimpleselection:
                return true;

            default:
                return false;
        }
    }

    hasRadio() {
        switch (this.formBand.controls.type.value) {
            case this.questionFormatType.boolean:
                return true;

            default:
                return false;
        }
    }

    hasFile() {
        switch (this.formBand.controls.type.value) {
            case this.questionFormatType.selection:
            case this.questionFormatType.tablesimpleselection:
                return true;

            default:
                return false;
        }
    }

    hasRemove() {
        switch (this.formBand.controls.type.value) {
            case this.questionFormatType.selection:
            case this.questionFormatType.tablesimpleselection:
            case this.questionFormatType.yesnomultiplechoice:
            case this.questionFormatType.dropdowncomplement:
                return true;

            default:
                return false;
        }
    }

    hasDisabledName() {
        switch (this.formBand.controls.type.value) {
            case this.questionFormatType.boolean:
                return true;

            default:
                return false;
        }
    }

    getLetters(group: FormGroup, index: number) {
        group.controls.label.setValue(abecedario(index));
        return abecedario(index);
    }

    getControls() {
        const formArray = this.formBand.controls[
            this.controlAlternative
        ] as FormArray;
        return formArray.controls;
    }

    getSubControls(formgroup: FormGroup) {
        const formArray = formgroup.controls.subAlternatives as FormArray;
        return formArray.controls;
    }

    private customAlternatives(TYPE: QuestionFormatType) {
        this.formBand.controls[this.controlAlternative] = this._fb.array([]);
        switch (TYPE) {
            case QuestionFormatType.boolean:
                this.alternativeAdd({ name: 'true' });
                this.alternativeAdd({ name: 'false' });
                break;

            default:
                break;
        }
    }

    private getDefaultAlternative(status: boolean) {
        switch (this.formBand.controls.type.value) {
            case this.questionFormatType.yesnomultiplechoice:
                return status ? 'Yes' : 'No';
            case this.questionFormatType.tablesimpleselection: {
                return status
                    ? 'Liters of fuel in 1 h'
                    : 'Liters of fuel in 60 km';
            }
        }
    }

    private alternativeAdd(values?: any) {
        const alternativesFormArray: any = this.formBand.controls[
            this.controlAlternative
        ] as FormArray;
        const group: FormGroup = this.alternativeItem();

        // SUB ALTERNATIVES
        switch (this.formBand.controls.type.value) {
            case QuestionFormatType.yesnomultiplechoice:
            case QuestionFormatType.dropdowncomplement:
            case QuestionFormatType.tablesimpleselection:
                const subAlternatives = group.controls
                    .subAlternatives as FormArray;
                subAlternatives.push(
                    this.subAlternativeItem(this.getDefaultAlternative(true))
                );
                subAlternatives.push(
                    this.subAlternativeItem(this.getDefaultAlternative(false))
                );
                break;

            default:
                break;
        }

        if (values) group.patchValue(values);
        alternativesFormArray.push(group) as FormGroup;
    }

    private alternativeItem() {
        return this._fb.group({
            // id: [null, null],
            label: [null, null],
            name: [
                { value: null, disabled: this.hasDisabledName() },
                [Validators.required]
            ],
            isTheAnswer: [false, null],
            includeImage: [false, null],
            file: [null, null],
            fileName: [null, null],
            itHasAlternatives: [this.hasSubalternatives, null],
            subAlternatives: this._fb.array([])
        });
    }

    private subAlternativeItem(value?: string) {
        return this._fb.group({
            name: [{ value: value, disabled: null }, null],
            isTheAnswer: [false, null]
        });
    }
}

export interface AlternativeItem {
    id?: string;
    label?: string;
    name?: string;
    isTheAnswer?: string;
    includeImage?: string;
    file?: string;
    fileName?: string;
}
