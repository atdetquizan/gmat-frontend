import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators, FormControl } from '@angular/forms';
import { toDataURL } from '../../../../tools/common/gmat-functions';
import { QuestionActionType } from '../../../../exam/exam-tools/question-action-type.enum';
import { QuestionFormatType } from '../question-format-type.enum';

@Component({
    selector: 'gmat-admin-questions-shared-multiple-choice',
    templateUrl: './admin-questions-shared-multiple-choice.component.html',
    styles: []
})
export class AdminQuestionsSharedMultipleChoiceComponent implements OnInit {
    form: FormGroup;
    type = QuestionFormatType;
    @Input() question: any;
    @Input() formBand: FormGroup;
    constructor(private _fb: FormBuilder) {
        this.createfrom();
    }

    ngOnInit() {
        this.formBand.controls.hasHeadersTable.setValue(true);
        this.formBand.controls['filesDescription'].valueChanges.subscribe(value => {
            if (!value) {
                this.form.controls.descriptions.setValue(null);
            }
        });
        if (this.question) {
            this.form.controls.descriptions.setValue(this.question.filesDescription);

            this.formBand.controls.hasHeadersTable.setValue(
                this.question.hasHeadersTable ? true : false
            );
            this.formBand.controls.hasTabs.setValue(this.question.hasTabs ? true : false);
            if (this.question.headerTags) {
                for (const header of this.question.headerTags) {
                    this.headersTablesAdd(header);
                }
            }
            if (this.question.tabs) {
                for (const tab of this.question.tabs) {
                    this.tabsAdd(tab);
                }
            }
        }
    }

    onChangeFileHeader(file: any, controls: FormControl, input: any) {
        console.log(file.name);
        input.value = file.name;
        toDataURL(file).then((res: any) => {
            const base64 = res.target.result;
            controls.get('file').setValue(base64.substr(base64.indexOf(',') + 1));
        });
    }

    onClickAddHeaders() {
        this.headersTablesAdd();
    }

    onClickAddTabs() {
        this.tabsAdd();
    }

    onChangeToggle(status: boolean) {
        if (status) {
            this.formBand.controls.filesDescription.clearValidators();
        } else {
            this.formBand.controls.filesDescription.setValidators([Validators.required]);
        }
        this.formBand.controls.filesDescription.updateValueAndValidity();
        this.formBand.controls.filesDescription.setValue(null);
        this.formBand.controls.headersTable = this._fb.array([]);
        this.formBand.controls.hasHeadersTable.setValue(status ? false : true);
        this.formBand.controls.tabs = this._fb.array([]);
        this.formBand.controls.hasTabs.setValue(status ? true : false);
    }

    onClickRemoveHeaders(index: number) {
        const formarray = this.formBand.controls['headersTable'] as FormArray;
        formarray.removeAt(index);
    }

    getHeadersControls() {
        return (this.formBand.controls['headersTable'] as FormArray).controls;
    }

    getTabsControls() {
        return (this.formBand.controls['tabs'] as FormArray).controls;
    }

    private headersTablesAdd(value?: any) {
        const headersTable = this.formBand.controls['headersTable'] as FormArray;
        headersTable.push(this.headersTableItem(value));
    }

    private headersTableItem(value?: any) {
        return this._fb.group({
            header: [
                {
                    value: value ? value.header : null,
                    disabled: false
                },
                null
            ],
            fileName: [
                {
                    value: value ? this.fileName(value.filePathUrl) : null,
                    disabled: false
                },
                null
            ],
            file: [
                {
                    value: value ? value.filePathUrl : null,
                    disabled: false
                },
                null
            ]
        });
    }

    private fileName(fileURL: any) {
        const url = fileURL.split('/');
        return url[url.length - 1];
    }

    private tabsAdd(value?: any) {
        const tabs = this.formBand.controls['tabs'] as FormArray;
        tabs.push(this.tabsItem(value));
    }

    private tabsItem(value?: any) {
        return this._fb.group({
            title: [value ? value.title : null, [Validators.required]],
            description: [value ? value.description : null, [Validators.required]]
        });
    }

    private createfrom() {
        this.form = this._fb.group({
            descriptions: [null, null]
        });
        this.onChangeControls();
    }

    private onChangeControls() {
        this.form.controls.descriptions.valueChanges.subscribe(value => {
            if (value) {
                this.formBand.controls['filesDescription'].setValue(value);
            }
        });
    }
}
