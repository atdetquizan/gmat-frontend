import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminQuestionAlternativeService {

    constructor(
        private http: HttpClient
    ) { }

    patch(questionId: number, parameters: any) {
        return this.http.patch(`${environment.apiurl}/alternatives/${questionId}`, parameters);
    }

    post(parameters) {
        return this.http.post(`${environment.apiurl}/alternatives`, parameters);
    }

    get(alternativeId: number) {
        return this.http.get(`${environment.apiurl}/alternatives/${alternativeId}`);
    }

    delete(alternativeId: number) {
        return this.http.delete(`${environment.apiurl}/alternatives/${alternativeId}`);
    }
}
