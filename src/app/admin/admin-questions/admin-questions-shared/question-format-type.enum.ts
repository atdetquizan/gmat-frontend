export enum QuestionFormatType {
    selection = 'selection',
    boolean = 'boolean',
    explanation = 'explanation',
    yesnomultiplechoice = 'yes_no_multiple_choice',
    dropdowncomplement = 'drop_down_complement',
    tablesimpleselection = 'table_simple_selection',
}
