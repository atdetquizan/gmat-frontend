import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { QuestionFormatType } from '../question-format-type.enum';

@Component({
    selector: 'gmat-admin-questions-shared-dropdown',
    templateUrl: './admin-questions-shared-dropdown.component.html',
    styleUrls: ['./admin-questions-shared-dropdown.component.less']
})
export class AdminQuestionsSharedDropdownComponent implements OnInit {
    type = QuestionFormatType;
    @Input() question: any;
    @Input() formBand: FormGroup;
    constructor() {}

    ngOnInit() {}
}
