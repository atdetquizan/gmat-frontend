import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { QuestionFormatType } from '../question-format-type.enum';

@Component({
    selector: 'gmat-admin-questions-shared-simple-selection',
    templateUrl: './admin-questions-shared-simple-selection.component.html',
    styles: []
})
export class AdminQuestionsSharedSimpleSelectionComponent implements OnInit {
    type = QuestionFormatType;
    @Input() question: any;
    @Input() formBand: FormGroup;

    constructor() {}

    ngOnInit() {}
}
