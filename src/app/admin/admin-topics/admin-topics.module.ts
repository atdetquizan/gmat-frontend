import { ControlMessagesModule } from './../../tools/control-messages/control-messages.module';
import { AdminSectionService } from './../admin-section/admin-section.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminTopicsComponent } from './admin-topics.component';
import { AdminTopicsRoutes } from './admin-topics.routing';
import { AdminTopicsListComponent } from './admin-topics-list/admin-topics-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AdminTopicsNewComponent } from './admin-topics-new/admin-topics-new.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AdminTopicsService } from './admin-topics.service';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { AdminToolsModule } from '../admin-tools/admin-tools.module';
import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { PagePaginationModule } from '../admin-tools/page-pagination/page-pagination.module';

const PROVIDERS_AUTH = [
    AdminTopicsService,
    AdminSectionService
]
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

@NgModule({
    imports: [
        CommonModule,
        AdminTopicsRoutes,
        ReactiveFormsModule,
        ModalModule.forRoot(),
        HttpClientModule,
        AdminToolsModule,
        ControlMessagesModule,
        PerfectScrollbarModule,
        PagePaginationModule
    ],
    declarations: [
        AdminTopicsComponent,
        AdminTopicsListComponent,
        AdminTopicsNewComponent
    ],
    entryComponents: [
        AdminTopicsNewComponent
    ],
    providers: [
        PROVIDERS_AUTH,
        { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorAdmin, multi: true },
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ]
})
export class AdminTopicsModule { }
