import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminTopicsComponent } from './admin-topics.component';
import { AdminTopicsListComponent } from './admin-topics-list/admin-topics-list.component';

const routes: Routes = [
    {
        path: '',
        component: AdminTopicsComponent,
        children: [
            {
                path: 'list',
                component: AdminTopicsListComponent
            },
            {
                path: '',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminTopicsRoutes { }

