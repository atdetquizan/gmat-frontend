import { AdminSectionService } from './../../admin-section/admin-section.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { AdminTopicsService } from '../admin-topics.service';
import { environment } from 'src/environments/environment';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';

@Component({
    selector: 'gmat-admin-topics-new',
    templateUrl: './admin-topics-new.component.html',
    styleUrls: ['./admin-topics-new.component.less']
})
export class AdminTopicsNewComponent implements OnInit {
    form: FormGroup;
    title: string;
    sections: any = [];
    topic: any;
    namefile: string;
    fileselection: any;
    public eventClosed: EventEmitter<any> = new EventEmitter();

    constructor(
        public bsModalRef: BsModalRef,
        private fb: FormBuilder,
        private adminTopicsService: AdminTopicsService,
        private sweetalertService: SweetalertService,
        private adminSectionService: AdminSectionService,
        private reactiveFormsService: ReactiveFormsService
    ) {
        this.creacionform();
    }

    ngOnInit() {
        this.showData();
        if (this.topic) {
            const file = this.topic.image ? this.topic.image.split('/') : [];
            this.namefile = file.pop();
            this.form.patchValue(this.topic);
            this.form.controls.filename.clearValidators();
            this.form.controls.filename.updateValueAndValidity();
        }
    }

    save() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (!this.form.valid) return;

        if (this.form.value.id) {
            this.adminTopicsService.patch(this.form.value).subscribe((res: any) => {
                if (this.fileselection) {
                    this.adminTopicsService
                        .filePost(res.id, this.fileselection)
                        .subscribe((resfile: any) => {
                            this.success();
                        });
                } else {
                    this.success();
                }
            });
        } else {
            this.adminTopicsService.post(this.form.value).subscribe((res: any) => {
                if (this.fileselection) {
                    this.adminTopicsService.filePost(res.id, this.fileselection).subscribe(() => {
                        this.success();
                    });
                }
            });
        }
    }
    success() {
        this.form.reset();
        this.sweetalertService
            .swal({
                title: environment.messages.save.title,
                text: environment.messages.save.text,
                icon: environment.messages.iconsuccess
            })
            .then(() => {
                this.eventClosed.emit(true);
                this.bsModalRef.hide();
            });
    }

    changeFile(e) {
        this.fileselection = e.target.files[0];
        this.namefile = this.fileselection.name;
    }

    private creacionform() {
        this.form = this.fb.group({
            id: [null, null],
            name: [null, [Validators.required]],
            sectionId: ['', [Validators.required]],
            filename: [null, [Validators.required]]
        });
    }

    private showData() {
        this.adminSectionService.get().subscribe((res: any) => {
            this.sections = res;
        });
    }
}
