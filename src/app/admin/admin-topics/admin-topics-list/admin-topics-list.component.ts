import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AdminTopicsService } from '../admin-topics.service';
import { AdminTopicsNewComponent } from '../admin-topics-new/admin-topics-new.component';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment';
import { AdminSectionService } from '../../admin-section/admin-section.service';
import { PaginationType } from '../../admin-tools/page-pagination/page-pagination-tools/page-pagination-type.enum';

@Component({
    selector: 'gmat-admin-topics-list',
    templateUrl: './admin-topics-list.component.html',
    styleUrls: ['./admin-topics-list.component.less']
})
export class AdminTopicsListComponent implements OnInit {
    form: FormGroup;
    bsModalRef: BsModalRef;
    topics: any = [];
    sections: any = [];
    sectionId: any;
    constructor(
        private modalService: BsModalService,
        public fb: FormBuilder,
        private adminTopicsService: AdminTopicsService,
        private sweetalertService: SweetalertService,
        private adminSectionService: AdminSectionService
    ) {
        this.createFrm();
    }

    ngOnInit() {
        this.showData();
        this.showDataSelect();
    }

    clickOpenNew(topic?: any) {
        const initialState = {
            title: topic ? 'Edit Topics' : 'Create Topics',
            topic: topic
            // parameters
        };
        const modal = this.modalService.show(AdminTopicsNewComponent, {
            class: 'modal-create-section modal-dialog-centered',
            initialState
        });
        modal.content.eventClosed.subscribe((res: boolean) => {
            if (res) {
                this.showData();
            }
        });
    }

    search(value: any) {
        const content: any = document.querySelectorAll('.ContentSearch');
        for (const item of content) {
            const index = item.innerText.toUpperCase().indexOf(value.toUpperCase());
            if (index < 0) {
                item.style.display = 'none';
            } else {
                item.style.display = 'table-row';
            }
        }
    }

    onClickDelete(item: any) {
        this.sweetalertService
            .swal({
                title: environment.messages.delete.title,
                text: environment.messages.delete.text,
                icon: 'warning',
                buttons: true,
                dangerMode: true
            })
            .then(willDelete => {
                if (willDelete) {
                    this.adminTopicsService.delete(item.id).subscribe((res: any) => {
                        this.sweetalertService.swal(environment.messages.delete.text2, {
                            icon: 'success'
                        });
                        this.showData();
                    });
                }
            });
    }

    changeSection(value: any) {
        // if (value) {
        // this.adminTopicsService.getBySectionId(value).subscribe(res => {
        //     this.topics = res;
        // });
        // } else {
        this.sectionId = value;
        this.showData();
        // }
    }

    setColumns() {
        return [
            {
                title: 'Topic',
                field: 'name'
            },
            {
                title: 'Image',
                field: 'image',
                type: PaginationType.image
            },
            {
                title: 'Section',
                field: 'sectionName'
            },
            {
                title: 'Actions',
                class: 'text-center',
                type: PaginationType.buttons,
                buttons: {
                    delete: {
                        icon: 'far fa-trash-alt'
                    },
                    edit: {
                        icon: 'fas fa-edit'
                    }
                }
            }
        ];
    }

    showData(page?: any) {
        let parameters: any = {};
        parameters.perPage = environment.pagination.adminpanel;
        parameters.page = page ? page : 1;
        if (this.sectionId) {
            parameters.sectionId = this.sectionId;
        }
        if (this.form.controls.filter.value) {
            parameters.word = this.form.controls.filter.value;
        }
        this.adminTopicsService.get(parameters).subscribe((res: any) => {
            this.topics = res;
        });
    }

    private createFrm() {
        this.form = this.fb.group({
            filter: [null, null]
        });
    }

    private showDataSelect() {
        this.adminSectionService.get().subscribe((res: any) => {
            this.sections = res;
        });
    }
}
