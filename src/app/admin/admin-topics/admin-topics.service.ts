import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminTopicsService {

    constructor(
        private http: HttpClient
    ) { }

    post(parameters: any) {
        return this.http.post(`${environment.apiurl}/topics`, parameters);
    }

    get(parameters?: any) {
        return this.http.get(`${environment.apiurl}/topics`, { params: parameters });
    }

    getById(sectionId: number) {
        return this.http.get(`${environment.apiurl}/topics/${sectionId}`);
    }

    patch(parameters: any) {
        return this.http.patch(`${environment.apiurl}/topics/${parameters.id}`, parameters);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiurl}/topics/${id}`);
    }

    getBySectionId(sectionId: number) {
        return this.http.get(`${environment.apiurl}/sections/${sectionId}/topics`);
    }

    filePost(id: number, file: any) {
        const formData: FormData = new FormData();
        formData.append('file', file);
        return this.http.post(`${environment.apiurl}/images/upload/topics/${id}`, formData);
    }

}
