import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { AdminRoutes } from './admin.routing';
import { INTERCEPTOR_ERRORS } from '../tools/common/gmat-functions';
@NgModule({
    imports: [AdminRoutes, CommonModule],
    declarations: [AdminComponent],
    providers: []
})
export class AdminModule {}
