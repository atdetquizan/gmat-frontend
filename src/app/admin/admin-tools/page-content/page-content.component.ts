import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'gmat-page-content',
    templateUrl: './page-content.component.html',
    styleUrls: ['./page-content.component.less']
})
export class PageContentComponent implements OnInit {
    toggle = false;
    constructor(
        private router: Router
    ) { }

    ngOnInit() {
    }

    toggleNavigation() {
        this.toggle = this.toggle ? false : true;
    }

    onClickSignOut() {
        localStorage.removeItem('xsermin');
        this.router.navigate(['./admin/login']);
    }

}
