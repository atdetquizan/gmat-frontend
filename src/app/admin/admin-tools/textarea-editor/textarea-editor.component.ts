import { Component, OnInit, Input, forwardRef, ViewChildren, ViewChild, OnChanges, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import Quill from 'quill';

@Component({
    selector: 'gmat-textarea-editor',
    templateUrl: './textarea-editor.component.html',
    styleUrls: ['./textarea-editor.component.less'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TextareaEditorComponent),
            multi: true
        }
    ]
})
export class TextareaEditorComponent implements OnInit, ControlValueAccessor, OnChanges {
    _value;
    txtQuill: any;
    @Input() disabled = false;
    @Input() placeholder: string;

    get value() {
        return this._value;
    }

    set value(val) {
        this._value = val;
        this.propagateChange(this._value);
    }

    propagateChange = (_: any) => { };
    @ViewChild('texteditor') texteditor: ElementRef;
    constructor(private _fb: FormBuilder) {
        // this.createForm();
    }

    ngOnInit() {
        this.txtQuill = new Quill(this.texteditor.nativeElement, {
            modules: {
                toolbar: [
                    ['bold', 'italic', 'underline', 'strike'], // toggled buttons
                    ['blockquote', 'code-block'],
                    [{ header: 1 }, { header: 2 }], // custom button values
                    [{ list: 'ordered' }, { list: 'bullet' }],
                    [{ script: 'sub' }, { script: 'super' }], // superscript/subscript
                    [{ indent: '-1' }, { indent: '+1' }], // outdent/indent
                    [{ direction: 'rtl' }], // text direction

                    [{ size: ['small', false, 'large', 'huge'] }], // custom dropdown
                    [{ header: [1, 2, 3, 4, 5, 6, false] }],

                    ['formula'],
                    [{ color: [] }, { background: [] }], // dropdown with defaults from theme
                    [{ font: [] }],
                    [{ align: [] }],

                    ['clean']
                ]
            },
            placeholder: this.placeholder,
            theme: 'snow'
        })
        this.txtQuill.on('text-change', (delta, oldDelta, source) => {
            this.value = this.txtQuill.container.innerHTML;
            this.propagateChange(this.value);
        });
    }

    ngOnChanges(values: any) {
        //
    }

    writeValue(value: any) {
        if (value !== undefined) {
            this.value = value;
            if (this.value) {
                this.txtQuill.clipboard.dangerouslyPasteHTML(value);
            } else {
                this.txtQuill.setContents([{ insert: '\n' }]);
            }
        }
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched() { }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }
}
