import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'gmat-page-sidebar',
    templateUrl: './page-sidebar.component.html',
    styleUrls: ['./page-sidebar.component.less']
})
export class PageSidebarComponent implements OnInit {
    menuopciones: any;
    constructor() {
        this.menuopciones = environment.menuopciones;
    }

    ngOnInit() {
    }

}
