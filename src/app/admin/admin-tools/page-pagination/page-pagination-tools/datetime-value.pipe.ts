import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { environment } from 'src/environments/environment';

@Pipe({
    name: 'datetimevalue'
})
export class DatetimeValuePipe implements PipeTransform {

    transform(value: any, args?: any): any {
        return moment(value).format(environment.format.datetime);
    }

}
