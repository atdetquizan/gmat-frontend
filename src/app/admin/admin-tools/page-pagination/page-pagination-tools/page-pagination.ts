// invoke value of object => invokePath('key.key', obj)
export function invokePath(path: string, obj: any): any {
    return path.split('.').reduce((prev, curr) => {
        return prev ? prev[curr] : null;
    }, obj || self);
}
