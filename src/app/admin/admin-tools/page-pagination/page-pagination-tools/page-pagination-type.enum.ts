export enum PaginationType {
    datetime = 1,
    image = 2,
    buttons = 3
}

export enum PaginationButtonType {
    delete = 1,
    edit = 2
}