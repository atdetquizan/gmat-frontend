import { Pipe, PipeTransform } from '@angular/core';
import { invokePath } from './page-pagination';

@Pipe({
    name: 'invokevalues'
})
export class InvokeValuesPipe implements PipeTransform {

    transform(value: any, path: any): any {
        return invokePath(path, value) ? invokePath(path, value) : '';
    }

}
