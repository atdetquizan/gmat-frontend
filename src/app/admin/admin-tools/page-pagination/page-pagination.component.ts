import { Component, OnInit, Input, ChangeDetectionStrategy, EventEmitter, Output } from '@angular/core';
import { PaginationType } from './page-pagination-tools/page-pagination-type.enum';

@Component({
    selector: 'gmat-page-pagination',
    templateUrl: './page-pagination.component.html',
    styleUrls: ['./page-pagination.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush

})
export class PagePaginationComponent implements OnInit {
    @Input() pagination: any;
    @Input() columns: any;
    @Input() viewpages = 3;
    @Input() textloading = 'Loading';
    @Input() textNoFound = 'No records found.'
    @Output() changepage = new EventEmitter();
    @Output() changedelete = new EventEmitter();
    @Output() changeedit = new EventEmitter();
    // loading: boolean;
    pagecurrent = 1;
    paginationType = PaginationType;
    viewTotalPages = 0;
    constructor() { }

    ngOnInit() {
        // this.loading = true;
    }

    ngOnChanges(changes: any) {
        if (changes.pagination) {
            if (this.pagination.current_page === 1) {
                this.viewTotalPages = 0;
                this.pagecurrent = 1;
            }
        }
        // if (this.pagination)
            // this.loading = false;
    }

    clickChangePages(page: number) {
        // this.loading = true;
        this.changepage.emit(page);
    }

    clickBackPages(page: number) {
        // this.loading = true;
        if (this.pagecurrent - 1 <= page && this.pagecurrent > 1) {
            this.pagecurrent -= 1;
        }
        this.changepage.emit(page);
    }

    clickNextPages(page: number) {
        // this.loading = true;
        if (this.viewpages <= page) {
            if (page <= this.pagination.last_page)
                this.pagecurrent += 1;
        }
        this.changepage.emit(page);
    }

    arrayPages(): any[] {
        return Array(this.getParseData() ? this.viewpages : 0);
    }

    getParseData() {
        if (this.pagination['data'] && this.pagination.data['clients']) {
            return this.pagination.data['clients']
        }
        if (this.pagination['data'] && this.pagination.data.length) {
            return this.pagination.data;
        }
        return [];
    }
}
