import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvokeValuesPipe } from './page-pagination-tools/invoke-values.pipe';
import { PagePaginationComponent } from './page-pagination.component';
import { DatetimeValuePipe } from './page-pagination-tools/datetime-value.pipe';
import { PerfectScrollbarModule, PerfectScrollbarConfigInterface, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    // suppressScrollX: true
};

@NgModule({
    imports: [
        CommonModule,
        PerfectScrollbarModule
    ],
    declarations: [
        PagePaginationComponent,
        InvokeValuesPipe,
        DatetimeValuePipe
    ],
    exports: [
        PagePaginationComponent,
        InvokeValuesPipe,
        DatetimeValuePipe
    ],
    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        }
    ]
})
export class PagePaginationModule { }
