import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageContentComponent } from './page-content/page-content.component';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { PageSidebarComponent } from './page-sidebar/page-sidebar.component';
import { RouterModule } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { ComboboxSelectComponent } from './combobox-select/combobox-select.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TextareaEditorComponent } from './textarea-editor/textarea-editor.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

const components = [PageContentComponent, PageSidebarComponent, ComboboxSelectComponent, TextareaEditorComponent];

@NgModule({
    imports: [CommonModule, RouterModule, PerfectScrollbarModule, ReactiveFormsModule, FormsModule],
    declarations: components,
    exports: components,
    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AppHttpInterceptorAdmin,
            multi: true
        }
    ]
})
export class AdminToolsModule {}
