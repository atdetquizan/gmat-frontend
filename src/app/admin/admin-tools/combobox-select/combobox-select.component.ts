import { Component, OnInit, Input, forwardRef, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, ControlValueAccessor, NG_VALUE_ACCESSOR, FormControl } from '@angular/forms';

@Component({
    selector: 'gmat-combobox-select',
    templateUrl: './combobox-select.component.html',
    styleUrls: ['./combobox-select.component.less'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ComboboxSelectComponent),
            multi: true
        }
    ]
})
export class ComboboxSelectComponent implements ControlValueAccessor {
    form: FormControl;

    @Input()
    name: string;

    @Input('value')
    val = '';

    @Input() data = [];

    onChange: any = () => {};
    onTouched: any = () => {};

    get value() {
        return this.val;
    }

    set value(val) {
        this.val = val;
        this.onChange(val);
        this.onTouched();
    }

    onChangeSelect(val) {
        this.onChange(val);
        this.onTouched();
    }

    registerOnChange(fn) {
        this.onChange = fn;
    }

    registerOnTouched(fn) {
        this.onTouched = fn;
    }

    writeValue(value) {
        this.value = value ? value : '';
    }
}
