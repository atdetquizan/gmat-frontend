import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminAuthComponent } from './admin-auth.component';
import { AdminAuthRoutes } from './admin-auth.routing';
import { AdminAuthLoginComponent } from './admin-auth-login/admin-auth-login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminAuthService } from './admin-auth.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { AdminToolsModule } from '../admin-tools/admin-tools.module';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
const PROVIDERS_AUTH = [
    AdminAuthService,
    AuthenticationService
]
@NgModule({
    imports: [
        CommonModule,
        AdminAuthRoutes,
        ReactiveFormsModule,
        HttpClientModule,
        AdminToolsModule,
        ControlMessagesModule
    ],
    declarations: [
        AdminAuthComponent,
        AdminAuthLoginComponent
    ],
    providers: [
        PROVIDERS_AUTH,
        { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorAdmin, multi: true }
    ]
})
export class AdminAuthModule { }
