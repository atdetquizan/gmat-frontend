import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminAuthComponent } from './admin-auth.component';
import { AdminAuthLoginComponent } from './admin-auth-login/admin-auth-login.component';

const routes: Routes = [
    {
        path: '',
        component: AdminAuthComponent,
        children: [
            {
                path: '',
                component: AdminAuthLoginComponent
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminAuthRoutes { }

