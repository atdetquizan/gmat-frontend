import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class AdminAuthService {

    constructor() { }

    getUserAdmin() {
        return localStorage.getItem('xsermin') ? JSON.parse(atob(localStorage.getItem('xsermin'))) : null;
    }

}
