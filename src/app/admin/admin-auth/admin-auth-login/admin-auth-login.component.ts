import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/authentication/authentication.service';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';

@Component({
    selector: 'gmat-admin-auth-login',
    templateUrl: './admin-auth-login.component.html',
    styleUrls: ['./admin-auth-login.component.less']
})
export class AdminAuthLoginComponent implements OnInit {
    form: FormGroup;
    loading = false;
    constructor(
        private fb: FormBuilder,
        private authenticationService: AuthenticationService,
        private reactiveFormsService: ReactiveFormsService,
        private router: Router,
        private sweetalertService: SweetalertService
    ) {
        this.creacionform();
    }

    ngOnInit() {}

    signup() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            this.loading = true;
            const values = this.form.value;
            values.ttl = environment.ttl;
            values.type = environment.userstype.admin;
            this.authenticationService.login(values).subscribe(
                res => {
                    localStorage.setItem('xsermin', btoa(JSON.stringify(res)));
                    this.router.navigate(['/admin']);
                },
                err => {
                    this.loading = false;
                    const error = err.error.error;
                    this.sweetalertService.swal({
                        title: error.name,
                        text: error.message,
                        icon: environment.messages.iconerror
                    });
                }
            );
        }
    }

    private creacionform() {
        this.form = this.fb.group({
            email: [null, [Validators.email, Validators.required]],
            password: [null, [Validators.required]]
        });
    }
}
