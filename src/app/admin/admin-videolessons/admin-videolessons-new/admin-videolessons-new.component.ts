import { AdminQuestionsService } from './../../admin-questions/admin-questions.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AdminVideolessonsService } from '../admin-videolessons.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { emptyToNull, toDataURL } from 'src/app/tools/common/gmat-functions';
import { TypeLessons } from 'src/app/portal/portal-platform/portal-platform-lessons/portal-platform-lessons-shared/TypeLessons.enum';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'gmat-admin-videolessons-new',
    templateUrl: './admin-videolessons-new.component.html',
    styleUrls: ['./admin-videolessons-new.component.less'],
})
export class AdminVideolessonsNewComponent implements OnInit {
    alert: any;
    form: FormGroup;
    sections: any = [];
    topics: any = [];
    subjects: any = [];
    subsubjects: any = [];
    objectGroups: any[] = [];
    fileselection: any;
    namefile: string;
    id: number;
    show = false;

    constructor(
        public fb: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private adminVideolessonsService: AdminVideolessonsService,
        private sweetalertService: SweetalertService,
        private reactiveFormsService: ReactiveFormsService,
        private adminQuestionsService: AdminQuestionsService
    ) {
        this.createForm();
    }

    ngOnInit() {
        this.showDataSection().subscribe(() => {
            this.id = this.activatedRoute.snapshot.params.id;
            if (this.id) {
                this.show = true;
                this.adminVideolessonsService
                    .getById(this.id)
                    .subscribe((res: any) => {
                        this.form.controls.fileName.clearValidators();
                        this.namefile = res.attachedName;
                        // debugger;
                        this.form.patchValue(res);
                        // this.onChangeSections(this.form.controls.sectionId.value);
                        // if (this.form.controls.topicId.value)
                        //     this.onChangeTopics(this.form.controls.topicId.value);
                        // if (this.form.controls.subjectId.value)
                        //     this.onChangeSubjects(this.form.controls.subjectId.value);

                        this.onChangeSections(res.sectionId);
                        this.form.controls.topicId.setValue(res.topicId);
                        this.onChangeTopics(res.topicId);
                        this.form.controls.subjectId.setValue(res.subjectId);
                        this.onChangeSubjects(res.subjectId);
                        this.form.controls.subSubjectId.setValue(res.subSubjectId);
                    });
            }
        });
    }

    onChangeSections(sectionId: number) {
        // reset lessons controls
        this.onResetLesson(TypeLessons.sections);
        // get section object
        const section = this.sections.find(x => x.id === Number(sectionId));
        if (section) {
            // set object topics
            this.topics = section && section.topics ? section.topics : [];
            // enable controls topics
            if (section.topics && section.topics.length > 0)
                this.form.controls.topicId.enable();
        }
    }

    onChangeTopics(topicId: number) {
        // reset lessons controls
        this.onResetLesson(TypeLessons.topics);
        // get section object
        const topic = this.topics.find(x => x.id === Number(topicId));
        if (topic) {
            // set object topics
            this.subjects = topic && topic.subjects ? topic.subjects : [];
            // enable controls topics
            if (topic.subjects && topic.subjects.length > 0)
                this.form.controls.subjectId.enable();
        }
    }

    onChangeSubjects(subjectId: number) {
        // reset lessons controls
        this.onResetLesson(TypeLessons.subjects);
        // get section object
        const subject = this.subjects.find(x => x.id === Number(subjectId));
        if (subject) {
            // set object topics
            this.subsubjects = subject.subSubjects ? subject.subSubjects : [];
            // enable controls topics
            if (subject.subSubjects && subject.subSubjects.length > 0)
                this.form.controls.subSubjectId.enable();
        }
    }

    onClickSave() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (this.form.valid) {
            const parameters = this.form.getRawValue();
            parameters.sectionId =
                this.topics.length === 0
                    ? emptyToNull(parameters.sectionId)
                    : null;
            parameters.topicId =
                this.subjects.length === 0
                    ? emptyToNull(parameters.topicId)
                    : null;
            parameters.subjectId =
                this.subsubjects.length === 0
                    ? emptyToNull(parameters.subjectId)
                    : null;
            parameters.subSubjectId = emptyToNull(parameters.subSubjectId);
            parameters.fileName = this.namefile;
            
            const showUrl = this.id ? this.adminVideolessonsService.patch(this.id, parameters) : this.adminVideolessonsService.post(parameters);
            showUrl.subscribe(
                res => {
                    this.success();
                },
                error => {
                    this.alert = {
                        type: 'danger',
                        title: 'Error, ',
                        msg: error,
                    };
                }
            );
        }
    }

    success() {
        this.namefile = '';
        this.form.reset();
        this.sweetalertService.swal({
            title: environment.messages.save.title,
            text: environment.messages.save.text,
            icon: environment.messages.iconsuccess,
        }).then(() => {
            // if(this.id) {
            //     window.location.reload();
            //  } 
            this.router.navigate(['./admin/videolessons/list']);
        });
        this.onResetLesson(TypeLessons.sections);
       
    }

    onResetType() {
        this.namefile = '';
        this.form.reset();
        this.onResetLesson(TypeLessons.sections);
    }

    changeFile(e) {
        this.fileselection = e.target.files[0];
        this.namefile = this.fileselection.name;
        toDataURL(this.fileselection).then((dataUrl: any) => {
            const base64 = dataUrl.target.result;
            this.form.controls.encodedPdf.setValue(
                base64.substr(base64.indexOf(',') + 1)
            );
        });
    }

    onClickCancelar() {
        this.router.navigate(['./admin/videolessons/list']);
    }

    // SET RESET SELECT LESSONS
    private onResetLesson(lesson: TypeLessons) {
        switch (lesson) {
            case TypeLessons.sections:
                this.topics = [];
                this.subjects = [];
                this.subsubjects = [];
                // set value empty
                this.form.controls.topicId.setValue('');
                this.form.controls.subjectId.setValue('');
                // disabled controlsf
                this.form.controls.topicId.disable();
                this.form.controls.subjectId.disable();
                break;

            case TypeLessons.topics:
                this.subjects = [];
                this.subsubjects = [];
                // set value empty
                this.form.controls.subjectId.setValue('');
                // disabled controls
                this.form.controls.subjectId.disable();
                break;
        }
        // default ya que siempre se va repetir en todas las acciones
        this.subsubjects = [];
        // set value empty
        this.form.controls.subSubjectId.setValue('');
        // disabled controls
        this.form.controls.subSubjectId.disable();
    }
    private showDataSection() {
        return this.adminQuestionsService.get().pipe(
            tap((res: any) => {
                this.sections = res;
            })
        );
    }

    private createForm() {
        this.form = this.fb.group({
            id: [null, null],
            nodeName: [{ value: '', disabled: true }, null],
            sectionId: ['', (this.id = this.activatedRoute.snapshot.params.id) ? null : [Validators.required]],
            topicId: [{ value: '', disabled: true }, [Validators.required]],
            subjectId: [{ value: '', disabled: true }, [Validators.required]],
            subSubjectId: [
                { value: '', disabled: true },
                [Validators.required],
            ],
            url: [null, [Validators.required]],
            title: [null, [Validators.required]],
            description: [null, [Validators.required]],
            fileName: [null, null],
            encodedPdf: [null, null],
        });
    }
}
