import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AdminVideolessonsService } from '../admin-videolessons.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { PaginationType } from '../../admin-tools/page-pagination/page-pagination-tools/page-pagination-type.enum';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';

@Component({
    selector: 'gmat-admin-videolessons-list',
    templateUrl: './admin-videolessons-list.component.html',
    styleUrls: ['./admin-videolessons-list.component.less']
})
export class AdminVideolessonsListComponent implements OnInit {

    form: FormGroup;
    videoslessons: any = [];

    constructor(
        private router: Router,
        public fb: FormBuilder,
        private adminVideolessonsService: AdminVideolessonsService,
        private sweetalertService: SweetalertService
    ) {
        this.createfrm();
    }

    ngOnInit() {
        this.showData();
    }

    clickOpenNew(item: any) {
        this.router.navigate([`./admin/videolessons/edit/` + item.id])
    }

    onClickDelete(item: any) {
        this.sweetalertService.swal({
            title: environment.messages.delete.title,
            text: environment.messages.delete.text,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                this.adminVideolessonsService.delete(item.id).subscribe((res: any) => {
                    this.sweetalertService.swal(environment.messages.delete.text2, {
                        icon: "success",
                    });
                    this.showData();
                })
            }
        });
    }

    setColumns() {
        return [
            {
                title: 'ID',
                field: 'id'
            },
            {
                title: 'Section',
                field: 'sectionName'
            },
            {
                title: 'Topics',
                field: 'topicName'
            },
            {
                title: 'Subject',
                field: 'subjectName'
            },
            {
                title: 'Sub-Subject',
                field: 'subSubjectName'
            },
            {
                title: 'Title',
                field: 'title'
            },
            {
                title: 'Video url',
                field: 'url'
            },
            {
                title: 'Description',
                field: 'description'
            },
            {
                title: 'FileName',
                field: 'description'
            },
            {
                title: 'Actions',
                class: 'text-center',
                type: PaginationType.buttons,
                buttons: {
                    delete: {
                        icon: 'far fa-trash-alt'
                    },
                    edit: {
                        icon: 'fas fa-edit'
                    }
                }
            }
        ];
    }

    showData(page?: any) {
        let parameters: any = {};
        parameters.perPage = environment.pagination.adminpanel;
        parameters.page = page ? page : 1;
        if (this.form.controls.filter.value) {
            parameters.word = this.form.controls.filter.value;
        }
        this.adminVideolessonsService.get(parameters).subscribe((res: any) => {
            this.videoslessons = res;
        }, () => {
            this.videoslessons = [];
        });
    }

    private createfrm() {
        this.form = this.fb.group({
            filter: [null, null]
        });
    }

}
