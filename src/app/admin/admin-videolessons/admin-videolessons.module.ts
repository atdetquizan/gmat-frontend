import { AdminQuestionsService } from './../admin-questions/admin-questions.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminVideolessonsComponent } from './admin-videolessons.component';
import { AdminVideolessonsListComponent } from './admin-videolessons-list/admin-videolessons-list.component';
import { AdminVideolessonsRoutes } from './admin-videolessons.routing';
import { AdminToolsModule } from '../admin-tools/admin-tools.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdminVideolessonsNewComponent } from './admin-videolessons-new/admin-videolessons-new.component';
import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { PerfectScrollbarModule, PerfectScrollbarConfigInterface, PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { AdminVideolessonsService } from './admin-videolessons.service';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { ReactiveFormsModule } from '@angular/forms';
import { PagePaginationModule } from '../admin-tools/page-pagination/page-pagination.module';
import { AlertModule } from 'ngx-bootstrap';
import { INTERCEPTOR_ERRORS } from 'src/app/tools/common/gmat-functions';

const PROVIDERS_AUTH = [
    AdminVideolessonsService,
    AdminQuestionsService
];

@NgModule({
    imports: [
        CommonModule,
        AdminVideolessonsRoutes,
        AdminToolsModule,
        HttpClientModule,
        ReactiveFormsModule,
        ControlMessagesModule,
        PerfectScrollbarModule,
        PagePaginationModule,
        AlertModule.forRoot()
    ],
    declarations: [
        AdminVideolessonsComponent,
        AdminVideolessonsListComponent,
        AdminVideolessonsNewComponent
    ],
    providers: [
        PROVIDERS_AUTH,
        INTERCEPTOR_ERRORS,
        { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorAdmin, multi: true }
    ]
})
export class AdminVideolessonsModule { }
