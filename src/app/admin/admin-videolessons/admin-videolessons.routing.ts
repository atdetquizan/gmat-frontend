import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminVideolessonsComponent } from './admin-videolessons.component';
import { AdminVideolessonsListComponent } from './admin-videolessons-list/admin-videolessons-list.component';
import { AdminVideolessonsNewComponent } from './admin-videolessons-new/admin-videolessons-new.component';

const routes: Routes = [
    {
        path: '',
        component: AdminVideolessonsComponent,
        children: [
            {
                path: 'list',
                component: AdminVideolessonsListComponent
            },
            {
                path: 'new',
                component: AdminVideolessonsNewComponent
            },
            {
                path: 'edit/:id',
                component: AdminVideolessonsNewComponent
            },
            {
                path: '',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminVideolessonsRoutes { }
