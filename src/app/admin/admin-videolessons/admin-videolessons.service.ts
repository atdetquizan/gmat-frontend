import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminVideolessonsService {

    constructor(
        private http: HttpClient
    ) { }

    post(parameters: any) {
        return this.http.post(`${environment.apiurl}/videos`, parameters);
    }

    patch(id: any, parameters: any) {
        return this.http.patch(`${environment.apiurl}/videos/${id}`, parameters);
    }

    get(parameters?: any) {
        return this.http.get(`${environment.apiurl}/videos`, { params: parameters });
    }

    getById(id: number) {
        return this.http.get(`${environment.apiurl}/videos/${id}`);
    }

    getByClientId(parameters: any) {
        return this.http.post(`${environment.apiurl}/clients/videos`, parameters);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiurl}/videos/${id}`);
    }
}
