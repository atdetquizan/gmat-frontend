import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminSectionComponent } from './admin-section.component';
import { AdminSectionListComponent } from './admin-section-list/admin-section-list.component';

const routes: Routes = [
    {
        path: '',
        component: AdminSectionComponent,
        children: [
            {
                path: 'list',
                component: AdminSectionListComponent
            },
            {
                path: '',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminSectionRoutes { }

