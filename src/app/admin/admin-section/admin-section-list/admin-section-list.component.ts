import { Component, OnInit, EventEmitter } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AdminSectionNewComponent } from '../admin-section-new/admin-section-new.component';
import { AdminSectionService } from '../admin-section.service';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { environment } from 'src/environments/environment';
import { PaginationType, PaginationButtonType } from '../../admin-tools/page-pagination/page-pagination-tools/page-pagination-type.enum';


@Component({
    selector: 'gmat-admin-section-list',
    templateUrl: './admin-section-list.component.html',
    styleUrls: ['./admin-section-list.component.less']
})
export class AdminSectionListComponent implements OnInit {
    form: FormGroup;
    sections: any = [];
    constructor(
        private modalService: BsModalService,
        public fb: FormBuilder,
        private adminSectionService: AdminSectionService,
        private sweetalertService: SweetalertService
    ) {
        this.createfrm();
    }

    ngOnInit() {
        this.showData();
    }

    clickOpenNew(section?: any) {
        const initialState = {
            title: section ? 'Edit Section' : 'Create Section',
            section: section
            // parameters
        };
        const modal = this.modalService.show(AdminSectionNewComponent, { class: 'modal-create-section modal-dialog-centered', initialState });
        modal.content.eventClosed.subscribe((res: boolean) => {
            if (res) {
                this.showData();
            }
        });
    }

    onClickDelete(item: any) {
        this.sweetalertService.swal({
            title: environment.messages.delete.title,
            text: environment.messages.delete.text,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                this.adminSectionService.delete(item.id).subscribe((res: any) => {
                    this.sweetalertService.swal(environment.messages.delete.text2, {
                        icon: "success",
                    });
                    this.showData();
                })
            }
        });
    }

    setColumns() {
        return [
            {
                title: 'ID',
                field: 'id'
            },
            {
                title: 'Section - Name',
                field: 'name'
            },
            {
                title: 'Image',
                field: 'image',
                class: 'text-center',
                type: PaginationType.image
            },
            {
                title: 'Actions',
                class: 'text-center',
                type: PaginationType.buttons,
                buttons: {
                    delete: {
                        icon: 'far fa-trash-alt'
                    },
                    edit: {
                        icon: 'fas fa-edit'
                    }
                }
            }
        ];
    }

    showData(page?: any) {
        let parameters: any = {};
        parameters.perPage = environment.pagination.adminpanel;
        parameters.page = page ? page : 1;
        if (this.form.controls.filter.value) {
            parameters.word = this.form.controls.filter.value;
        }
        this.adminSectionService.get(parameters).subscribe((res: any) => {
            this.sections = res;
        }, () => {
            this.sections = null;
        });
    }

    private createfrm() {
        this.form = this.fb.group({
            filter: [null, null]
        });
    }

}
