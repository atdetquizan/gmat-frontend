import { Component, OnInit, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SweetalertService } from 'src/app/tools/services/sweetalert.service';
import { AdminSectionService } from '../admin-section.service';
import { environment } from 'src/environments/environment';
import { ReactiveFormsService } from 'src/app/tools/services/reactive-forms.service';

@Component({
    selector: 'gmat-admin-section-new',
    templateUrl: './admin-section-new.component.html',
    styleUrls: ['./admin-section-new.component.less']
})
export class AdminSectionNewComponent implements OnInit {
    form: FormGroup;
    title: string;
    section: any;
    fileselection: any;
    namefile: string;
    public eventClosed: EventEmitter<any> = new EventEmitter();
    constructor(
        public bsModalRef: BsModalRef,
        private fb: FormBuilder,
        private adminSectionService: AdminSectionService,
        private sweetalertService: SweetalertService,
        private reactiveFormsService: ReactiveFormsService
    ) {
        this.creacionform();
    }

    ngOnInit() {
        if (this.section) {
            const file = this.section.image ? (this.section.image).split('/') : [];
            this.namefile = file.pop();
            this.form.controls.filename.clearValidators();
            this.form.patchValue(this.section);
        }
    }

    save() {
        this.reactiveFormsService.markFormGroupTouched(this.form);
        if (!this.form.valid)
            return;

        if (this.form.value.id) {
            this.adminSectionService.path(this.form.value)
                .subscribe((res: any) => {
                    if (this.fileselection) {
                        this.adminSectionService.filePost(res.id, this.fileselection)
                            .subscribe(() => {
                                this.success();
                            });
                    } else {
                        this.success();
                    }
                });
        } else {
            this.adminSectionService.post(this.form.value).subscribe((res: any) => {
                if (this.fileselection) {
                    this.adminSectionService.filePost(res.id, this.fileselection)
                        .subscribe(() => {
                            this.success();
                        });
                }
            });
        }
    }

    success() {
        this.form.reset();
        this.sweetalertService.swal({
            title: environment.messages.save.title,
            text: environment.messages.save.text,
            icon: environment.messages.iconsuccess
        }).then(() => {
            this.eventClosed.emit(true);
            this.bsModalRef.hide();
        });

    }

    changeFile(e) {
        this.fileselection = e.target.files[0];
        this.namefile = this.fileselection.name;
    }

    private creacionform() {
        this.form = this.fb.group({
            id: [null, null],
            name: [null, [Validators.required]],
            filename: [null, [Validators.required]],
            hasGraphic: [false, null],
            adaptive: [false, null]
        });
    }


}
