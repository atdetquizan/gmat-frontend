import { ControlMessagesModule } from 'src/app/tools/control-messages/control-messages.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminSectionComponent } from './admin-section.component';
import { AdminSectionRoutes } from './admin-section.routing';
import { AdminSectionListComponent } from './admin-section-list/admin-section-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminSectionNewComponent } from './admin-section-new/admin-section-new.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AdminSectionService } from './admin-section.service';
import { AppHttpInterceptorAdmin } from 'src/app/interceptors/AppHttpInterceptorAdmin';
import { AdminToolsModule } from '../admin-tools/admin-tools.module';
import { PagePaginationModule } from '../admin-tools/page-pagination/page-pagination.module';

const PROVIDERS_AUTH = [
    AdminSectionService
]

@NgModule({
    imports: [
        CommonModule,
        AdminSectionRoutes,
        ReactiveFormsModule,
        HttpClientModule,
        ModalModule.forRoot(),
        AdminToolsModule,
        ControlMessagesModule,
        PagePaginationModule
    ],
    declarations: [
        AdminSectionComponent,
        AdminSectionListComponent,
        AdminSectionNewComponent,
    ],
    entryComponents: [
        AdminSectionNewComponent
    ],
    providers: [
        PROVIDERS_AUTH,
        { provide: HTTP_INTERCEPTORS, useClass: AppHttpInterceptorAdmin, multi: true },
    ]
})
export class AdminSectionModule { }
