import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AdminSectionService {

    constructor(
        private http: HttpClient
    ) { }

    post(parameters: any) {
        return this.http.post(`${environment.apiurl}/sections`, parameters);
    }

    getById(sectionId: number) {
        return this.http.get(`${environment.apiurl}/sections/${sectionId}`);
    }

    get(parameters?: any) {
        return this.http.get(`${environment.apiurl}/sections`, { params: parameters });
    }

    path(parameters) {
        return this.http.patch(`${environment.apiurl}/sections/${parameters.id}`, parameters);
    }

    delete(sectionId: number) {
        return this.http.delete(`${environment.apiurl}/sections/${sectionId}`);
    }

    filePost(sectionId: number, file: any) {
        const formData: FormData = new FormData();
        formData.append('file', file);
        return this.http.post(`${environment.apiurl}/images/upload/sections/${sectionId}`, formData);
    }

    getVideos(sectionId: number) {
        return this.http.get(`${environment.apiurl}/sections/${sectionId}/videos`);
    }

}
