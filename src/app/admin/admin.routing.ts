import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AuthAdminGuard } from '../tools/guards/auth-admin.guard';

const routes: Routes = [
    {
        path: '',
        component: AdminComponent,
        children: [
            {
                path: 'login',
                loadChildren: './admin-auth/admin-auth.module#AdminAuthModule'
            },
            {
                path: 'users',
                loadChildren: './admin-users/admin-users.module#AdminUsersModule',
                canActivate: [AuthAdminGuard]
            },
            {
                path: 'section',
                loadChildren: './admin-section/admin-section.module#AdminSectionModule',
                canActivate: [AuthAdminGuard]
            },
            {
                path: 'topics',
                loadChildren: './admin-topics/admin-topics.module#AdminTopicsModule',
                canActivate: [AuthAdminGuard]
            },
            {
                path: 'subjects',
                loadChildren: './admin-subjects/admin-subjects.module#AdminSubjectsModule',
                canActivate: [AuthAdminGuard]
            },
            {
                path: 'subsubjects',
                loadChildren: './admin-subsubjects/admin-subsubjects.module#AdminSubsubjectsModule',
                canActivate: [AuthAdminGuard]
            },
            {
                path: 'videolessons',
                loadChildren: './admin-videolessons/admin-videolessons.module#AdminVideolessonsModule',
                canActivate: [AuthAdminGuard]
            },
            {
                path: 'questions',
                loadChildren: './admin-questions/admin-questions.module#AdminQuestionsModule',
                canActivate: [AuthAdminGuard]
            },
            {
                path: 'pricing',
                loadChildren: './admin-pricing/admin-pricing.module#AdminPricingModule',
                canActivate: [AuthAdminGuard]
            },
            {
                path: 'page',
                loadChildren: './admin-page/admin-page.module#AdminPageModule',
                canActivate: [AuthAdminGuard]
            },
            {
                path: '',
                redirectTo: 'users'
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutes { }

