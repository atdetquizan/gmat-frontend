import { Component, HostListener } from '@angular/core';

@Component({
    selector: 'gmat-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent {
    title = 'gmat';
}
