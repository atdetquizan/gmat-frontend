import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpResponse,
    HttpEvent
} from '@angular/common/http';
import { finalize, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AdminAuthService } from '../admin/admin-auth/admin-auth.service';
import { loading } from '../tools/common/gmat-functions';


@Injectable()
export class AppHttpInterceptorAdmin implements HttpInterceptor {

    constructor(
        private auth: AdminAuthService
    ) {
        //
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const started = Date.now();
        let ok: string;
        loading(true);
        // this.loadingBar.start();
        /* authentication token user */
        const xsermin = this.auth.getUserAdmin();
        if (xsermin) {
            req = req.clone({
                setHeaders: {
                    // 'x-api-key': this.getApiKey(req.url)
                    'Authorization': xsermin.id
                }
            });
        }
        /* fin authentication token user */
        // extend server response observable with logging
        return next.handle(req)
            .pipe(
                tap(
                    // Succeeds when there is a response; ignore other events
                    event => ok = event instanceof HttpResponse ? 'succeeded' : '',
                    // Operation failed; error is an HttpErrorResponse
                    error => {
                        return error.error;
                    }
                ),
                // Log when response observable either completes or errors
                finalize(() => {
                    const elapsed = Date.now() - started;
                    const msg = `${req.method} "${req.urlWithParams}" ${ok} in ${elapsed} ms.`;
                    // this.loadingBar.complete();
                    loading(false);
                })
            );
    }
}
