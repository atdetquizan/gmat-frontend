import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpResponse,
    HttpEvent
} from '@angular/common/http';
import { finalize, tap, retry } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable()
export class AppHttpInterceptorClient implements HttpInterceptor {
    constructor(private auth: AuthenticationService) {}

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        const xser = this.auth.getUserClient();
        if (xser) {
            req = req.clone({
                setHeaders: {
                    Authorization: xser.id
                }
            });
        }
        /* fin authentication token user */
        // extend server response observable with logging
        return next.handle(req);
    }
}
