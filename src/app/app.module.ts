import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRouting } from './app.routing';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { AuthEventService } from './tools/services/auth-event.service';
@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        AppRouting,
        HttpClientModule,
        LoadingBarModule
    ],
    providers: [
        AuthEventService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
