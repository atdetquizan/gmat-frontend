// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    secreteKey: 'e7d5442d-fcd8-4b5e-8da3-cbcd17e21eb9',
    qulqiKey: 'pk_test_FeiUCjdGY76xdEkN',
    apiurl: 'https://pappstest.com:5000/api',
    ttl: 31556926,
    pagination: {
        adminpanel: 10,
    },
    format: {
        datetime: 'DD/MM/YYYY HH:mm A'
    },
    messages: {
        save: {
            title: 'Successful registration',
            text: 'The information was registered correctly.'
        },
        delete: {
            title: 'Are you sure to delete?',
            text: 'Once deleted, all related information will be deleted.',
            text2: 'It was successfully deleted.'
        },
        recoverkey: {
            title: 'Successful Shipping',
            text: 'Check your email'
        },
        changepassword: {
            title: 'Successful Password Change',
            text: 'Log in'
        },
        error: {
            title: 'Error, ',
            text: 'There is a problem with the connection or the server.'
        },
        errorcar: {
            title: 'Error, ',
            text: 'It is required that you enter a payment method.'
        },
        iconerror: 'error',
        iconsuccess: 'success',
        iconwarning: 'warning'
    },
    menuopciones: [
        {
            title: 'Users',
            route: '/admin/users'
        },
        {
            title: 'Section',
            route: '/admin/section'
        },
        {
            title: 'Topics',
            route: '/admin/topics'
        },
        {
            title: 'Subjects',
            route: '/admin/subjects'
        },
        {
            title: 'Sub-Subjects',
            route: '/admin/subsubjects'
        },
        {
            title: 'Video Lessons',
            route: '/admin/videolessons'
        },
        {
            title: 'Questions',
            route: '/admin/questions'
        },
        {
            title: 'Pricing',
            route: '/admin/pricing'
        },
        {
            title: 'Page',
            route: '/admin/page'
        }
    ],
    userstype: {
        client: 'client',
        admin: 'admin'
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
